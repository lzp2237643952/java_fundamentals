public class VariableName {

	public static void main(String[] args) {
		// 变量的命名
		int a1, a_5$, _6b, $c5;
		// int a*b; //编译错误，不能包含*号等特殊字符
		// int 1a; //编译错误，不能以数字开头
		int a2 = 5;
		// System.out.println(A2); //编译错误，严格区分大小写
		// int class; //编译错误，不能使用关键字
		int 年龄; // 正确，但不建议
		int age; // 建议"英文的见名知意"
		int score, myScore, myJavaScore; // 建议"驼峰命名法"
	}
}
