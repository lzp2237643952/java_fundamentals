# 01_Java关键字

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC
---

<!-- TOC -->

- [1. 访问控制](#1-访问控制)
    - [1.1. private：私有的](#11-private私有的)
    - [1.2. protected：受保护的](#12-protected受保护的)
    - [1.3. public：公共的](#13-public公共的)
- [2. 类、方法和变量修饰符](#2-类方法和变量修饰符)
    - [2.1. abstract：声明抽象](#21-abstract声明抽象)
    - [2.2. class：类](#22-class类)
    - [2.3. extends：泛化,继承](#23-extends泛化继承)
    - [2.4. final：最终的,不可改变的](#24-final最终的不可改变的)
    - [2.5. implements：实现（接口）](#25-implements实现接口)
    - [2.6. interface：接口](#26-interface接口)
    - [2.7. native：本地，原生方法（非Java实现）](#27-native本地原生方法非java实现)
    - [2.8. new：新,创建](#28-new新创建)
    - [2.9. static：静态](#29-static静态)
    - [2.10. strictfp：严格,精准](#210-strictfp严格精准)
    - [2.11. synchronized：线程,同步](#211-synchronized线程同步)
    - [2.12. transient：短暂](#212-transient短暂)
    - [2.13. volatile：易失](#213-volatile易失)
- [3. 程序控制语句](#3-程序控制语句)
    - [3.1. break：跳出循环](#31-break跳出循环)
    - [3.2. case：定义一个值以供switch选择](#32-case定义一个值以供switch选择)
    - [3.3. continue：继续](#33-continue继续)
    - [3.4. default：默认](#34-default默认)
    - [3.5. do：运行](#35-do运行)
    - [3.6. else：否则](#36-else否则)
    - [3.7. for：循环](#37-for循环)
    - [3.8. if：如果](#38-if如果)
    - [3.9. instanceof：实例](#39-instanceof实例)
    - [3.10. return：返回](#310-return返回)
    - [3.11. switch：根据值选择执行](#311-switch根据值选择执行)
    - [3.12. while：循环](#312-while循环)
- [4. 错误处理](#4-错误处理)
    - [4.1. assert：断言表达式是否为真](#41-assert断言表达式是否为真)
    - [4.2. catch：捕捉异常](#42-catch捕捉异常)
    - [4.3. finally：有没有异常都执行](#43-finally有没有异常都执行)
    - [4.4. throw：抛出一个异常对象](#44-throw抛出一个异常对象)
    - [4.5. throws：声明一个异常可能被抛出](#45-throws声明一个异常可能被抛出)
    - [4.6. try：捕获异常](#46-try捕获异常)
- [5. 包相关](#5-包相关)
    - [5.1. import：引入](#51-import引入)
    - [5.2. package：包](#52-package包)
- [6. 基本类型](#6-基本类型)
    - [6.1. boolean：布尔型](#61-boolean布尔型)
    - [6.2. byte：字节型](#62-byte字节型)
    - [6.3. char：字符型](#63-char字符型)
    - [6.4. double：双精度浮点](#64-double双精度浮点)
    - [6.5. float：单精度浮点](#65-float单精度浮点)
    - [6.6. int：整型](#66-int整型)
    - [6.7. long：长整型](#67-long长整型)
    - [6.8. short：短整型](#68-short短整型)
    - [6.9. null：空](#69-null空)
- [7. 变量引用](#7-变量引用)
    - [7.1. super：父类,超类](#71-super父类超类)
    - [7.2. this：本类](#72-this本类)
    - [7.3. void：无返回值](#73-void无返回值)
- [8. 保留关键字](#8-保留关键字)
    - [8.1. goto：是关键字，但不能使用](#81-goto是关键字但不能使用)
    - [8.2. const：是关键字，但不能使用](#82-const是关键字但不能使用)

<!-- /TOC -->

---

Java关键字是内置的一些单词。

这些保留字不能用于常量、变量、和任何标识符的名称。

- [ ] 待完善

<a id="markdown-1-访问控制" name="1-访问控制"></a>
## 1. 访问控制

<a id="markdown-11-private私有的" name="11-private私有的"></a>
### 1.1. private：私有的

<a id="markdown-12-protected受保护的" name="12-protected受保护的"></a>
### 1.2. protected：受保护的

<a id="markdown-13-public公共的" name="13-public公共的"></a>
### 1.3. public：公共的

<a id="markdown-2-类方法和变量修饰符" name="2-类方法和变量修饰符"></a>
## 2. 类、方法和变量修饰符

<a id="markdown-21-abstract声明抽象" name="21-abstract声明抽象"></a>
### 2.1. abstract：声明抽象

<a id="markdown-22-class类" name="22-class类"></a>
### 2.2. class：类

<a id="markdown-23-extends泛化继承" name="23-extends泛化继承"></a>
### 2.3. extends：泛化,继承

<a id="markdown-24-final最终的不可改变的" name="24-final最终的不可改变的"></a>
### 2.4. final：最终的,不可改变的

<a id="markdown-25-implements实现接口" name="25-implements实现接口"></a>
### 2.5. implements：实现（接口）

<a id="markdown-26-interface接口" name="26-interface接口"></a>
### 2.6. interface：接口

<a id="markdown-27-native本地原生方法非java实现" name="27-native本地原生方法非java实现"></a>
### 2.7. native：本地，原生方法（非Java实现）

<a id="markdown-28-new新创建" name="28-new新创建"></a>
### 2.8. new：新,创建

<a id="markdown-29-static静态" name="29-static静态"></a>
### 2.9. static：静态

<a id="markdown-210-strictfp严格精准" name="210-strictfp严格精准"></a>
### 2.10. strictfp：严格,精准

<a id="markdown-211-synchronized线程同步" name="211-synchronized线程同步"></a>
### 2.11. synchronized：线程,同步

<a id="markdown-212-transient短暂" name="212-transient短暂"></a>
### 2.12. transient：短暂

实现Serilizable接口，将不需要序列化的属性前添加关键字transient
序列化对象的时候，这个属性就不会序列化到指定的目的地中

<a id="markdown-213-volatile易失" name="213-volatile易失"></a>
### 2.13. volatile：易失

volatile 声明的变量可以被看作是一种程度较轻的 synchronized

<a id="markdown-3-程序控制语句" name="3-程序控制语句"></a>
## 3. 程序控制语句

<a id="markdown-31-break跳出循环" name="31-break跳出循环"></a>
### 3.1. break：跳出循环

<a id="markdown-32-case定义一个值以供switch选择" name="32-case定义一个值以供switch选择"></a>
### 3.2. case：定义一个值以供switch选择

<a id="markdown-33-continue继续" name="33-continue继续"></a>
### 3.3. continue：继续

<a id="markdown-34-default默认" name="34-default默认"></a>
### 3.4. default：默认

<a id="markdown-35-do运行" name="35-do运行"></a>
### 3.5. do：运行

<a id="markdown-36-else否则" name="36-else否则"></a>
### 3.6. else：否则

<a id="markdown-37-for循环" name="37-for循环"></a>
### 3.7. for：循环

<a id="markdown-38-if如果" name="38-if如果"></a>
### 3.8. if：如果

<a id="markdown-39-instanceof实例" name="39-instanceof实例"></a>
### 3.9. instanceof：实例

<a id="markdown-310-return返回" name="310-return返回"></a>
### 3.10. return：返回

<a id="markdown-311-switch根据值选择执行" name="311-switch根据值选择执行"></a>
### 3.11. switch：根据值选择执行

<a id="markdown-312-while循环" name="312-while循环"></a>
### 3.12. while：循环

<a id="markdown-4-错误处理" name="4-错误处理"></a>
## 4. 错误处理

<a id="markdown-41-assert断言表达式是否为真" name="41-assert断言表达式是否为真"></a>
### 4.1. assert：断言表达式是否为真

<a id="markdown-42-catch捕捉异常" name="42-catch捕捉异常"></a>
### 4.2. catch：捕捉异常

<a id="markdown-43-finally有没有异常都执行" name="43-finally有没有异常都执行"></a>
### 4.3. finally：有没有异常都执行

<a id="markdown-44-throw抛出一个异常对象" name="44-throw抛出一个异常对象"></a>
### 4.4. throw：抛出一个异常对象

<a id="markdown-45-throws声明一个异常可能被抛出" name="45-throws声明一个异常可能被抛出"></a>
### 4.5. throws：声明一个异常可能被抛出

<a id="markdown-46-try捕获异常" name="46-try捕获异常"></a>
### 4.6. try：捕获异常

<a id="markdown-5-包相关" name="5-包相关"></a>
## 5. 包相关

<a id="markdown-51-import引入" name="51-import引入"></a>
### 5.1. import：引入

<a id="markdown-52-package包" name="52-package包"></a>
### 5.2. package：包

<a id="markdown-6-基本类型" name="6-基本类型"></a>
## 6. 基本类型

<a id="markdown-61-boolean布尔型" name="61-boolean布尔型"></a>
### 6.1. boolean：布尔型

<a id="markdown-62-byte字节型" name="62-byte字节型"></a>
### 6.2. byte：字节型

<a id="markdown-63-char字符型" name="63-char字符型"></a>
### 6.3. char：字符型

<a id="markdown-64-double双精度浮点" name="64-double双精度浮点"></a>
### 6.4. double：双精度浮点

<a id="markdown-65-float单精度浮点" name="65-float单精度浮点"></a>
### 6.5. float：单精度浮点

<a id="markdown-66-int整型" name="66-int整型"></a>
### 6.6. int：整型

<a id="markdown-67-long长整型" name="67-long长整型"></a>
### 6.7. long：长整型

<a id="markdown-68-short短整型" name="68-short短整型"></a>
### 6.8. short：短整型

<a id="markdown-69-null空" name="69-null空"></a>
### 6.9. null：空

Java 中 null 是一种特殊的类型，表示空，一般用于引用类型。

在 Java 中的所有引用类型，默认值都是 null 。

但是我们不能讲 null 赋值给基本数据类型，比如 int、boolean 等类型。

null 不会在内存中占用空间，尤其注意和空字符串的区别。

例如：

```java
String s1 = "";    // 表示空字符串
String s2 = null;  // 表示空引用
```

<a id="markdown-7-变量引用" name="7-变量引用"></a>
## 7. 变量引用

<a id="markdown-71-super父类超类" name="71-super父类超类"></a>
### 7.1. super：父类,超类

<a id="markdown-72-this本类" name="72-this本类"></a>
### 7.2. this：本类

<a id="markdown-73-void无返回值" name="73-void无返回值"></a>
### 7.3. void：无返回值

<a id="markdown-8-保留关键字" name="8-保留关键字"></a>
## 8. 保留关键字

<a id="markdown-81-goto是关键字但不能使用" name="81-goto是关键字但不能使用"></a>
### 8.1. goto：是关键字，但不能使用

<a id="markdown-82-const是关键字但不能使用" name="82-const是关键字但不能使用"></a>
### 8.2. const：是关键字，但不能使用