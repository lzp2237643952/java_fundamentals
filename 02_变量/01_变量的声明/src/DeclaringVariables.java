public class DeclaringVariables {

	public static void main(String[] args) {
		// 1、变量的声明
		int a; // 声明一个整型的变量，名为a
		int b, c, d; // 声明三个整型的变量，名为b,c,d

		// 2.变量的初始化:第一次赋值
		int e = 250; // 声明整型变量e并赋值为250
		int f; // 声明整型变量f
		f = 250; // 给变量f赋值为250

		// 3、一条语句中声明多个同类型的变量
		double q, p, l;// 声明浮点型的变量，名为q,p,l

		// 4、声明的同时初始化
		int k = 250;// 声明整型变量k并赋值为250

		// 5、先声明后初始化
		int age; // 声明整型变量age
		age = 18; // 给变量age赋值为18
		System.out.println("我永远" + age + "岁");
	}

}
