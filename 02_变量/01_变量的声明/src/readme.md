# 02_01_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [DeclaringVariables.java](DeclaringVariables.java)

**概要**

变量的声明

**学习目标**

- 声明类型
- 初始化赋值