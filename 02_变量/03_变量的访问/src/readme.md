# 02_03_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [AccessToVariables.java](AccessToVariables.java)

**概要**

变量的访问

**学习目标**

- 学会编译正确的java代码
- 学会如何给变量赋值