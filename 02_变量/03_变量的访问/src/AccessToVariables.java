public class AccessToVariables {
	public static void main(String[] args) {
		// 变量的访问:
		int g = 5;
		int h = g + 10; // 取出g的值5，加10后，再赋值给整型变量h
		System.out.println(h); // 输出变量h的值15
		System.out.println("h"); // 输出h，双引号中的原样输出
		g = g + 10; // 取出g的值5，加10后，再赋值给g
					// 在g的本身基础之上增10
		System.out.println(g); // 15

		// int i = 3.14; //编译错误，数据类型不匹配
		// System.out.println(m); //编译错误，m未声明
		int m;
		// System.out.println(m); //编译错误，m未初始化
	}

}
