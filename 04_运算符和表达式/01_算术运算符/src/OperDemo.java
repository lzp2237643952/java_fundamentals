// 运算符的演示(取模、++、--)
public class OperDemo {
	public static void main(String[] args) {
		/*
		 * % 取模/取余, 余数为0即为整数
		 */

		System.out.println(5 % 2); // 1, 商2余1
		System.out.println(8 % 2); // 0, 商4余0--整除
		System.out.println(2 % 8); // 2, 商0余2

		/*
		 * ++/-- 自增1/自减1，可在变量前也可在变量后 单独使用时，在前在后没有差别
		 */

		int a = 5, b = 5;
		a++; // 相当于a=a+1;
		++b; // 相当于b=b+1
		System.out.println(a); // 6
		System.out.println(b); // 6

		/*
		 * ++/-- 被使用时，在前在后有差别: a++的值为a ++a的值为a+1
		 */

		int a1 = 5, b1 = 5;
		int c = a1++; // 1、将a++的值5赋值给c 2、a自增1变为6
		int d = ++b1; // 1、将++b的值6赋值给d 2、b自增1变为6

		System.out.println(a1); // 6
		System.out.println(b1); // 6
		System.out.println(c); // 5
		System.out.println(d); // 6

		/*
		 * 被使用时,在前在后有差别 a--的值为a --a的值为a-1
		 */

		int a2 = 5, b2 = 5;
		// 1、输出a--的值为5
		// 2、a自减1变成4
		System.out.println(a2--); // 5
		System.out.println(a2); // 4

		// 1、输出--b的值4
		// 2、b自减1变成4
		System.out.println(--b2); // 4
		System.out.println(b2); // 4
	}

}
