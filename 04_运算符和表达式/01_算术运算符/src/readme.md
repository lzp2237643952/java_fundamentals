# 04_01_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [OperDemo.java](OperDemo.java)

**概要**

运算符(取模、++、--)的演示

**学习目标**

- 取模运算时的舍入问题
- ++、--在前在后的差别
