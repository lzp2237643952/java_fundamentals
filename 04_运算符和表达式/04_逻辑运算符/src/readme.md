# 04_04_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [LogicAndDemo.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F/04_%E9%80%BB%E8%BE%91%E8%BF%90%E7%AE%97%E7%AC%A6/src/LogicAndDemo.java)

**概要**

运算符'&&'的演示

**学习目标**

* `&&`的运算规则
* 两边都为真则为真,见false则false

## [LogicOrDemo.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F/04_%E9%80%BB%E8%BE%91%E8%BF%90%E7%AE%97%E7%AC%A6/src/LogicOrDemo.java)

**概要**

运算符'||'的演示

**学习目标**

* `||`的运算规则
* 有一边为真则为真,见true则true

## [LogicNotDemo.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F/04_%E9%80%BB%E8%BE%91%E8%BF%90%E7%AE%97%E7%AC%A6/src/LogicNotDemo.java)

**概要**

运算符'!'的演示

**学习目标**

* `!`的运算规则
* 非真则假,非假则真