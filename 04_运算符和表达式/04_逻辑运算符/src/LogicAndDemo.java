/**
 * 逻辑与的代码演示
 */
public class LogicAndDemo {
    public static void main(String[] args) {
        /*
         * &&:逻辑与(并且), 两边都为真则为真, 见false则false
         */
        int a = 5, b = 10, c = 5;
       

        // true && false = false 例如：
        System.out.println(b >= a && c > b);
        //也就是 10>=5(true) && 5>10(false) = false

        // false && true = false 例如：
        System.out.println(b <= c && a < b);
        //也就是 10 <= 5(false) && 5 < 10(true) = false

        // false && false = false 例如：
        System.out.println(a == b && c > b);
        //也就是 5==10(false) && 5>10 = false

        // true&&true=true 例如：
        System.out.println(c != b && a < b);
        // 也就是 5!=10(true) && 5<10(true) = true
    }
}