/**
 * 逻辑非的代码演示
 */
public class LogicNotDemo {
    public static void main(String[] args) {
        /*
        * !:逻辑非(取反), 非真则假, 非假则真
        */

        int a = 5, b = 10;

         //!false=true 例如：
        System.out.println(!(a>b));
        //也就是 !(5>10)=true

        //!true=false 例如：
        System.out.println(!(a<b)); 
        //也就是 !(5<10)=false
    }

    
}