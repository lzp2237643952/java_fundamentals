/**
 * 逻辑或的代码演示
 */
public class LogicOrDemo {
    public static void main(String[] args) {
        /*
        *  ||:逻辑或(或者), 有一边真则为真, 出现一个true则true
        */
        
        int a = 5, b = 10, c = 5;

        
        //  true || false = true  例如：
        System.out.println(b>=a || c>b); 
        // 也就是 10>=5(true) || 5>10(false) = true

        //false || true = true  例如：
        System.out.println(b<=c|| a<b); 
        // 也就是 10<=5(false) || 5<10(true) = true

        //true || true = true 例如：
        System.out.println(c!=b || a<b);
        // 也就是 5!=10(true) || 5<10(true) = true

        //false || false = false 例如：
        System.out.println(a==b || c>b); 
        // 也就是 5==10(false) || 5>10(false) = false
    }

    
}