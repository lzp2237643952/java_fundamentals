# 04_03_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [RelationalOperator.java](RelationalOperator.java)

### 概要

关系运算符的演示

### 学习目标

* 关系运算符的分类
* 关系运算符的结果