//关系运算符的演示
public class RelationalOperator {
	public static void main(String[] args) {
		/*
		 * 关系运算符包括：
		 * >(大于)
		 * <(小于) 
		 * >=(大于或等于)
		 * <=(小于或等于) 
		 * ==(等于)
		 * !=(不等于)
		 * 
		 * 关系运算的结果为boolean型，关系成立则为true，关系不成立则为false
		 */
		int a = 5, b = 10, c = 5;
		boolean b1 = a > b;
		
		System.out.println(b1); // false
		System.out.println(c < b); // true
		System.out.println(a >= c); // true
		System.out.println(c <= b); // true
		System.out.println(a == c); // true
		System.out.println(a != c); // false

	}

}
