//字符串拼接
public class OperDemo4 {
	public static void main(String[] args) {
		int age = 25;
		System.out.println("age="); // age=
		System.out.println(age); // 25
		System.out.println("age=" + age); // age=25
		System.out.println("我的年龄是:" + age );
		System.out.println("我今年" + age + " 岁 了");

		System.out.println(10 + 20 + "" + 30); // "3030"
		System.out.println(10 + 20 + 30 + ""); // "60"
		System.out.println("" + 10 + 20 + 30); // "102030"
	}

}
