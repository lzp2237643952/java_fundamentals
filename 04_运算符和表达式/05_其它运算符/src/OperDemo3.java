//条件(三目)运算符
public class OperDemo3 {
	public static void main(String[] args) {
		int a = 5;
		int flag = a < 0 ? a : 6;
		System.out.println(flag);  // 6
	}
}
