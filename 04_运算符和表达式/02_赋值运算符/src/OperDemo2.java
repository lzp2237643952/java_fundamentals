//赋值运算符
public class OperDemo2 {
	public static void main(String[] args) {

		int a = 5; // 定义整形变量a, 并赋值为 5

		// 相当于a=(int)(a+5)
		// 注意在使用复合运算符的时候, 内置了类型转换
		a += 5;
		System.out.println(a); // 10

		a *= 3;
		System.out.println(a); // 30

		a /= 6;
		System.out.println(a); // 5
	}

}
