# README

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC
---

## 项目介绍

Java 基础教程和案例, 根据达内线下课程全新录制。

## 项目结构

- [01_运行环境](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/01_运行环境)
    - [01_操作系统](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/01_运行环境/01_操作系统)
    - [02_运行环境](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/01_运行环境/02_运行环境)
    - [03_开发工具](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/01_运行环境/03_开发工具)
- [02_变量](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/02_变量)
    - [01_变量的声明](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/02_变量/01_变量的声明)
    - [02_变量的命名](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/02_变量/02_变量的命名)
    - [03_变量的访问](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/02_变量/03_变量的访问)
- [03_基本数据类型](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/03_基本数据类型)
    - [01_数值类型](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/03_基本数据类型/01_数值类型)
    - [02_字符类型](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/03_基本数据类型/02_字符类型)
    - [03_布尔类型](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/03_基本数据类型/03_布尔类型)
    - [04_类型转换](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/03_基本数据类型/04_类型转换)
- [04_运算符和表达式](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F)
    - [01_算术运算符](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F/01_算术运算符)
    - [02_赋值运算符](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F/02_赋值运算符)
    - [03_关系运算符](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F/03_关系运算符)
    - [04_逻辑运算符](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F/04_逻辑运算符)
    - [05_其它运算符](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F/05_其它运算符)
- [05_分支结构](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/05_分支结构)
    - [01_if语句](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/05_分支结构/01_if语句)
    - [02_if_else语句](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/05_分支结构/02_if_else语句)
    - [03_else_if语句](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/05_分支结构/03_else_if语句)
    - [04_switch语句](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/05_分支结构/04_switch语句)
    - [05_分支嵌套](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/05_分支结构/05_分支嵌套)
- [06_循环结构](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/06_循环结构)
    - [01_while循环结构](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/06_循环结构/01_while循环结构)
    - [02_do_while循环结构](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/06_循环结构/02_do_while循环结构)
    - [03_for循环结构](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/06_循环结构/03_for循环结构)
    - [04_循环嵌套](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/06_循环结构/04_循环嵌套)
    - [05_循环控制](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/06_循环结构/05_循环控制)
- [07_数组](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/07_数组)
    - [01_数组的定义](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/07_数组/01_数组的定义)
    - [02_数组的访问](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/07_数组/02_数组的访问)
    - [03_数组的复制](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/07_数组/03_数组的复制)
    - [04_数组的排序](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/07_数组/04_数组的排序)
- [08_方法](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/08_方法)
    - [01_方法的定义](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/08_方法/01_方法的定义)
    - [02_方法的调用](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/08_方法/02_方法的调用)
    - [03_方法的参数](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/08_方法/03_方法的参数)
- [09_项目案例](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/09_项目案例)
- [README.md](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/README.md)

## 录播视频

- 001_Java编译运行过程
    - [腾讯视频](http://v.qq.com/x/page/i0646vi0kii.html)
    - [B站视频](https://www.bilibili.com/video/av23106417/?p=1)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTY1OTczMg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn92o9d.html#vfrm=8-8-0-1)

- 002_JVMJREJDK名词
    - [腾讯视频](https://v.qq.com/x/page/u0646gdr8yx.html)
    - [B站视频](https://www.bilibili.com/video/av23106417/?p=2)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU1MTU4NzIyMA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry84nzq1.html#vfrm=8-8-0-1)

- 003_安装JDK
    - [腾讯视频](https://v.qq.com/x/page/d0646tvbhko.html)
    - [B站视频](https://www.bilibili.com/video/av23106417/?p=3)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU1MTU4NzI0OA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry84uyex.html#vfrm=8-8-0-1)

- 004_配置环境变量
   - [腾讯视频](https://v.qq.com/x/page/p0646h2bf1u.html)
   - [B站视频](https://www.bilibili.com/video/av23106417/?p=4)
   - [优酷视频](https://v.youku.com/v_show/id_XMzU1MTU5NTI4OA==.html?spm=a2hzp.8253869.0.0)
   - [爱奇艺视频](http://www.iqiyi.com/w_19ry84p1d1.html#vfrm=8-8-0-1)

- 005_运用cmd编写HelloWorld
    - [腾讯视频](https://v.qq.com/x/page/m06469cbicl.html)
    - [B站视频](https://www.bilibili.com/video/av23106417/?p=5)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU1MTU3NTA2OA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry84auc1.html#vfrm=8-8-0-1)

- 006_Eclipse说明
    - [腾讯视频](https://v.qq.com/x/page/p0646olpbpl.html)
    - [B站视频](https://www.bilibili.com/video/av23106417/?p=6)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU1MTU3NTA5Ng==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry84js59.html#vfrm=8-8-0-1)

- 007_运用eclipse编写HelloWorld01
    - [腾讯视频](https://v.qq.com/x/page/i0646caggpb.html)
    - [B站视频](https://www.bilibili.com/video/av23106417/?p=7)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU1MTU3NTExMg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry85eaf9.html#vfrm=8-8-0-1)

- 008_运用eclipse编写HelloWorld02
    - [腾讯视频](https://v.qq.com/x/page/q0646314si6.html)
    - [B站视频](https://www.bilibili.com/video/av23106417/?p=8)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU1MTU3Njc3Ng==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry84ardt.html#vfrm=8-8-0-1)

- 009_创建项目结构
    - [腾讯视频](https://v.qq.com/x/page/v0646mcr40f.html)
    - [B站视频](https://www.bilibili.com/video/av23106417/?p=9)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU1MTU4MDI2NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry84jtf9.html#vfrm=8-8-0-1)

- 010_变量的引入
    - [腾讯视频](https://v.qq.com/x/page/l0646ny2bib.html)
    - [B站视频](https://www.bilibili.com/video/av23106970/?p=1)
    - [优酷视频](https://v.youku.com/v_show/id_XMzQ3NTU1ODg2MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rwr63itp.html#vfrm=8-8-0-1)

- 011_什么是变量
    - [腾讯视频](https://v.qq.com/x/page/o06060den8e.html)
    - [B站视频](https://www.bilibili.com/video/av23106970/?p=2)
    - [优酷视频](https://v.youku.com/v_show/id_XMzQ3NTU2NTgwMA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rwr65i6h.html#vfrm=8-8-0-1)

- 012_变量的声明和初始化
    - [腾讯视频](https://v.qq.com/x/page/w06069zb2qu.html)
    - [B站视频](https://www.bilibili.com/video/av23106970/?p=3)
    - [优酷视频](https://v.youku.com/v_show/id_XMzQ3NTU2NTk3Ng==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rwr68fl1.html#vfrm=8-8-0-1)

- 013_变量的访问
    - [腾讯视频](https://v.qq.com/x/page/u06061wohrg.html)
    - [B站视频](https://www.bilibili.com/video/av23106970/?p=4)
    - [优酷视频](https://v.youku.com/v_show/id_XMzQ3NTU2NTk3Mg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rwrnmlcp.html#vfrm=8-8-0-1)

- 014_Linux基本操作命令
    - [B站视频](https://www.bilibili.com/video/av23106970/?p=5)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDMxOTM0MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlzgcyt.html#vfrm=8-8-0-1)

- 015_变量的命名
    - [腾讯视频](https://v.qq.com/x/page/j06064yhhre.html)
    - [B站视频](https://www.bilibili.com/video/av23106970/?p=6)
    - [优酷视频](https://v.youku.com/v_show/id_XMzQ3NTU2NTc3Mg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rwr53jdp.html#vfrm=8-8-0-1)

- 016_变量命名的演示
    - [腾讯视频](http://v.qq.com/x/page/b0646fohq86.html)
    - [B站视频](https://www.bilibili.com/video/av23106970/?p=7)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDMyMzU5Ng==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlyn3kp.html#vfrm=8-8-0-1)

- 017_8种基本数据类型
    - [腾讯视频](https://v.qq.com/x/page/t061582oco1.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=1)
    - [优酷视频](https://v.youku.com/v_show/id_XMzQ3NTU2NTk4MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rwrnkoo5.html#vfrm=8-8-0-1)

- 018_int类型
    - [腾讯视频](https://v.qq.com/x/page/g0615o2igmh.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=2)
    - [优酷视频](https://v.youku.com/v_show/id_XMzQ3NTU2NjYxMg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rwrnnx0p.html#vfrm=8-8-0-1)

- 019_long类型
    - [腾讯视频](https://v.qq.com/x/page/q06158xw5vl.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=3)
    - [优酷视频](https://v.youku.com/v_show/id_XMzQ3NTU2ODE2NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rwrnql0h.html#vfrm=8-8-0-1)

- 020_double类型的概念
    - [腾讯视频](https://v.qq.com/x/page/q0646ii5ipe.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=4)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDM1MzU4NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlyrzvp.html#vfrm=8-8-0-1)

- 021_double类型的代码演示
    - [腾讯视频](https://v.qq.com/x/page/t0646991vjs.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=5)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTYyMTc0OA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlxz4o5.html#vfrm=8-8-0-1)

- 022_boolean类型的概念
    - [腾讯视频](https://v.qq.com/x/page/r0615op5pt5.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=6)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDM1NjE2NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlxvvh5.html#vfrm=8-8-0-1)

- 023_char类型的概念01
    - [腾讯视频](https://v.qq.com/x/page/s0646vwf2qt.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=7)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDM2NTI0MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxly4fbd.html#vfrm=8-8-0-1)

- 024_char类型的概念02
    - [腾讯视频](https://v.qq.com/x/page/x0646duuiuc.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=8)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDM2NjAyNA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxly3w5h.html#vfrm=8-8-0-1)

- 025_char类型转义符
    - [腾讯视频](https://v.qq.com/x/page/h0646xh2qll.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=9)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDM3NTA4OA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxly4gfd.html#vfrm=8-8-0-1)

- 026_基本数据类型的转换
    - [腾讯视频](https://v.qq.com/x/page/h06467jb1ie.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=10)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTczMTk4OA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxly4ubt.html#vfrm=8-8-0-1)

- 027_基本数据类型的转换两点规则
    - [腾讯视频](https://v.qq.com/x/page/c0615u3uc24.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=11)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTczMTg3Ng==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlxzt3t.html#vfrm=8-8-0-1)

- 028_基本数据类型的转换代码演示
    - [腾讯视频](https://v.qq.com/x/page/d0646f14xtv.html)
    - [B站视频](https://www.bilibili.com/video/av23108274/?p=12)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTczMTk4NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxly5ckd.html#vfrm=8-8-0-1)

- 029_算术运算符的概念
    - [腾讯视频](https://v.qq.com/x/page/g06243fgkzb.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=1)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTczNzcyMA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlxbudt.html#vfrm=8-8-0-1)

- 030_算术运算符自增自减使用
    - [腾讯视频](https://v.qq.com/x/page/a0624apjbjg.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=2)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTczNzc4NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxly7zcp.html#vfrm=8-8-0-1)

- 031_算术运算符自增自减被使用
    - [腾讯视频](https://v.qq.com/x/page/n0624rm0qq4.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=3)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc0MDE3Mg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxly4dvx.html#vfrm=8-8-0-1)

- 032_算术运算符代码演示
    - [腾讯视频](https://v.qq.com/x/page/n0624ndnewl.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=4)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc0NDIwOA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxly6011.html#vfrm=8-8-0-1)

- 033_关系运算符的概念
    - [腾讯视频](https://v.qq.com/x/page/g0624epor8a.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=5)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc1NzA4OA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxly4os5.html#vfrm=8-8-0-1)

- 034_关系运算符的代码演示
    - [腾讯视频](https://v.qq.com/x/page/v0624x1q7d5.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=6)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc0Njg0NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlxbt5t.html#vfrm=8-8-0-1)

- 035_Scanner接收用户的输入
    - [腾讯视频](https://v.qq.com/x/page/d0615vf992u.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=7)
    - [优酷视频](https://v.youku.com/v_show/id_XMzQ3NTU2OTk5Ng==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rwr5guqp.html#vfrm=8-8-0-1)

- 036_逻辑运算符的概念
    - [腾讯视频](https://v.qq.com/x/page/i0624tbdrcq.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=8)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU4MDk0NDA1Mg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19u92t.html#vfrm=8-8-0-1)

- 037_逻辑运算符的代码演示
    - [腾讯视频](https://v.qq.com/x/page/p0624pio3b5.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=9)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU4MDkzMDg3Mg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry1ag2dx.html#vfrm=8-8-0-1)

- 038_逻辑运算符的短路
    - [腾讯视频](https://v.qq.com/x/page/k0624g63i4g.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=10)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU4MDkzMDc1Mg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19ypl5.html#vfrm=8-8-0-1)

- 039_赋值运算符
    - [腾讯视频](https://v.qq.com/x/page/b06247fvrz0.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=11)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU4MDkzNzgyMA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19u2rp.html#vfrm=8-8-0-1)

- 040_字符串连接运算符
    - [腾讯视频](https://v.qq.com/x/page/n062485knvd.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=12)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU4MDk1OTc2MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry1aiod9.html#vfrm=8-8-0-1)

- 041_条件运算符
    - [腾讯视频](https://v.qq.com/x/page/k0624jl6vqc.html)
    - [B站视频](https://www.bilibili.com/video/av23111999/?p=13)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU4MDk0ODI4NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19ymep.html#vfrm=8-8-0-1)

- 042_分支结构案例引入01
    - [腾讯视频](https://v.qq.com/x/page/g0624whvt0y.html)
    - [B站视频](https://www.bilibili.com/video/av23112425/?p=1)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc1MDgxNg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19u23x.html#vfrm=8-8-0-1)

- 043_分支结构案例引入02
    - [腾讯视频](https://v.qq.com/x/page/q0624q5op67.html)
    - [B站视频](https://www.bilibili.com/video/av23112425/?p=2)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc1MTMwOA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19yo5l.html#vfrm=8-8-0-1)

- 044_分支结构之if语句
    - [腾讯视频](https://v.qq.com/x/page/w0624ahlb0l.html)
    - [B站视频](https://www.bilibili.com/video/av23112425/?p=3)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc1NzczMg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19yo5l.html#vfrm=8-8-0-1)

- 045_分支结构if案例
    - [腾讯视频](https://v.qq.com/x/page/v062461pbim.html)
    - [B站视频](https://www.bilibili.com/video/av23112425/?p=4)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc1MjA3Ng==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry1a5ejt.html#vfrm=8-8-0-1)

- 046_分支结构之if-else
    - [腾讯视频](https://v.qq.com/x/page/d0624f3xqpk.html)
    - [B站视频](https://www.bilibili.com/video/av23112425/?p=5)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc2MTE1Mg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry1a1ugx.html#vfrm=8-8-0-1)

- 047_分支结构if-else案例
    - [腾讯视频](https://v.qq.com/x/page/r06240awlhi.html)
    - [B站视频](https://www.bilibili.com/video/av23112425/?p=6)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry1a5ejt.html#vfrm=8-8-0-1)

- 048_分支结构switch-case的概念
    - [腾讯视频](https://v.qq.com/x/page/v064640sb0j.html)
    - [B站视频](https://www.bilibili.com/video/av23112425/?p=7)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc2NjQxNg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19ev3t.html#vfrm=8-8-0-1)

- 049_分支结构switch-case的break关键字
    - [腾讯视频](https://v.qq.com/x/page/h06468sgxjj.html)
    - [B站视频](https://www.bilibili.com/video/av23112425/?p=8)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc2ODE4MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19zpqx.html#vfrm=8-8-0-1)

- 050_分支结构switch-case代码演示
    - [腾讯视频](https://v.qq.com/x/page/v0646w6skaf.html)
    - [B站视频](https://www.bilibili.com/video/av23112425/?p=9)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc4MTU0MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19ffrh.html#vfrm=8-8-0-1)

- 051_循环结构while概念
    - [腾讯视频](https://v.qq.com/x/page/x0631m6yr16.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=1)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDA1ODAyOA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19ry19w53x.html#vfrm=8-8-0-1)

- 052_循环结构while循环三要素
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=2)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDE2NTA2MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxmp53gx.html#vfrm=8-8-0-1)

- 053_循环结构while的执行逻辑
    - [腾讯视频](https://v.qq.com/x/page/x0631m6yr16.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=3)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDA1NzgxMg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlxvetx.html#vfrm=8-8-0-1)

- 054_循环结构while之break关键字
    - [腾讯视频](https://v.qq.com/x/page/r0631eg44jg.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=4)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDA2OTE2NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxlxvenh.html#vfrm=8-8-0-1)

- 055_循环结构while代码演示
    - [腾讯视频](https://v.qq.com/x/page/x06462dxlxa.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=5)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDA3MTIyNA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxmofzkt.html#vfrm=8-8-0-1)

- 056_循环结构while的注意事项01
    - [腾讯视频](https://v.qq.com/x/page/k06318cu9ri.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=6)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDA3NDg4OA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxmni49h.html#vfrm=8-8-0-1)

- 057_循环结构while的注意事项02
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=7)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MTc4MTcxNg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxmnhoxh.html#vfrm=8-8-0-1)

- 058_循环结构dowhile的概念
    - [腾讯视频](https://v.qq.com/x/page/w0631r353ct.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=8)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDE2NDg0MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxmc66hx.html#vfrm=8-8-0-1)

- 059_循环结构dowhile的代码分析01
    - [腾讯视频](https://v.qq.com/x/page/c0631xyyxfl.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=9)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDA3Nzg2NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxmcny6l.html#vfrm=8-8-0-1)

- 060_循环结构dowhile的代码演示02
    - [腾讯视频](https://v.qq.com/x/page/i06460hflcx.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=10)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDE3NDI3Ng==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxmi7nst.html#vfrm=8-8-0-1)

- 061_循环结构for的概念
    - [腾讯视频](https://v.qq.com/x/page/s0646atri2x.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=11)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDE5NDE4NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn9fk85.html#vfrm=8-8-0-1)

- 062_循环结构for代码案例求1-100的累加和
    - [腾讯视频](https://v.qq.com/x/page/q0646o7jfwo.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=12)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxna7u9t.html#vfrm=8-8-0-1)

- 063_循环结构for的特殊样式
    - [腾讯视频](https://v.qq.com/x/page/j0646kbi25d.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=13)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxnaakbh.html#vfrm=8-8-0-1)

- 064_循环结构for案例随机加法运算器01
    - [腾讯视频](https://v.qq.com/x/page/f0631m74wf3.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=14)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDIwMTQ0NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn8m0rd.html#vfrm=8-8-0-1)

- 065_循环结构for案例随机加法运算器02
    - [腾讯视频](https://v.qq.com/x/page/e06460ub1by.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=15)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDIwNDY4MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn98amd.html#vfrm=8-8-0-1)

- 066_循环结构for案例随机加法运算器03
    - [腾讯视频](https://v.qq.com/x/page/m0631mlafjt.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=16)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDIwNTM1Mg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn8zpcp.html#vfrm=8-8-0-1)

- 067_循环结构for中常用的关键字break和continue
    - [腾讯视频](https://v.qq.com/x/page/y0631bjf0cq.html)
    - [B站视频](https://www.bilibili.com/video/av23114193/?p=17)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDIwNzI3Ng==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn9fzip.html#vfrm=8-8-0-1)

- 068_循环问题
    - [腾讯视频](https://v.qq.com/x/page/r06329guhqi.html)
    - [B站视频](https://www.bilibili.com/video/av23114187/?p=1)
    - [爱奇艺视频](https://v.youku.com/v_show/id_XMzU5MDIwOTE0OA==.html?spm=a2hzp.8253869.0.0)

- 069_嵌套循环案例九九乘法表01
    - [腾讯视频](https://v.qq.com/x/page/v0632lfowiq.html)
    - [B站视频](https://www.bilibili.com/video/av23114187/?p=2)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn8wee1.html#vfrm=8-8-0-1)

- 070_嵌套循环案例九九乘法表02
    - [腾讯视频](https://v.qq.com/x/page/b0632jp79fw.html)
    - [B站视频](https://www.bilibili.com/video/av23114187/?p=3)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn909u1.html#vfrm=8-8-0-1)

- 071_嵌套循环案例九九乘法表03
    - [腾讯视频](https://v.qq.com/x/page/b06469n3f18.html)
    - [B站视频](https://www.bilibili.com/video/av23114187/?p=4)
    - [爱奇艺视频](https://v.youku.com/v_show/id_XMzU5MDIxNTM1Ng==.html?spm=a2hzp.8253869.0.0)
    - [优酷视频](http://www.iqiyi.com/w_19rxn9c7kh.html#vfrm=8-8-0-1)

- 072_嵌套循环案例九九乘法表04
    - [腾讯视频](https://v.qq.com/x/page/v06462hw76w.html)
    - [B站视频](https://www.bilibili.com/video/av23114187/?p=5)
    - [爱奇艺视频](https://v.youku.com/v_show/id_XMzU5MDIxOTMzMg==.html?spm=a2hzp.8253869.0.0)
    - [优酷视频](http://www.iqiyi.com/w_19rxn9fa2x.html#vfrm=8-8-0-1)

- 073_嵌套循环案例九九乘法表05
    - [腾讯视频](https://v.qq.com/x/page/a0646p4ij5i.html)
    - [B站视频](https://www.bilibili.com/video/av23114187/?p=6)
    - [爱奇艺视频](https://v.youku.com/v_show/id_XMzU5MDIyMDI0MA==.html?spm=a2hzp.8253869.0.0)
    - [优酷视频](http://www.iqiyi.com/w_19rxn9f6nd.html#vfrm=8-8-0-1)

- 074_数组的概念01
    - [腾讯视频](https://v.qq.com/x/page/m06323kwav1.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=1)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn85xdl.html#vfrm=8-8-0-1)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI1NTU3Mg==.html?spm=a2hzp.8253869.0.0)

- 075_数组的概念02
    - [腾讯视频](https://v.qq.com/x/page/d0632mm4qw4.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=2)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn85xdl.html#vfrm=8-8-0-1)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI1ODQ4NA==.html?spm=a2hzp.8253869.0.0)

- 076_数组的定义代码演示01
    - [腾讯视频](https://v.qq.com/x/page/z06322165ns.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=3)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI1NTU2NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxnabewl.html#vfrm=8-8-0-1)

- 077_数组的定义代码演示02
    - [腾讯视频](http://v.qq.com/x/page/e0646ujcse3.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=4)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI1ODQ4NA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn8xsxt.html#vfrm=8-8-0-1)

- 078_数组的初始化
    - [腾讯视频](https://v.qq.com/x/page/b0632ykewfj.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=5)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI2MDYwMA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn905nt.html#vfrm=8-8-0-1)

- 079_数组的初始化代码演示
    - [腾讯视频](https://v.qq.com/x/page/o0632e171po.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=6)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU0Mzk2OA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn85ro1.html#vfrm=8-8-0-1)

- 080_数组的访问
    - [腾讯视频](https://v.qq.com/x/page/z06338tfa5s.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=7)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI4NDkyOA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn8ocll.html#vfrm=8-8-0-1)

- 081_通过下标获取数组元素
    - [腾讯视频](https://v.qq.com/x/page/w0633g89moe.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=8)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI4NDcxMg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn9257p.html#vfrm=8-8-0-1)

- 082_数组的遍历演示01
    - [腾讯视频](https://v.qq.com/x/page/n0633fmgthc.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=9)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI4NDczMg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn85ukh.html#vfrm=8-8-0-1)

- 083_数组的遍历演示02
    - [腾讯视频](https://v.qq.com/x/page/p06330pddmy.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=10)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI5OTk5Mg==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn921bh.html#vfrm=8-8-0-1)

- 084_求数组最大值
    - [腾讯视频](https://v.qq.com/x/page/g0633hu2atd.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=11)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDMwMTE0MA==.html?spm=a2hzp.8253869.0.0)
    -[爱奇艺视频](http://www.iqiyi.com/w_19rxn7ezex.html#vfrm=8-8-0-1)

- 085_数组的排序Arrays.sort
    - [腾讯视频](https://v.qq.com/x/page/a06460nfawx.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=12)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn91vrh.html#vfrm=8-8-0-1)

- 086_数组的冒泡排序01
    - [腾讯视频](https://v.qq.com/x/page/e06463h0urx.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=13)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ1NTE0MA==.html?spm=a2hzp.8253869.0.0)
    - [爱奇艺视频](http://www.iqiyi.com/w_19rxn85kjl.html#vfrm=8-8-0-1)

- 087_数组的冒泡排序02
    - [腾讯视频](https://v.qq.com/x/page/l0646ojkg3n.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=14)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ1NTEyNA==.html?spm=a2hzp.8253869.0.0)

- 088_数组的冒泡排序03
    - [腾讯视频](https://v.qq.com/x/page/p0646mktid1.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=15)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ1NzEzNg==.html?spm=a2hzp.8253869.0.0)

- 089_数组的冒泡排序04
    - [腾讯视频](https://v.qq.com/x/page/t0646yijmga.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=16)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ1OTY2OA==.html?spm=a2hzp.8253869.0.0)

- 090_数组的冒泡排序05
    - [腾讯视频](https://v.qq.com/x/page/s0646gegsci.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=17)

- 091_数组的复制的引入
    - [腾讯视频](https://v.qq.com/x/page/d0633ljq1x9.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=18)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ2NDAxMg==.html?spm=a2hzp.8253869.0.0)

- 092_数组的复制System.Arraycopy01
    - [腾讯视频](https://v.qq.com/x/page/w0633vwox7y.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=19)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU0NDg4OA==.html?spm=a2hzp.8253869.0.0)

- 093_数组的复制System.Arraycopy02
    - [腾讯视频](https://v.qq.com/x/page/u0633xq4cec.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=20)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ2NTY2OA==.html?spm=a2hzp.8253869.0.0)

- 094_数组的复制Arrays.copyof01
    - [腾讯视频](https://v.qq.com/x/page/d06333yn82y.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=21)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU0NjA4MA==.html?spm=a2hzp.8253869.0.0)

- 095_数组的复制Arrays.copyof02
    - [腾讯视频](https://v.qq.com/x/page/r0633vdizef.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=22)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ2ODEyOA==.html?spm=a2hzp.8253869.0.0)

- 096_数组的复制最大值放在最后一个元素
    - [腾讯视频](https://v.qq.com/x/page/f0633n42p9v.html)
    - [B站视频](https://www.bilibili.com/video/av23114182/?p=23)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ2ODg4OA==.html?spm=a2hzp.8253869.0.0)

- 097_方法的引入01
    - [腾讯视频](https://v.qq.com/x/page/w0637h8y478.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=1)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ3MTE3Ng==.html?spm=a2hzp.8253869.0.0)

- 098_方法的定义02
    - [腾讯视频](https://v.qq.com/x/page/j0638w7ibj7.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=2)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ3MjA5Ng==.html?spm=a2hzp.8253869.0.0)

- 099_方法的参数03
    - [腾讯视频](https://v.qq.com/x/page/r0638pk3mjx.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=3)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU0NzEyMA==.html?spm=a2hzp.8253869.0.0)

- 100_方法的参数04
    - [腾讯视频](https://v.qq.com/x/page/y0638abq9so.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=4)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU0NzkwOA==.html?spm=a2hzp.8253869.0.0)

- 101_方法的参数05
    - [腾讯视频](https://v.qq.com/x/page/i0637mzne9o.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=5)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ3NDA2MA==.html?spm=a2hzp.8253869.0.0)

- 102_方法的参数06
    - [腾讯视频](https://v.qq.com/x/page/b06385otstq.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=6)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU0ODM2NA==.html?spm=a2hzp.8253869.0.0)

- 103_方法的返回值01值
    - [腾讯视频](https://v.qq.com/x/page/q0638hltcb5.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=7)

- 104_方法的返回值02
    - [腾讯视频](https://v.qq.com/x/page/o0638s0l1kx.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=8)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU1MDUwNA==.html?spm=a2hzp.8253869.0.0)

- 105_方法的返回值03
    - [腾讯视频](https://v.qq.com/x/page/d0638bzlgde.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=9)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ4MjYxNg==.html?spm=a2hzp.8253869.0.0)

- 106_方法的返回值04
    - [腾讯视频](https://v.qq.com/x/page/w0638t95rap.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=10)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ4Mjg4NA==.html?spm=a2hzp.8253869.0.0)

- 107_方法的返回值05
    - [腾讯视频](https://v.qq.com/x/page/i0638svzt5t.html)
    - [B站视频](https://www.bilibili.com/video/av23114859/?p=11)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ4NTk2OA==.html?spm=a2hzp.8253869.0.0)

- 109_猜字符游戏02
    - [腾讯视频](https://v.qq.com/x/page/z0638f9cot3.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=2)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ5MDkyOA==.html?spm=a2hzp.8253869.0.0)

- 110_猜字符游戏03
    - [腾讯视频](https://v.qq.com/x/page/m06389p5qbh.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=3)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU1MTg3Mg==.html?spm=a2hzp.8253869.0.0)

- 111_猜字符游戏04
    - [腾讯视频](https://v.qq.com/x/page/m0638zg8u79.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=4)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU1NDY3Ng==.html?spm=a2hzp.8253869.0.0)

- 112_猜字符游戏05
    - [腾讯视频](https://v.qq.com/x/page/k063866uxgb.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=5)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU1NTY5Mg==.html?spm=a2hzp.8253869.0.0)

- 113_猜字符游戏06
    - [腾讯视频](https://v.qq.com/x/page/t0638zxdc96.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=6)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ5OTI3Ng==.html?spm=a2hzp.8253869.0.0)

- 114_猜字符游戏07
    - [腾讯视频](https://v.qq.com/x/page/j0638qn26c0.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=7)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDQ5OTM5Ng==.html?spm=a2hzp.8253869.0.0)

- 115_生成随机字符数组01
    - [腾讯视频](https://v.qq.com/x/page/h0638a91c3e.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=8)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU1ODMwNA==.html?spm=a2hzp.8253869.0.0)

- 116_生成随机字符数组02
    - [腾讯视频](https://v.qq.com/x/page/o0639p2olzp.html) 
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=9)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU1OTc0OA==.html?spm=a2hzp.8253869.0.0)
- 117_生成随机字符数组03
    - [腾讯视频](https://v.qq.com/x/page/i06399fpmju.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=10)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDUwNjcwOA==.html?spm=a2hzp.8253869.0.0)
- 118_生成随机字符数组04
    - [腾讯视频](https://v.qq.com/x/page/x06395o4ti9.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=11)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU4NzYyOA==.html?spm=a2hzp.8253869.0.0)

- 119_生成随机字符数组05
    - [腾讯视频](https://v.qq.com/x/page/a06396220zf.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=12)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDUxMjk5Mg==.html?spm=a2hzp.8253869.0.0)
- 120_主方法01
    - [腾讯视频](https://v.qq.com/x/page/d06398riyhs.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=13)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDU2MTYxNg==.html?spm=a2hzp.8253869.0.0)

- 121_主方法02
    - [腾讯视频](https://v.qq.com/x/page/d0639qsxvti.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=14)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDUxOTAxNg==.html?spm=a2hzp.8253869.0.0)

- 122_主方法03
    - [腾讯视频](https://v.qq.com/x/page/z0639liacwr.html)
    - [B站视频](https://www.bilibili.com/video/av23116948/?p=15)
    - [优酷视频](https://v.youku.com/v_show/id_XMzU5MDUyMDg0NA==.html?spm=a2hzp.8253869.0.0)

## 题库

- [01_运行环境](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/01_%E8%BF%90%E8%A1%8C%E7%8E%AF%E5%A2%83/%E9%A2%98%E5%BA%93/java%E8%BF%90%E8%A1%8C%E7%8E%AF%E5%A2%83.md)
- [02_变量](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/02_%E5%8F%98%E9%87%8F/%E9%A2%98%E5%BA%93/%E5%8F%98%E9%87%8F.md)
- [03_基本数据类型](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/03_%E5%9F%BA%E6%9C%AC%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B/%E9%A2%98%E5%BA%93)
- [04_运算符和表达式](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/04_%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F/%E9%A2%98%E5%BA%93/%E8%BF%90%E7%AE%97%E7%AC%A6%E5%92%8C%E8%A1%A8%E8%BE%BE%E5%BC%8F%E5%9F%BA%E7%A1%80%E6%B5%8B%E8%AF%95%E9%A2%98.md)
- [05_分支结构](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/05_%E5%88%86%E6%94%AF%E7%BB%93%E6%9E%84/%E9%A2%98%E5%BA%93/%E5%88%86%E6%94%AF%E7%BB%93%E6%9E%84%E5%9F%BA%E7%A1%80%E6%B5%8B%E8%AF%95%E9%A2%98.md)
- [06_循环结构](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/06_%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/%E9%A2%98%E5%BA%93/%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84%E5%9F%BA%E7%A1%80%E6%B5%8B%E8%AF%95%E9%A2%98.md)
- [07_数组](https://gitee.com/tedulivevideo/java_fundamentals/tree/master/07_%E6%95%B0%E7%BB%84/%E9%A2%98%E5%BA%93)
- [08_方法](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/08_%E6%96%B9%E6%B3%95/%E9%A2%98%E5%BA%93/%E6%96%B9%E6%B3%95%E5%9F%BA%E7%A1%80%E6%B5%8B%E8%AF%95%E9%A2%98.md)
## 使用说明

### 微信公众号

微信订阅号

![微信订阅号：TLV_CN](https://gitee.com/uploads/images/2018/0503/151539_f40c28b3_675903.jpeg "订阅号二维码.jpg")

微信服务号

![微信服务号：TEDU_TMOOC](https://gitee.com/uploads/images/2018/0503/151806_8be41250_675903.jpeg "服务号二维码.jpg")

## 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request