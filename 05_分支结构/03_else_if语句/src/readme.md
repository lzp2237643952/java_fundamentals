# 05_03_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [ElseIfText.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/05_%E5%88%86%E6%94%AF%E7%BB%93%E6%9E%84/03_else_if%E8%AF%AD%E5%8F%A5/src/ElseIfText.java)

### 概要

else-if语句的代码演示，并制作判断成绩等级的小程序

### 学习目标

* 学会else-if语句
* 自己使用else-if制作一个小程序
