package cn.tedu;

//else-if的代码演示
public class ElseIfText {
	public static void main(String[] args) {
		/*
		 * A:成绩大于等于90 
		 * B:成绩大于等于80并且小于80 
		 * C:成绩大于60并且小于80 
		 * D:成绩小于60
		 */
		int score = 95;
		if (score >= 90) {
			System.out.println("A");
		} else if (score >= 80) {
			System.out.println("B");
		} else if (score >= 60) {
			System.out.println("C");
		} else {
			System.out.println("D");
		}
	}

}
