# 05_04_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [SwitchDemo.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/05_%E5%88%86%E6%94%AF%E7%BB%93%E6%9E%84/04_switch%E8%AF%AD%E5%8F%A5/src/SwitchDemo.java)

### 概要

switch case的演示代码以及客服小程序

### 学习目标

* 学会使用switch case代码以及break的使用
* 自己使用switch case编写小程序