package cn.tedu;

//switch……case的代码演示
public class SwitchDemo {
	public static void main(String[] args) {
		int num = 2;
		switch (num) {// byte,short,int,String(JDK1.7之后)
		case 1:
			System.out.println("呼叫教学部");
			break;
		case 2:// 以此为入口
			System.out.println("呼叫人事部");
			break;// 跳出switch
		case 3:
			System.out.println("呼叫财务部");
			break;
		default:// 所有case未匹配时执行
			System.out.println("人工服务");
		}
	}

}
