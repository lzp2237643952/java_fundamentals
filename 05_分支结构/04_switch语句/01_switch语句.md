# 001_switch语句

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC
---

<!-- TOC -->

- [001_switch语句](#001-switch)
    - [1、switch-case语句执行逻辑](#1switch-case)
    - [2、switch-case和break联合使用](#2switch-casebreak)
    - [3、switch-case语句用于分支](#3switch-case)
    - [4、switch-case的优势](#4switch-case)

<!-- /TOC -->

---

## 1、switch-case语句执行逻辑

switch-case是一种特殊的分支结构，与else if类似，但其应用面不如else if，只能用于特殊的情况之下， switch-case可以 **根据一个整数值的不同取值，从不同的程序入口开始执行。**

语法结构如下：

```java
switch(整型表达式) {
    case 整型常量值1: //  入口1
        语句1;
        语句2;
    case 整型常量值2: //  入口2
        语句3;
        ……
    default: //  默认入口
        语句n;
}
```

switch-case流程图如下所示：

![switch流程图](https://i.imgur.com/OrO0hFM.png)

分析上图，可以看出它的执行逻辑如下：

1. 计算整数表达式的值：
2. 若值等于整型常量值1，则从语句1开始执行，而后语句2、3，一直执行到语句n。
3. 若值等于整型常量值2，则从语句3开始执行，一直执行到语句n。
4. 若没有找到匹配的值，则只执行语句n。

 通过分析可以看出， **switch是以case后的整型常量作为入口的，若值相等，即开始执行其后面的语句。**

 使用switch时需要注意两个问题： **第一，case后面的常量值必须不同，第二，switch后面的整型表达式的值必须是整型或字符型。**

## 2、switch-case和break联合使用

通常case1、case2、……、caseN对应完全不同的操作，可以和break语句配合使用， **执行完相应语句后即退出switch块，不继续执行下面的语句。**
例如：

```java
switch(整型表达式) {
    case 整型常量值1: //  入口1
        语句1;
        语句2;
	    break ;
    case 整型常量值2: //  入口2
        语句3;
	    break ;
    //……
    default: //  默认入口
        语句n;
}
```

break语句的作用在于 **跳出switch结构**

## 3、switch-case语句用于分支

在实际应用中，switch-case语句常常与break配合使用，例如：

```java
int num = 2;
switch(num) {
    case 1:
       	System.out.println(“呼叫教学部”);
       	break;
    case 2:
       	System.out.println(“呼叫人事部”);
       	break;
    default:
       	System.out.println(“人工服务”);
}
```

代码运行结果为：“呼叫人事部”。因为匹配case2输出后，即break跳出switch语句了。

## 4、switch-case的优势

switch-case结构在实际应用中较广泛， 常常 **和break语句结合使用实现分支的功能。**

在很多情况下,switch-case可以代替else if结构，而switch-case实现分支功能的效率要高于else if结构，并且结构更清晰，所以推荐使用。

**从JDK 7.0开始，switch-case可以支持字符串表达式**，将更加方便程序的操作。
