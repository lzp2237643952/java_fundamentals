# 001_if_else语句

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. if-else语句的执行逻辑](#1-if-else语句的执行逻辑)
- [2. if-else语句流程图](#2-if-else语句流程图)
- [3. if-else语句用于处理分支逻辑](#3-if-else语句用于处理分支逻辑)

<!-- /TOC -->

---

<a id="markdown-1-if-else语句的执行逻辑" name="1-if-else语句的执行逻辑"></a>
## 1. if-else语句的执行逻辑

```java
语句0；
if(逻辑表达式){
    语句块1；
}else {
    语句块2；
}
语句块3；
```

执行循环的顺序是：

1. 执行语句0
2. 判断if逻辑表达式的值：
	- 若值为true，则执行语句块1
	- 若值为false，则执行语句块2
3. 执行语句3

<a id="markdown-2-if-else语句流程图" name="2-if-else语句流程图"></a>
## 2. if-else语句流程图

如下图：

![if-else语句流程](https://i.imgur.com/68vnOGG.png)

- 当条件满足时，执行语句块1，然后执行if-else语句下面的语句
- 否则执行语句块2，再执行if-else语句下面的语句

<a id="markdown-3-if-else语句用于处理分支逻辑" name="3-if-else语句用于处理分支逻辑"></a>
## 3. if-else语句用于处理分支逻辑

通过下面代码，可以解决新增加的异常情况处理问题：

```java
if( money >= totalPrice ) {
    double change = money – totalPrice;
    System.out.println("应收金额为:" + totalPrice +"，找零为:" + change);
} else {
    System.out.println(“Error! 收款金额小于应收金额”);
}
```

money 为收款金额

totalPrice 为应收金额

当收款金额大于应收金额时则计算找零并输出，否则提示错误
