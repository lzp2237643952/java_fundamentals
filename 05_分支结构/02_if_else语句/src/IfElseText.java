package cn.tedu;

import java.util.Scanner;

//if else分支结构
public class IfElseText {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入商品单价:");
		double unitPrice = scanner.nextDouble();
		System.out.println("请输入商品数量:");
		int amount = scanner.nextInt();
		System.out.println("请输入金额:");
		double money = scanner.nextDouble();

		// 计算单价(单价*数量)
		double totalPrice = unitPrice * amount;
		// totalPrice=totalPrice*0.8; //打8折
		if (totalPrice >= 500) { // 满500
			totalPrice *= 0.8; // 打8折
		}
		if (money >= totalPrice) {// 够
			double change = money - totalPrice; // 计算找零(金额-总价)
			System.out.println("总价为:" + totalPrice + ",找零为:" + change);

		} else {
			System.out.println("ERROR!您给的钱不够,还差" + (totalPrice - money));
		}
	}
}
