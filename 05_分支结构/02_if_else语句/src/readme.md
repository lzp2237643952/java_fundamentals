# 05_02_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [IfElse.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/05_%E5%88%86%E6%94%AF%E7%BB%93%E6%9E%84/02_if_else%E8%AF%AD%E5%8F%A5/src/IfElseText.java)

### 概要

if-else的代码演示

### 学习目标

* 学会if-else的使用及结构