# 05_01_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [IfText.java](IfText.java)

**概要**

if语句的代码演示

**学习目标**

* if语句的使用及逻辑结构

## [IfScore.java](IfScore.java)

**概要**

if语句的代码演示，根据输入的考试成绩判断级别

* 小于60分：C级
* 60分到85分之间：B级
* 85分以上：A级

**学习目标**

* if语句的使用及逻辑结构