import java.util.Scanner;

//if语句演示
//柜台收银程序
public class IfText {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入商品单价:");
		Double unitPrice = scanner.nextDouble();
		System.out.println("请输入商品数量:");
		int amount = scanner.nextInt();
		System.out.println("请输入金额:");
		Double money = scanner.nextDouble();

		// 计算单价(单价*数量)
		Double totalPrice = unitPrice * amount;
		// totalPrice=totalPrice*0.8; //打8折
		if (totalPrice >= 500) { // 满500
			totalPrice *= 0.8; // 打8折
		}
		// 计算找零(金额-总价)
		Double change = money - totalPrice;
		System.out.println("总价为:" + totalPrice + ",找零为:" + change);
	}
}
