import java.util.Scanner;

public class IfScore {
    public static void main(String[] args) {
        // Scanner 用于接收用户的输入
        Scanner scanner = new Scanner(System.in);
        // 提示用户的信息
        System.out.println("请输入考试成绩（0-100）：");
        // 判断输入是否是 double 类型
        if (scanner.hasNextDouble()) {            
            double score = scanner.nextDouble();  // 获取用户输入
            if (score < 60) {  // 如果考试成绩不及格
                System.out.println(String.format("您的成绩是【%.1f】，属于【%s】级", score, "C"));
            }
            if (score >= 60 && score < 85) {  // 如果考试成绩合格
                System.out.println(String.format("您的成绩是【%.1f】，属于【%s】级", score, "B"));
            }
            if (score >= 85) {  // 如果考试成绩优秀
                System.out.println(String.format("您的成绩是【%.1f】，属于【%s】级", score, "A"));
            }
        }
        scanner.close();  // 关闭Scanner对象，释放资源
    }
}