# 06_02_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [DoWhileDemo.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/06_%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/02_do_while%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/src/DoWhileDemo.java)

### 概要

循环结构 dowhile循环 的相关代码

### 学习目标

* 猜数字小游戏
* dowhile循环的结构以及循环三要素