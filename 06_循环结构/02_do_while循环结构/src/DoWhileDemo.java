package cn.tedu;

import java.util.Scanner;

//do……while循环
public class DoWhileDemo {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = 250;
		int guess;
		do {
			System.out.println("猜吧");
			guess = scan.nextInt();
		} while (guess != num);
		System.out.println("恭喜你,猜对了");

		/*
		 * 三要素： 循环变量：用户猜的数guess 
		 * 1)System.out.println("猜吧"); int
		 * guess=scan.nextInt(); 
		 * 2)guess != num 
		 * 3)System.out.println("猜吧");
		 *  guess = scan.nextInt();
		 */

		/*
		 * 猜数字小游戏: 
		 * 1)手里藏起来一个数num(假设250) 
		 * 2)猜吧 300 太大了 猜吧 200 太小了 猜吧 251 太大了 猜吧 250 恭喜你猜对了
		 */
	}

}
