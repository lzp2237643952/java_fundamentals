# 001_do_while循环结构

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [001_do_while循环结构](#001-do-while)
	- [1、do-while语句的执行逻辑](#1do-while)
	- [2、do-while语句的流程图](#2do-while)
	- [3、do-while语句用于处理循环逻辑](#3do-while)
	- [4、while和do-while语句的区别](#4whiledo-while)

<!-- /TOC -->

---

名词解释：

[百度百科](https://baike.baidu.com/item/do%20while/2814046?fr=aladdin)
[维基百科](https://zh.wikipedia.org/wiki/Do-while%E5%BE%AA%E7%8E%AF)

## 1、do-while语句的执行逻辑

do-while语句也是循环的一种常见语法结构，语法如下：

```java
do {
		语句块
} while( boolean表达式 ) ;
```

执行过程为：

1. 先执行语句块
2. 再计算boolean表达式的值，如果为true，再次执行语句块。如此循环往复，直到boolean表达式的值为false为止

需要注意的是: **无论boolean表达式是否为true，都要先执行一次语句块。**

## 2、do-while语句的流程图

do-while语句的流程图如图所示：

![dowhile语句流程图](https://i.imgur.com/5vu9VS7.png)

与while语句一样，do-while语句也要避免“死循环”的发生。

## 3、do-while语句用于处理循环逻辑

通过如下示例体会do-while语句的执行逻辑：

```java
int pwd;
do{
	System.out.print(“请输入密码”);
	pwd = scanner.nextInt();
} while ( 123 != pwd) ;
```

上面的语句执行过程如下，首先声明整型变量pwd，在do语句中提示“请输入密码”,接收用户输入的数据赋给pwd， 而后判断123是否不等于pwd，若为true则执行do语句块，以此类推，直到123等于pwd时，循环结束。

## 4、while和do-while语句的区别

while与do-while都是用于执行循环结构的语句，区别在于，while循环先判断再执行，而do-while循环先执行一次，再判断。

那么，**当初始情况不满足循环条件时，while循环就一次都不会执行，而do-while循环不管任何情况都至少会执行一次。**
