package cn.tedu;
//for循环的演示-04
public class ForDemo2 {
	public static void main(String[] args) {
		//跳过个位为3的
		int sum=0;  //和
		for (int num = 1; num <=100; num++) {
			if (num%10==3) {
				continue;
			}
			sum=sum+num;
		}
		System.out.println("sum="+sum);
		/*
		 * sum=0
		 * num=1  sum=1
		 * num=2  sum=1+2
		 * num=3
		 * num=4  sum=1+2+4
		 * ……
		 * num=13
		 * num=23
		 * num=33
		 * ……
		 * num=93
		 */
	}

}
