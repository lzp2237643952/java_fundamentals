# 06_05_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [ForDemo2.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/06_%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/05_%E5%BE%AA%E7%8E%AF%E6%8E%A7%E5%88%B6/src/ForDemo2.java)

### 概要

使用循环控制continue

### 学习目标

* 利用循环控制continue1至100进行求和，跳过个数为3的