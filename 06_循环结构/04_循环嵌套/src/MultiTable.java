package cn.tedu;
//九九乘法表
public class MultiTable {
	public static void main(String[] args) {
		for (int num = 1; num <= 9; num++) {// 行
			for (int i = 1; i <= num; i++) {// 列
				System.out.print(i+"*"+num+"="+i*num+"\t");
			}
			System.out.println();//换行
			
		}
	}
/*
 * 1)循环中套循环，多行多列时使用，一般外层控制行，内层控制列
 * 2)执行规则：外层循环走一次，内层循环走所有次
 */

}
