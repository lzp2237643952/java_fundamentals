# 06_04_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [MultiTable.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/06_%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/04_%E5%BE%AA%E7%8E%AF%E5%B5%8C%E5%A5%97/src/MultiTable.java)

### 概要

利用for循环嵌套打印九九乘法表

### 学习目标

* 学会使用循环嵌套
* 学会打印九九乘法表