# 001_while循环结构

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. while 语句的执行逻辑](#1-while-语句的执行逻辑)
- [2. while语句的流程图](#2-while语句的流程图)
- [3. while语句用于处理循环逻辑](#3-while语句用于处理循环逻辑)
- [4. 使用break语句跳出循环](#4-使用break语句跳出循环)

<!-- /TOC -->

---

while 用于实现控制语句中的循环结构

在满足条件的情况下，让计算机重复执行定义好的代码。

**名词解释**
[百度百科](https://baike.baidu.com/item/while/755564?fr=aladdin)
[维基百科](https://zh.wikipedia.org/wiki/While%E8%BF%B4%E5%9C%88)

<a id="markdown-1-while-语句的执行逻辑" name="1-while-语句的执行逻辑"></a>
## 1. while 语句的执行逻辑

执行逻辑为：

1. 计算boolean表达式的值，如果值为true则执行语句块。
2. 语句块执行完后再次判断boolean表达式的值
	1. 如果为true则继续执行语句块，如此循环往复
	2. 直到boolean为false时退出while循环

语法结构：

![while语法结构](https://i.imgur.com/OcRMkCb.png)

<a id="markdown-2-while语句的流程图" name="2-while语句的流程图"></a>
## 2. while语句的流程图

流程图如下：

![while流程图](https://i.imgur.com/hqlR3Qa.png)

需要注意的是，一般情况下，循环操作中 **会存在使得循环条件不满足的可能性，否则该循环将成为“死循环”。**

“死循环”意味着会一直执行循环体操作，循环后面的语句永远不会被执行，“死循环”在软件系统中是需要避免的。

<a id="markdown-3-while语句用于处理循环逻辑" name="3-while语句用于处理循环逻辑"></a>
## 3. while语句用于处理循环逻辑

通过如下示例体会while语句的执行逻辑：

```java
int age = 1;
while (age<=100) {
System.out.println(“马上有钱”);
age++;
}
```

上面的语句执行过程如下：

首先声明整型变量age并赋初始值为1，

而后判断age是否小于等于100，

条件为真，输出“马上有钱”并将age的值增1变为2，

再次判断条件，此时age为2，依然小于100，再一次输出“马上有钱”

并将age的值再增1变为3…… 以此类推，

直到age等于101时，判断条件为false，循环结束。

<a id="markdown-4-使用break语句跳出循环" name="4-使用break语句跳出循环"></a>
## 4. 使用break语句跳出循环

break用在循环体中用于退出循环

例如：

```java
int   x =  0;
while ( x < 10 ) {
	if ( 5 == x )
    {
		break;
    }
	System.out.println(x);
	x + + ;
}
```

输出结果为：0 1 2 3 4

当x==5时，执行break语句直接退出循环结构了