package cn.tedu;

public class WhileDemo1 {
	public static void main(String[] args) {
		// 2.输出9的乘法表
		int num = 1;
		while (num <= 9) {// 2*9=18
			/*
			 * if (num==4) { break; }
			 */
			System.out.println(num + "*9=" + num * 9);
			num++;
		}
		System.out.println("over");
	}
}
