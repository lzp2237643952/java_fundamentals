# 06_01_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [WhileDemo.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/06_%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/01_while%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/src/WhileDemo.java)

**概要**

循环结构 while循环 的相关代码

**学习目标**

* 循环的三要素
* while循环的结构

## [WhileDemo1.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/06_%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/01_while%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/src/WhileDemo1.java)

**概要**

循环结构 while循环 的相关代码

**学习目标**

* 输出9的乘法表
* 尝试自己输出其他数字的乘法表