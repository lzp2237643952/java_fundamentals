# 001_for循环结构

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [001_for循环结构](#001-for)
	- [1、考虑如下循环问题的相同之处](#1)
	- [2、for语句的执行逻辑](#2for)
	- [3、for语句的流程图](#3for)
	- [4、for语句用于实现固定次数循环](#4for)
	- [5、for语句三个表达式特殊用法](#5for)

<!-- /TOC -->

---

名词解释：

[百度百科](https://baike.baidu.com/item/for%E5%BE%AA%E7%8E%AF/5755435?fr=aladdin)
[维基百科](https://zh.wikipedia.org/wiki/For%E8%BF%B4%E5%9C%88)

## 1、考虑如下循环问题的相同之处

请考虑如下三个题目的相似之处：

1. 计算从1加到100的值；
2. 计算1+1/3+1/5+…+1/999；
3. 找出从第1号学员到第500号学员中成绩大于90的学员。

通过上面的问题可以看出，如上三个题目都是在有规律的重复执行某个操作，在程序中考虑使用循环结构来解决。

首先需要先找到前面介绍的循环变量，此循环变量在每一次的循环中有规律的发生变化，并且常常作为判断是否继续循环的条件。

首先，看第1个题目，设计循环变量i ，i的值为1,2,…,100，即：从1开始，每次增1。第二个题目，设计循环变量i , i的值为1,3,5,…,999，即：从1开始，每次增2。第三个题目，设计循环变量i , i的值为1,2,…,500，即：从1开始，每次增1。

通过上面的分析，可以看出，如上的 **循环都是固定次数的循环，这种情况，优先考虑使用for语句来实现。**

## 2、for语句的执行逻辑

for语句是循环中最最常用的一种方式。 **for循环用于将某个语句或语句块重复执行预定次数的情形**。

语法如下：

```java
for ( 表达式1；表达式2；表达式3  )  {
	语句块（循环体）
}
```

可以看出，for循环的三个表达式之间通过分号；进行分隔，其 **执行逻辑**如下所示：

1. 计算表达式1的值，通常为循环变量赋初值；
2. 计算表达式2（表达式2为逻辑表达式）的值，即判断循环条件是否为真，若值为真则执行循环体一次(语句块)，否则跳出循环；
3. 执行循环体；
4. 计算表达式3的值，此处通常写更新循环变量的赋值表达式；
5. 计算表达式2的值，若值为true则执行循环体，否则跳出循环；
6. 如此循环往复，直到表达式2的值为false。

## 3、for语句的流程图

如图所示：

![for语句流程图](https://i.imgur.com/P7fb8Kt.png)

比如：

```java
for ( int i = 1 ; i <= 10 ; i + + ) {
		System.out.println( i );
}
```

输出结果为：1 2 3 4 5 6 7 8 9 10

执行顺序为：首先，初始化i的值为1，判断i是否小于等于10，结果为true，其次，输出i的值1，然后，执行i的自增1，再判断i是否小于等于10，结果为true，执行输出i的值2，以此类推……输出i的值10后，执行i的自增1，此为i为11，判断结果为false，程序终止。

## 4、for语句用于实现固定次数循环

for语句常常用于 **解决固定次数循环的处理。**

比如：累加求和（求  1 + 2 +3 + … + 100 = ？）

```java
int  sum = 0 ;
for ( int i = 1 ; i <= 100 ; i + + ) {
	  sum += i ;
}
System.out.println(“1到100的和为：” + sum ) ;
```

分析上面代码的执行过程，首先，声明一个变量用于保存累加和，此变量命名为sum，赋初始值为0，而后，for循环从1开始，每次增1，执行100次，循环体中将i的值累加到sum变量中。

注意： **每次都是在sum的基础之上加上i，所以使用了sum += i**。循环结束后，输出结果“1到100的和为：5050“。

## 5、for语句三个表达式特殊用法

**表达式1位置为空时：**

```java
int sum = 0 ;
int i = 1;
for (  ; i <= 10 ; i + + ) {
	sum += i ;
}
System.out.println(“1到10的和为：” + sum );
```

通过上面的代码可以看出，虽然省略了表达式1，但只是将它放在了for循环的外面进行声明，只是位置不同而已。在此需要注意一点，**即使for语句中不写表达式1了，表达式2前面的分号；也不能省略。**

**表达式3的位置为空时：**

```java
int sum = 0 ;
for ( int i = 1 ; i <= 10 ; ) {
	sum += i ;
	i + + ;
}
System.out.println(“1到10的和为：” + sum ) ;
```

通过代码也可以看到，虽然省略了表达式3，但是仍然把表达式3放在了for循环中，只是位置不同， **在此需要注意一点，即使for语句中不写表达式3了，表达式2后面的分号；也不能省略。**

**表达式1,2,3位置均为空时：**

```java
for (;;) {
    System.out.println("我要学习...");
}
```

通过上面的代码可以看出，如上,**代码没有循环变量、没有条件控制，因此会造成死循环，而死循环在编写程序过程中是必须要避免的**，可以 **在循环体中添加break跳出循环。**

**表达式1和3的位置内容多样化**

for语句中的三个表达式中 **表达式1和表达式3可以使用逗号表达式，逗号表达式就是通过”，”运算符隔开的多个表达式组成的表达式，从左向右进行计算。**

```java
for ( int  i =1 , j = 6  ;  i <= 6  ;  i +=2 , j -=2 ) {
	System.out.println(“ i , j = “ + i + “,” + j );
}
```

输出结果为：

i , j = 1,6

i , j = 3,4

i , j = 5,2

上面的代码的执行逻辑如下：初始设置i为1，j为6，判断i是否小于等于6，为真执行循环体，而后执行i+=2，j-=2，即：i增2，j减2。再判断i是否小于等于6，为真继续执行循环体，以此类推，直到条件为false。
