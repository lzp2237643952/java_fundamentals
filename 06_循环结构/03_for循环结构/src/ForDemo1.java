package cn.tedu;
//for循环的演示-02
public class ForDemo1 {
	public static void main(String[] args) {
		//1+2+3+4+……+99+100=？
		int sum=0;  //和
		for (int num = 1; num<=100; num++) {
			sum=sum+num;
		}
		System.out.println("sum="+sum);
		/*
		 *           sum=0
		 *  num=1    sum=1
		 *  num=2    sum=1+2
		 *  num=3    sum=1+2+3
		 *  num=4    sum=1+2+3+4
		 *  ……
		 *  num=100  sum=1+2+3+4+……+100
		 *  num=101
		 */
		
		//死循环
		/*for (; ;) {
			System.out.println("我要学习……");
			
		}*/
		
		/*
		 * 
		 * for (int i = 0,j=5; i < 5; i+=2,j-=2) {
			
		   }
		
		 * i=0,j=5
		 * i=2,j=3
		 * i=4,j=1
		 * i=6,j=-1
		 */
	}

}
