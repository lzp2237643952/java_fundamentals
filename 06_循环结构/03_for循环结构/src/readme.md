# 06_03_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [ForDemo.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/06_%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/03_for%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/src/ForDemo.java)

**概要**

循环结构 for循环 的三要素讲解

**学习目标**

* for循环的结构以及循环三要素

## [ForDemo1.java](https://gitee.com/tedulivevideo/java_fundamentals/blob/master/06_%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/03_for%E5%BE%AA%E7%8E%AF%E7%BB%93%E6%9E%84/src/ForDemo1.java)

**概要**

使用for循环计算1到100的和

**学习目标**

* 利用for循环进行累加求和