# 代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [MethodCall.java](MethodCall.java)

**概要**

主函数中进行方法的调用

**学习目标**

- 熟练掌握方法的调用

## [MethodCall2.java](MethodCall2.java)

**概要**

调用方法时的参数传递

**学习目标**

- 熟练掌握方法的调用