public class MethodCall {
    public static void main(String[] args) {
        // 在主方法中调用add()方法并进行加法运算
        System.out.println(add(12 + 12));
    }

    // 声明一个私有的静态方法add()
    private static int add(int A) {
        return A;
    }
}