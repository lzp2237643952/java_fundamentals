public class MethodCall2 {
    /*
     * 调用方法时的参数传递
     */
    public static void main(String[] args) {
        // 在主方法中调用SayHi()方法
        SayHi("张三");
        SayHi("李四");
    }

    /*
     * 定义方法
     */
    public static void SayHi(String name) {
        System.out.println("我的名字是" + name);
    }
}
