public class Parameter {
    public static void main(String[] args) {
        draw("笔"); // 实参--笔
        // 实际参数是指在调用有参方法时真正传递的内容
    }

    public static void draw(String paper) { // 形参--paper
        System.out.println("画画需要" + paper);
        // 形式参数是用于接收实参内容的参数
    }
}