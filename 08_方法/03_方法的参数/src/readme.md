# 代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---
## [Parameter.java](Parameter.java)

**概要**

- 形参--形式参数是用于接受实参内容的参数
- 实参--是指在调用有参方法时真正传递的内容

**学习目标**

掌握实参和形参的区别