﻿import java.util.Date;

public class FunctionDefine {

    // 定义方法，在控制台输出一句话
    public static void showHello() {
        System.out.println("你好，这是我自己定义的方法！");
    }

    // 定义方法，获取当前系统时间
    public static Date getNow() {
        return new Date(); // 注意需要导入 java.util.Date 的包
    }

    // 主函数
    public static void main(String[] args) {
        // 调用方法
        showHello();

        // 调用又返回类型的方法
        System.out.println("现在的时间是：" + getNow());
  
    }
}