# 代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [FunctionDefine](FunctionDefine.java)

**概要**

定义方法的语法

**学习目标**

- 没有返回类型的方法定义
- 有返回类型的方法定义