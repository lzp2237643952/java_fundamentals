# 1. Java基础题库

<!-- TOC -->

- [1. 运行环境](#1-运行环境)
    - [1.1. （单选题）在控制台运行一个 `Java` 程序 `TestDemo.class`，使用的命令正确的是（ ）](#11-单选题在控制台运行一个-java-程序-testdemoclass使用的命令正确的是-)
    - [1.2. （单选题）在 `Java` 中，以下描述错误的是（ ）](#12-单选题在-java-中以下描述错误的是-)
    - [1.3. （单选题）Java是从（）语言改进重新设计](#13-单选题java是从语言改进重新设计)
    - [1.4. （单选题）Java编程语言之父是（）](#14-单选题java编程语言之父是)
    - [1.5. (多选题)下列描述中，哪些符合 Java 语言的特征（ ）](#15-多选题下列描述中哪些符合-java-语言的特征-)
    - [1.6. (多选题)下列哪些选项属于Java技术体系（）](#16-多选题下列哪些选项属于java技术体系)
    - [1.7. (多选题)下列对Java语言的叙述中，正确的是（）](#17-多选题下列对java语言的叙述中正确的是)

<!-- /TOC -->

<a id="markdown-1-运行环境" name="1-运行环境"></a>
## 1. 运行环境

<a id="markdown-11-单选题在控制台运行一个-java-程序-testdemoclass使用的命令正确的是-" name="11-单选题在控制台运行一个-java-程序-testdemoclass使用的命令正确的是-"></a>
### 1.1. （单选题）在控制台运行一个 `Java` 程序 `TestDemo.class`，使用的命令正确的是（ ）

- A) java TestDemo.class
- B) javac TestDemo.class
- C) java TestDemo
- D) javac TestDemo

<a id="markdown-12-单选题在-java-中以下描述错误的是-" name="12-单选题在-java-中以下描述错误的是-"></a>
### 1.2. （单选题）在 `Java` 中，以下描述错误的是（ ）

- A) `.class` 是源文件
- B) `.java` 是编译前的源文件
- C) `.class` 是编译后的文件
- D) `Java` 程序需编译后方可运行

<a id="markdown-13-单选题java是从语言改进重新设计" name="13-单选题java是从语言改进重新设计"></a>
### 1.3. （单选题）Java是从（）语言改进重新设计

- A) Ada
- B) C++
- C) Pasacal
- D) BASIC

<a id="markdown-14-单选题java编程语言之父是" name="14-单选题java编程语言之父是"></a>
### 1.4. （单选题）Java编程语言之父是（）

- A) James Gosling
- B) Abigail
- C) Brutus
- D) 以上选项都不对

<a id="markdown-15-多选题下列描述中哪些符合-java-语言的特征-" name="15-多选题下列描述中哪些符合-java-语言的特征-"></a>
### 1.5. (多选题)下列描述中，哪些符合 Java 语言的特征（ ）

- A) 支持跨平台(Windows,Linux,Unix等)
- B) GC(自动垃圾回收)，提高了代码安全性
- C) 支持类 C 的指针运算操作
- D) 不支持与其它语言书写的程序进行通讯

<a id="markdown-16-多选题下列哪些选项属于java技术体系" name="16-多选题下列哪些选项属于java技术体系"></a>
### 1.6. (多选题)下列哪些选项属于Java技术体系（）

- A) Java Me
- B) Java Se
- C) Java EE
- D) Java Card

<a id="markdown-17-多选题下列对java语言的叙述中正确的是" name="17-多选题下列对java语言的叙述中正确的是"></a>
### 1.7. (多选题)下列对Java语言的叙述中，正确的是（）

- A) Java虚拟机解释执行字节码
- B) Java的类是对具有相同行为对象的一种抽象
- C) Java中的垃圾回收机制是一个系统级的线程
- D) JDK的库文件目录是bin