# 1. _Linux基本命令

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. 录播视频](#1-录播视频)
    - [1.1. Linux基本操作命令](#11-linux基本操作命令)
- [2. 如何寻求帮助？](#2-如何寻求帮助)
    - [2.1. man](#21-man)
    - [2.2. info](#22-info)
    - [2.3. elp](#23-elp)
- [3. 如何简单操作？](#3-如何简单操作)
    - [3.1. 光标](#31-光标)
    - [3.2. Tab补全](#32-tab补全)
- [4. 常用命令](#4-常用命令)
    - [4.1. cd](#41-cd)
    - [4.2. ls](#42-ls)
    - [4.3. pwd](#43-pwd)
    - [4.4. mkdir](#44-mkdir)
    - [4.5. rm](#45-rm)
    - [4.6. cp](#46-cp)
    - [4.7. mv](#47-mv)
    - [4.8. cat](#48-cat)
    - [4.9. more](#49-more)
    - [4.10. less](#410-less)
    - [4.11. nano](#411-nano)
    - [4.12. reboot](#412-reboot)
    - [4.13. poweroff](#413-poweroff)
    - [4.14. ping](#414-ping)
    - [4.15. grep](#415-grep)
    - [4.16. mount](#416-mount)
    - [4.17. umount](#417-umount)
    - [4.18. tar](#418-tar)
    - [4.19. ln](#419-ln)
    - [4.20. chown](#420-chown)
    - [4.21. chmod](#421-chmod)
    - [4.22. useradd](#422-useradd)
    - [4.23. passwd](#423-passwd)
    - [4.24. whereis](#424-whereis)
    - [4.25. find](#425-find)
    - [4.26. wget](#426-wget)

<!-- /TOC -->

---

<a id="markdown-1-录播视频" name="1-录播视频"></a>
## 1. 录播视频

<a id="markdown-11-linux基本操作命令" name="11-linux基本操作命令"></a>
### 1.1. Linux基本操作命令

- [腾讯视频](https://www.bilibili.com/video/av23106970/?p=5)
- [优酷](https://v.youku.com/v_show/id_XMzU1MTU3Njc3Ng==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19rxlzgcyt.html#vfrm=8-8-0-1)

欢迎来到Linux世界，下面进入一级闯关模式: **命令行**。

<a id="markdown-2-如何寻求帮助" name="2-如何寻求帮助"></a>
## 2. 如何寻求帮助？

在 Linux 下遇到问题，最重要的是要自己寻求帮助，下面是三种寻求帮助的方法。

<a id="markdown-21-man" name="21-man"></a>
### 2.1. man

man 是Linux的帮助手册，即 manual。

因为大多数程序都会自带手册，所以可以通过 man 命令获取帮助。

执行以后，在 man page 页面中按 `q` 退出。

1、获取 ls 帮助

![man ls](https://i.imgur.com/iFGSD87.png)

2、查看有多少（针对不同方面的）同名手册

![man -f ls](https://i.imgur.com/b9raBOT.png)

3、查看特定的手册

![man lp ls](https://i.imgur.com/G9olQPh.png)

<a id="markdown-22-info" name="22-info"></a>
### 2.2. info

与 man 不同的是，可以像浏览网页一样在各个节点中跳转

1、从文档首页开始浏览

![info](https://i.imgur.com/QJEd1D0.png)

2、获取特定程序的帮助

![info program](https://i.imgur.com/iYRbeXn.png)

<a id="markdown-23-elp" name="23-elp"></a>
### 2.3. elp

除了上面的两种方法外，还有一种简单使用的方法，

那就是 `--help` 参数，一般程序都会有这个参数，会输出最简单有用的介绍。

![man --help](https://i.imgur.com/q8iTkKN.png)

![info --help](https://i.imgur.com/9u0H8kH.png)

![ls --help](https://i.imgur.com/2LZqFCK.png)

<a id="markdown-3-如何简单操作" name="3-如何简单操作"></a>
## 3. 如何简单操作？

在 Terminal（终端） 中，有许多操作技巧，这里就介绍几个简单的。

<a id="markdown-31-光标" name="31-光标"></a>
### 3.1. 光标

- `up`(方向键上)：可以调出输入历史执行记录，快速执行命令
- `down`(方向键下)：配合 up 选择历史执行记录
- `Home`：移动光标到本行开头
- `End`：移动光标到本行结尾
- `PgUp`：向上翻页
- `PaDN`：向下翻页
- `ctrl + C`：终止当前程序

<a id="markdown-32-tab补全" name="32-tab补全"></a>
### 3.2. Tab补全

Tab补全是非常有用的一个功能，可以用来自动补充命令或者文件名，省时准确。

- 未输入状态下连按两次 Tab 列出所有可用命令
- 已输入部分命令名或文件名，按 Tab 进行自动补全，多用你就肯定会喜欢的了。

<a id="markdown-4-常用命令" name="4-常用命令"></a>
## 4. 常用命令

以下命令按照通常的使用频度排列。

<a id="markdown-41-cd" name="41-cd"></a>
### 4.1. cd

`cd` 是打开某个路径的命令，也是打开某个文件夹，并跳转到该处。

![cd](https://i.imgur.com/STIIau8.png)

其中 path 有 **绝对路径** 和 **相对路径** 之分，

绝对路径强调从 / 起，一直到所在路径。

相对路径则相对于当前路径来说。

假设当前目录有 etc 文件夹（绝对路径应为 /home/username/etc）

如果直接 cd etc 则进入此文件夹

![cd etc](https://i.imgur.com/zk3u0sk.png)

但若是 cd /etc/ 则是进入系统 etc ，多琢磨一下就可以理解了。

![cd /etc/](https://i.imgur.com/OLeb7Hb.png)

另外在 Linux 中， `.` 代表当前目录， `..` 代表上级目录，因此返回上级目录可以 `cd ..` 。

![cd..](https://i.imgur.com/E0tFGe6.png)

<a id="markdown-42-ls" name="42-ls"></a>
### 4.2. ls

1、`list`，列出文件

![ls](https://i.imgur.com/ooHESKn.png)

2、`ls -l` 列出当前目录可见文件详细信息

![ls -l](https://i.imgur.com/2mj8tPA.png)

3、`ls -hl` 列出详细信息并可读大小显示文件大小

![ls -hl](https://i.imgur.com/KYGamdD.png)

4、`ls -al` 列出所有文件（包括隐藏）的详细信息

![ls -al](https://i.imgur.com/syyJvTq.png)

**Tips： Linux 中 以 . 开头的文件或文件夹均为隐藏文件或隐藏文件夹。**

<a id="markdown-43-pwd" name="43-pwd"></a>
### 4.3. pwd

`pwd` 用于返回当前目录的名字，为绝对路径名。

![pwd](https://i.imgur.com/2TQnIQc.png)

<a id="markdown-44-mkdir" name="44-mkdir"></a>
### 4.4. mkdir

`mkdir` 用于新建文件夹

![mkdir](https://i.imgur.com/K0ic0R7.png)

<a id="markdown-45-rm" name="45-rm"></a>
### 4.5. rm

`rm` 即 remove，删除文件

1、`rm -i` 删除文件前提示，若多个文件则每次提示

![rm -i](https://i.imgur.com/VOiwatJ.png)

2、`rm -rf` 递归删除文件夹下所有的文件以及文件夹，包括自身。

![rm -rf](https://i.imgur.com/rDMgv7E.png)

3、`rm -d` 删除空文件夹

![rm -d](https://i.imgur.com/0OlvI6P.png)

4、`rm -r` 删除文件夹

![rm -r](https://i.imgur.com/m1wJBSa.png)

5、`rm -f` 强制删除文件或文件夹

<a id="markdown-46-cp" name="46-cp"></a>
### 4.6. cp

即 copy，复制文件。

![linux1](../../assets/Linux7.png)

`cp -r` 将tedu下的所有文件（包含子文件夹中的所有文件）复制到tedu

![linux](../../assets/Linux21.png)

<a id="markdown-47-mv" name="47-mv"></a>
### 4.7. mv

移动文件

- [ ] 待完善。 _等待共产主义接班人补充_

<a id="markdown-48-cat" name="48-cat"></a>
### 4.8. cat

用于输出内容到终端 Terminal

![linux](../../assets/Linux22.png)

<a id="markdown-49-more" name="49-more"></a>
### 4.9. more

`more`与`cat`相似,都可以查看文件内容，所不同的是，当一个文档太长的时候，`cat`只能展示最后布满屏幕的内容，前面的内容是不可见的，用`more`来逐行显示内容。

![linux](../../assets/Linux23.png)

<a id="markdown-410-less" name="410-less"></a>
### 4.10. less

- [ ] 待完善。等待共产主义接班人补充

<a id="markdown-411-nano" name="411-nano"></a>
### 4.11. nano

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-412-reboot" name="412-reboot"></a>
### 4.12. reboot

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-413-poweroff" name="413-poweroff"></a>
### 4.13. poweroff

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-414-ping" name="414-ping"></a>
### 4.14. ping

- [ ] 待完善。等待共产主义接班人补充

<a id="markdown-415-grep" name="415-grep"></a>
### 4.15. grep

- [ ] 待完善。等待共产主义接班人补充

<a id="markdown-416-mount" name="416-mount"></a>
### 4.16. mount

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-417-umount" name="417-umount"></a>
### 4.17. umount

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-418-tar" name="418-tar"></a>
### 4.18. tar

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-419-ln" name="419-ln"></a>
### 4.19. ln

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-420-chown" name="420-chown"></a>
### 4.20. chown

- [ ] 待完善。等待共产主义接班人补充

<a id="markdown-421-chmod" name="421-chmod"></a>
### 4.21. chmod

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-422-useradd" name="422-useradd"></a>
### 4.22. useradd

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-423-passwd" name="423-passwd"></a>
### 4.23. passwd

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-424-whereis" name="424-whereis"></a>
### 4.24. whereis

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-425-find" name="425-find"></a>
### 4.25. find

- [ ] 待完善。 等待共产主义接班人补充

<a id="markdown-426-wget" name="426-wget"></a>
### 4.26. wget

- [ ] 待完善。 等待共产主义接班人补充

恭喜你，你已经学习了完了26 个基础的 Linux 命令！

虽然这里只是一些最基础的命令，但是熟练使用这些命令就踏出了你从一位 Linux 新手成为 Linux 玩家的第一步！