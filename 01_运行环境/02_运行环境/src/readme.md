# 01_02_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. [1. HelloWorld.java](HelloWorld.java)](#1-1-helloworldjavahelloworldjava)

<!-- /TOC -->

---

<a id="markdown-1-1-helloworldjavahelloworldjava" name="1-1-helloworldjavahelloworldjava"></a>
## 1. [1. HelloWorld.java](HelloWorld.java)

**概要**

编写第一个Java程序，在控制台输出 `Hello Java World！`

**学习目标**

- 主函数 `main()`
- 控制台输出 `System.out.println()`