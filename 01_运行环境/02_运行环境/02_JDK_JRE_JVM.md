# 1. _JDK_JRE_JVM

> 兴趣驱动学习, 让学习成为一种习惯!
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. 录播视频](#1-录播视频)
- [2. JDK](#2-jdk)
- [3. JRE](#3-jre)
- [4. JVM](#4-jvm)
- [5. JDK、JRE 和 JVM 的作用和关系](#5-jdkjre-和-jvm-的作用和关系)

<!-- /TOC -->

---

<a id="markdown-1-录播视频" name="1-录播视频"></a>
## 1. 录播视频

- [腾讯视频](https://v.qq.com/x/page/u0646gdr8yx.html)
- [B站](https://www.bilibili.com/video/av23106417/?p=2)
- [优酷](https://v.youku.com/v_show/id_XMzU1MTU4NzIyMA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19ry84nzq1.html#vfrm=8-8-0-1)

<a id="markdown-2-jdk" name="2-jdk"></a>
## 2. JDK

JDK 是 Java Development Kit 的简写, 中文叫 **Java开发工具包**

如果想开发 Java程序, 必须安装 JDK

JDK 包含了 Java运行环境 (JRE)

名词解释:
[百度百科](https://baike.baidu.com/item/jdk/1011)
[基维百科](https://zh.wikipedia.org/wiki/JDK)

<a id="markdown-3-jre" name="3-jre"></a>
## 3. JRE

JRE 是 Java Runtime Enviroment 的简写, 中文叫 **Java运行环境**

如果想在操作系统中运行 Java程序, 必须先安装 JRE

JRE 包含了 Java虚拟机 (JVM)

名词解释:
[百度百科](https://baike.baidu.com/item/JRE)
[基维百科](https://zh.wikipedia.org/wiki/JRE)

<a id="markdown-4-jvm" name="4-jvm"></a>
## 4. JVM

JVM 是 Java Virtual Machine 的简写, 中文叫 **Java虚拟机**

使用Java虚拟机实现Java语言的一个非常重要的特点就是与平台的无关性(跨平台)

引入Java虚拟机后，Java语言在不同平台上运行时不需要重新编译

Java虚拟机在执行字节码时，把字节码解释成具体平台上的机器指令执行

名词解释:
[百度百科](https://baike.baidu.com/item/JVM)
[基维百科](https://zh.wikipedia.org/wiki/Java虚拟机)

<a id="markdown-5-jdkjre-和-jvm-的作用和关系" name="5-jdkjre-和-jvm-的作用和关系"></a>
## 5. JDK、JRE 和 JVM 的作用和关系

JDK = JRE + 编译、运行等开发工具
JRE = JVM + 类库

运行一个Java程序所要的最小环境为 JRE

开发一个Java程序所要的最小环境为 JDK