# 1. _Java编译运行过程

> 兴趣驱动学习, 让学习成为一种习惯!
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. 录播视频地址](#1-录播视频地址)
- [2. C语言和Java语言的编译对比](#2-c语言和java语言的编译对比)
- [3. Java语言的运行原理](#3-java语言的运行原理)

<!-- /TOC -->

<a id="markdown-1-录播视频地址" name="1-录播视频地址"></a>
## 1. 录播视频地址

- [腾讯视频](http://v.qq.com/x/page/i0646vi0kii.html)
- [B站](https://www.bilibili.com/video/av23106417/?p=1)
- [优酷](https://v.youku.com/v_show/id_XMzU5MTY1OTczMg==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19rxn92o9d.html#vfrm=8-8-0-1)

<a id="markdown-2-c语言和java语言的编译对比" name="2-c语言和java语言的编译对比"></a>
## 2. C语言和Java语言的编译对比

C语言为**编译型**，源代码经过编译后生成可执行 **二进制代码（0/1）**

因此 Java 的执行速度比 C语言慢，但是Java能够 **跨平台执行**，而C语言不行

程序猿编写的Java源文件（.java）首先经过编译生成字节码文件（.class）

<a id="markdown-3-java语言的运行原理" name="3-java语言的运行原理"></a>
## 3. Java语言的运行原理

Java程序运行需要 **JVM**（Java Virtual Machine）的支持，

JVM是一个安装在操作系统中的软件，为字节码文件提供运行环境。

Java官方提供了针对不同平台的JVM软件，这些JVM遵循这相同的标准。

只要是标准的.class文件，就可以在不同的JVM上运行，而且运行的效果相同。

这样就实现了 **write once ,run anywhere!（一次编程，到处使用）**
