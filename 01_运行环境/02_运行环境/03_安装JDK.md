# 1. _安装JDK

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. 录播视频](#1-录播视频)
- [2. JDK的获取和安装](#2-jdk的获取和安装)
    - [2.1. JDK的下载](#21-jdk的下载)
    - [2.2. JDK的安装](#22-jdk的安装)
        - [2.2.1. 傻瓜式安装](#221-傻瓜式安装)
        - [2.2.2. 推荐的安装方式](#222-推荐的安装方式)
    - [2.3. 验证安装是否成功](#23-验证安装是否成功)
- [3. JDK 安装路径下的目录解释](#3-jdk-安装路径下的目录解释)
    - [3.1. bin目录](#31-bin目录)
    - [3.2. db目录](#32-db目录)
    - [3.3. jre目录](#33-jre目录)
    - [3.4. include目录](#34-include目录)
    - [3.5. lib目录](#35-lib目录)
    - [3.6. src.zip文件](#36-srczip文件)

<!-- /TOC -->

---

<a id="markdown-1-录播视频" name="1-录播视频"></a>
## 1. 录播视频

- [腾讯视频](https://v.qq.com/x/page/d0646tvbhko.html)
- [B站](https://www.bilibili.com/video/av23106417/?p=3)
- [优酷](https://v.youku.com/v_show/id_XMzU1MTU4NzI0OA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19ry84uyex.html#vfrm=8-8-0-1)

<a id="markdown-2-jdk的获取和安装" name="2-jdk的获取和安装"></a>
## 2. JDK的获取和安装

<a id="markdown-21-jdk的下载" name="21-jdk的下载"></a>
### 2.1. JDK的下载

1、用浏览器访问 <http://www.oracle.com> 网站, 选择 Java 开发工具下载：

在 Downloads 中选择 Java for Developers

![选择jdk](https://i.imgur.com/6Ip8Pev.png)

2、选择下载 Java:

![下载jdk](https://i.imgur.com/uc5ydTk.png)

3、接受软件许可协议以后, 下载 JDK 安装包:

![许可协议](https://i.imgur.com/IbUJtIk.png)

- [ ] 演示下载流程

<a id="markdown-22-jdk的安装" name="22-jdk的安装"></a>
### 2.2. JDK的安装

<a id="markdown-221-傻瓜式安装" name="221-傻瓜式安装"></a>
#### 2.2.1. 傻瓜式安装

<a id="markdown-222-推荐的安装方式" name="222-推荐的安装方式"></a>
#### 2.2.2. 推荐的安装方式

- 安装路径不要有中文或者特殊符号如空格等
- 所有和开发相关的软件最好安装目录统一

4、双击安装程序，执行JDK安装包, 选择多次 **下一步** 完成安装:

![安装](https://i.imgur.com/sw2XivY.png)

5、安装后, 到 `Program Files\Java` 文件夹查看安装情况:

![查看目录](https://i.imgur.com/nxkrfbB.png)

- [ ] 演示安装流程

<a id="markdown-23-验证安装是否成功" name="23-验证安装是否成功"></a>
### 2.3. 验证安装是否成功

通过DOS命令，切换到JDK安装的bin目录下

```bash
C:\Program Files\Java\jdk1.8.0_161\bin
```

然后分别输入 `javac` 和 `java`，如果正常显示一些内容，说明安装成功

<a id="markdown-3-jdk-安装路径下的目录解释" name="3-jdk-安装路径下的目录解释"></a>
## 3. JDK 安装路径下的目录解释

<a id="markdown-31-bin目录" name="31-bin目录"></a>
### 3.1. bin目录

该目录用于存放一些可执行程序

- java编译器: `javac.exe`
- java运行工具: `java.exe`
- 打包工具: `jar.exe`
- 文档生成工具: `javadoc.exe`

<a id="markdown-32-db目录" name="32-db目录"></a>
### 3.2. db目录

db目录是一个小型的数据库

从 jdk 6.0开始，Java中引用了一个新的成员JavaDB

这是一个纯Java实现、开源的数据库管理系统。

在学习`JDBC`时，不再需要额外地安装一个数据库软件，选择直接使用JavaDB即可

<a id="markdown-33-jre目录" name="33-jre目录"></a>
### 3.3. jre目录

此目录是Java运行时环境的根目录

它包括Java虚拟机，运行时的类包，Java应用启动器以及一个bin目录，但不包含开发环境中的开发工具。

<a id="markdown-34-include目录" name="34-include目录"></a>
### 3.4. include目录

由于JDK是通过C和C++实现的，因此在启动时需要引入一些C语言的头文件，该目录就是用于存放这些头文件

<a id="markdown-35-lib目录" name="35-lib目录"></a>
### 3.5. lib目录

lib是library的缩写，意为Java类库或库文件，是开发工具使用的归档包文件

<a id="markdown-36-srczip文件" name="36-srczip文件"></a>
### 3.6. src.zip文件

src.zip为src文件夹的压缩文件

src中放置的是JDK核心类的源代码，通过该文件可以查看Java基础类的源代码