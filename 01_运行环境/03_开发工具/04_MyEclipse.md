# 004_MyEclipse

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. MyEclipse工具介绍](#1-myeclipse工具介绍)
- [2. 下载安装配置开发环境 MyEclipse](#2-下载安装配置开发环境-myeclipse)
- [3. MyEclipse的安装](#3-myeclipse的安装)
- [4. MyEclipse常用骚操作](#4-myeclipse常用骚操作)

<!-- /TOC -->

---

<a id="markdown-1-myeclipse工具介绍" name="1-myeclipse工具介绍"></a>
## 1. MyEclipse工具介绍

[MyEclipse](https://baike.baidu.com/item/MyEclipse) 是在 Eclipse 基础上加上自己的插件开发而成的功能强大的企业级集成开发环境，主要用于 Java、Java EE 以及 移动应用的开发。

MyEclipse 的功能非常强大，支持也十分广泛，尤其是对各种开源产品的支持相当不错。

<a id="markdown-2-下载安装配置开发环境-myeclipse" name="2-下载安装配置开发环境-myeclipse"></a>
## 2. 下载安装配置开发环境 MyEclipse

<a id="markdown-3-myeclipse的安装" name="3-myeclipse的安装"></a>
## 3. MyEclipse的安装

<a id="markdown-4-myeclipse常用骚操作" name="4-myeclipse常用骚操作"></a>
## 4. MyEclipse常用骚操作