
# 003_VsCode

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. VSCode 工具介绍](#1-vscode-工具介绍)
- [2. 下载安装配置开发环境 VSCode](#2-下载安装配置开发环境-vscode)
- [3. VSCode的安装](#3-vscode的安装)
- [4. VSCode常用骚操作](#4-vscode常用骚操作)

<!-- /TOC -->

---

<a id="markdown-1-vscode-工具介绍" name="1-vscode-工具介绍"></a>
## 1. VSCode 工具介绍

<a id="markdown-2-下载安装配置开发环境-vscode" name="2-下载安装配置开发环境-vscode"></a>
## 2. 下载安装配置开发环境 VSCode

<a id="markdown-3-vscode的安装" name="3-vscode的安装"></a>
## 3. VSCode的安装

<a id="markdown-4-vscode常用骚操作" name="4-vscode常用骚操作"></a>
## 4. VSCode常用骚操作