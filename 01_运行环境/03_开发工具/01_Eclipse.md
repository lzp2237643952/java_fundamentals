# 1. _Eclipse

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

- [1. _Eclipse](#1--eclipse)
    - [1. 录播视频](#1)
        - [1.1. 运用cmd编写HelloWorld](#11-cmdhelloworld)
        - [1.2. Eclipse说明](#12-eclipse)
        - [1.3. 运用eclipse编写HelloWorld01](#13-eclipsehelloworld01)
        - [1.4. 运用eclipse编写HelloWorld02](#14-eclipsehelloworld02)
        - [1.5. 创建项目结构](#15)
    - [2. Eclipse 工具](#2-eclipse)
    - [3. 下载安装配置开发环境 Eclipse](#3--eclipse)
    - [4. Eclipse中的若干骚操作](#4-eclipse)
        - [4.1. 修改Eclipse里项目的包层次结构排版](#41-eclipse)
        - [4.2. Eclipse重新编译项目](#42-eclipse)
        - [4.3. 背景色设置](#43)
        - [4.4. Eclipse快捷键的使用](#44-eclipse)
        - [4.5. 为Eclipse添加更多提示功能](#45-eclipse)
        - [4.6. Eclipse中修改系统字体大小](#46-eclipse)

---

集成开发环境（**IDE**，Integrated Development Environment ）是专为程序员提供的应用软件，这些软件往往具备功能强大的图形界面, 在 IDE 的辅助下，程序员可以更加高效的完成编译、调试、提交、重构等工作。

作为一个合格的程序员应该对主流的IDE工具有较高的熟练度，但也要防止 **过分依赖IDE** 问题。

<a id="markdown-1-录播视频" name="1-录播视频"></a>
## 1. 录播视频

<a id="markdown-11-_运用cmd编写helloworld" name="11-_运用cmd编写helloworld"></a>
### 1.1. 运用cmd编写HelloWorld

- [腾讯视频](https://v.qq.com/x/page/m06469cbicl.html)
- [B站](https://www.bilibili.com/video/av23106417/?p=5)
- [优酷](https://v.youku.com/v_show/id_XMzU1MTU3NTA2OA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19ry84auc1.html#vfrm=8-8-0-1)

<a id="markdown-12-_eclipse说明" name="12-_eclipse说明"></a>
### 1.2. Eclipse说明

- [腾讯视频](https://v.qq.com/x/page/p0646olpbpl.html)
- [B站](https://www.bilibili.com/video/av23106417/?p=6)
- [优酷](https://v.youku.com/v_show/id_XMzU1MTU3NTA5Ng==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19ry84js59.html#vfrm=8-8-0-1)

<a id="markdown-13-_运用eclipse编写helloworld01" name="13-_运用eclipse编写helloworld01"></a>
### 1.3. 运用eclipse编写HelloWorld01

- [腾讯视频](https://v.qq.com/x/page/i0646caggpb.html)
- [B站](https://www.bilibili.com/video/av23106417/?p=7)
- [优酷](https://v.youku.com/v_show/id_XMzU1MTU3NTExMg==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19ry85eaf9.html#vfrm=8-8-0-1)

<a id="markdown-14-_运用eclipse编写helloworld02" name="14-_运用eclipse编写helloworld02"></a>
### 1.4. 运用eclipse编写HelloWorld02

- [腾讯视频](https://v.qq.com/x/page/q0646314si6.html)
- [B站](https://www.bilibili.com/video/av23106417/?p=8)
- [优酷](https://v.youku.com/v_show/id_XMzU1MTU3Njc3Ng==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19ry84ardt.html#vfrm=8-8-0-1)

<a id="markdown-15-_创建项目结构" name="15-_创建项目结构"></a>
### 1.5. 创建项目结构

- [腾讯视频](https://v.qq.com/x/page/v0646mcr40f.html)
- [B站](https://www.bilibili.com/video/av23106417/?p=9)
- [优酷](https://v.youku.com/v_show/id_XMzU1MTU4MDI2NA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19ry84jtf9.html#vfrm=8-8-0-1)

<a id="markdown-2-eclipse-工具" name="2-eclipse-工具"></a>
## 2. Eclipse 工具

对于Java程序员有许多IDE环境可以选择，但最主流的要数大名鼎鼎的Eclipse了。

[Eclipse](https://baike.baidu.com/item/eclipse/61703) 是目前主流的 IDE 开发环境。是 IBM 斥资数千万美元打造的开源项目，如今几乎统治了IDE市场。

除了开源之外，Eclipse成功的最大原因在于它是基于 **插件** 的特性。

Eclipse 本身是一个平台框架，提供标准的服务，众第三方厂商可以通过开发插件扩展Eclipse的功能，

相较于其他功能相对固定的IDE，Eclipse具有高度的灵活性。

Eclipse 界面

![Eclipse 界面](https://i.imgur.com/5C14JvJ.jpg)

<a id="markdown-3-下载安装配置开发环境-eclipse" name="3-下载安装配置开发环境-eclipse"></a>
## 3. 下载安装配置开发环境 Eclipse

1、用浏览器访问 <http://www.eclipse.org> 下载Eclipse:

![官网](https://i.imgur.com/s6vmkvu.png)

2、进入下载页面,下载Java EE版本的Eclipse:

![下载页面](https://i.imgur.com/nm00x2i.png)

3、在下载镜像选择页面, 点击 Download 即可:

![下载](https://i.imgur.com/DEHYcIw.png)

4、Eclipse会自动开始下载:

![自动下载](https://i.imgur.com/ba7hJXQ.png)

5、Eclipse是绿色软件, 没有安装过程, 释放zip包即可:

![压缩包](https://i.imgur.com/1vJ39s7.png)

6、进入Eclipse文件夹, 双击eclipse.exe, 就可以执行Eclipse程序:

![可执行文件](https://i.imgur.com/vDh7oLB.png)

<a id="markdown-4-eclipse中的若干骚操作" name="4-eclipse中的若干骚操作"></a>
## 4. Eclipse中的若干骚操作

<a id="markdown-41-修改eclipse里项目的包层次结构排版" name="41-修改eclipse里项目的包层次结构排版"></a>
### 4.1. 修改Eclipse里项目的包层次结构排版

Eclipse新建工程和若干包默认的排序是扁平的Flat（下图）

![ex01](../../assets/eclipse3.png)

显示出的包结构不是很清晰，通过设置可以调整为包继承的结构Hierarchical模式
点击Window-->Navigation-->Show View Menu-->Package Presentation--Hierarchical
调整后的结果如下图所示

![eclipse03](../../assets/eclipse3.png)

还有一种更简便的方法：点击如下图三的倒三角形图标 `Package Presentation-Hierarchical` ，这种方法最实用。

![eclipse04](../../assets/eclipse04.png)

<a id="markdown-42-eclipse重新编译项目" name="42-eclipse重新编译项目"></a>
### 4.2. Eclipse重新编译项目

项目代码修改后往往需要重新编译以免运行的时候出错

菜单栏点击 `project-->clean` 选项

将工程中的 `.class` 文件删除，同时重新编译工程。

![eclipse05](../../assets/eclipse05.png)

<a id="markdown-43-背景色设置" name="43-背景色设置"></a>
### 4.3. 背景色设置

经常对着电脑对眼睛难免有伤害，可以将编辑页面的背景颜色设置为豆沙绿护眼色，当然也可以设置为您想要的其他颜色。

具体骚操作如下

点击 `Window>Preferences>General>Editors>Text Editors>Background color` 即可进行设置。

![eclipae06](../../assets/eclipse06.png)

![eclipse07](../../assets/eclipse07.png)

<a id="markdown-44-eclipse快捷键的使用" name="44-eclipse快捷键的使用"></a>
### 4.4. Eclipse快捷键的使用

熟练使用快捷键可以充分提高编程效率。

1、重命名快捷键

`Alt+Shift+R` 可用于类名，方法名，属性名等的重命名

鼠标点击要重命名的地方，按住 `Alt+Shift+R`

会提示重新输入名字，此时重新键入新的名字

按 `Enter` 键即可改名，不管需要重命名的类

还是调用了该类的其他类名字都会改变

如果重命名的是类中的属性则按住快捷键两次，可实现get/set方法的自动重命名

![eclipse08](../../assets/eclipse08.png)

2、格式化代码

`Ctrl+Shift+F` 用于格式化代码，此格式化操作可以使用默认的配置，也可以自己设定。

如果想整个类格式化，可以先 `Ctrl+A` 全选，再执行格式化操作。

格式化也可以通过在代码区右键 `Source-->Format` 来实现。

![eclipse09](../../assets/eclipse09.png)

3、自动导入包及清除多余的包

在编写 `java` 代码时需要引入已有的 `jar` 包，但是手动一个个导包很麻烦，使用快捷键 `Ctrl+Shift+O` ，选择需要的 `jar` 包后确认，就可以实现自动导入整个类依赖的所有 `jar` 包和清除不用的 `jar` 包。

![eclipse10](../../assets/eclipse10.png)

4、把多行代码抽象为一个方法（提取方法）

`Alt+Shift+M` 快捷键，在代码编写的过程中，一个方法写的过长会影响阅读和调试，因此可以把某部分功能块的代码抽象出来单独作为一个方法。

同样 `Alt+Shift+L` 用于提取本地变量。

5、其他常用的快捷键

删除代码：`Ctrl+D` 用于删除代码

新建：`Ctrl+N`，可以现在需要新建的工程等。

6、代码注释

代码中加入注释可以让代码的可读性更高。

`Ctrl+/` 用于单行注释，去掉单行注释的快捷键是同样的

选中要注释的代码键入 `Ctrl+Shift+/` 可用于多行注释，光标在区域内键入 `Ctrl+Shift+\` 用于取消注释。

![eclipse11](../../assets/eclips11.png)

![eclipse12](../../assets/eclips12.png)

7、复制代码

`Ctrl+Alt+向下键箭头`，用于向下复制一行代码

`Ctrl+Alt+向上键箭头` ，用于向上复制一行代码。

8、创建空白行

`Shift+Enter` 在当前行下创建一个空白行，与光标是否在行末无关。

`Ctrl+Shift+enter` 在当前行之前插入空白行。

9、移动代码

`Alt+方向键` ，将当前行上下移动。

<a id="markdown-45-为eclipse添加更多提示功能" name="45-为eclipse添加更多提示功能"></a>
### 4.5. 为Eclipse添加更多提示功能

我们在使用Eclipse编辑代码时，常用的提示信息往往出现在输入符号 `.` 的时候

如输入类名 `.` 会出现该类下的所有方法列表，这样的提示可能不太方便

如果能在输入任意一个字母时就有相应的提示，会更便捷。

因此我们进行如下操作：

`Window-->Perferences-->Java-->Editor--Content Assist` 会有如下图所示页面，

将 `Auto activation triggers for java` 内容更改为：

`.abcdefghigklmnopqrstuvwxyz`

`Auto activation delay(ms)` 中的时延也可以设置如60ms，使得提示更加实时，设置完点击 `Apply` 即可。

这样在输入任意一个字符时便会有提示出现。

![eclipse13](../../assets/eclips13.png)

<a id="markdown-46-eclipse中修改系统字体大小" name="46-eclipse中修改系统字体大小"></a>
### 4.6. Eclipse中修改系统字体大小

`Window-->Preferences -->General-->Apperance-->Colors and Fonts -->Basic-->Text Font-->Edit` 通常我们习惯的字体大小设置如下图所示。

![eclipse14](../../assets/eclipse14.png)
