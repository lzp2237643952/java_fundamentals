# 002_IntelliJIDEA

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. IntelliJIDEA 工具介绍](#1-intellijidea-工具介绍)
- [2. 下载安装配置开发环境 IDEA](#2-下载安装配置开发环境-idea)
- [3. IDEA的安装](#3-idea的安装)
- [4. IDEA常用骚操作](#4-idea常用骚操作)

<!-- /TOC -->

---

<a id="markdown-1-intellijidea-工具介绍" name="1-intellijidea-工具介绍"></a>
## 1. IntelliJIDEA 工具介绍

<a id="markdown-2-下载安装配置开发环境-idea" name="2-下载安装配置开发环境-idea"></a>
## 2. 下载安装配置开发环境 IDEA

<a id="markdown-3-idea的安装" name="3-idea的安装"></a>
## 3. IDEA的安装

<a id="markdown-4-idea常用骚操作" name="4-idea常用骚操作"></a>
## 4. IDEA常用骚操作