import java.util.Arrays;

//数组的演示
public class ArrayDemo2 {
	public static void main(String[] args) {
		// 数组复制
		int[] a = { 10, 20, 30, 40, 50 };
		// a:源数组
		// a1:目标数组
		// 4:目标数组的长度(元素个数)
		int[] a1 = Arrays.copyOf(a, 4);// 灵活性差,效率低
		for (int i = 0; i < a1.length; i++) {
			System.out.println(a1[i]);
		}
	}
}
