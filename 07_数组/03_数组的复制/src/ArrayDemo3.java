import java.util.Arrays;

//数组的演示
public class ArrayDemo3 {
	public static void main(String[] args) {
		// 数组复制
		int[] a = { 10, 20, 30, 40, 50 };
		// 数组的扩容(创建一个新的数组,并将源数组中的数据复制进去)
		a = Arrays.copyOf(a, a.length - 1);
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}

}
