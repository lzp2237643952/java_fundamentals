# 07_03_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---
## [ArrayDemo.java](ArrayDemo.java)

**概要**

编写数组的复制

使用 `System.arraycopy(src, srcPos, dest, destPos, length)` 方法

- src:源数组
- srcPos:源数组的起始下标
- dest:目标数组
- destPos：目标数组的起始下标
- length：要复制的元素个数

**学习目标**

- 学会使用System.arraycopy方法
- 学会自己复制一个数组

## [ArrayDemo2.java](ArrayDemo2.java)

**概要**

编写数组的复制，使用 `Arrays.copyOf(original, newLength)` 方法

- original：源数组
- newLength：目标数组的长度

**学习目标**

- 学会使用Arrays.copyOf()方法
- 学会自己复制一个数组

## [ArrayDemo3.java](ArrayDemo3.java)

**概要**

编写数组的复制，类似数组的扩容(创建一个新的数组,并将源数组中的数据复制进去),

使用 `Arrays.copyOf(original, newLength)` 方法

- original：源数组
- newLength：目标数组的长度可以在源数组的基础上随意更改

**学习目标**

- 学会使用Arrays.copyOf()方法
- 学会自己复制一个数组
