//数组的演示
public class ArrayDemo {
	public static void main(String[] args) {
		// 数组复制
		int[] a = { 10, 20, 30, 40, 50 };
		int[] a1 = new int[6];
		// a:源数组
		// 1:源数组的起始下标
		// a1:目标数组
		// 0:目标数组的起始下标
		// 4:要复制的元素个数
		System.arraycopy(a, 1, a1, 0, 4);// 灵活性好,效率高
		for (int i = 0; i < a1.length; i++) {
			System.out.println(a1[i]);
		}
	}

}

/*
 * int[] a={10,20,30,40,50}; int[] b=new int[6];
 * 
 * for(int i=0;i<a.length;i++){ b[i]=a[i]; }
 *
 * b[0]=a[0]; b[1]=a[1]; b[2]=a[2]; b[3]=a[3]; b[4]=a[4];
 */
