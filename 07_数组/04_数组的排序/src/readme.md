# 07_04_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---
## [ArrayMax.java](ArrayMax.java)

**概要**

编写数组的排序，将数组元素的最大值放在数组最后一个一个元素的下一个位置

**学习目标**

* 学会数组排序算法
* 学会自己声明一个数组并排序

## [BubbleSort.java](BubbleSort.java)

**概要**

编写数组的冒泡排序算法，每一轮都是从第一个元素开始冒,每一次都是和它的下一个元素，冒出来的就不带参与比较了

**学习目标**

* 学会数组冒泡排序算法
* 学会自己声明一个数组并排序

## [BubbleSort2.java](BubbleSort2.java)

**概要**

编写数组的排序算法，升序排序为若前数大于后数则交换,保证前数小于后数，降序排序为若前数小于后数则交换,保证前数大于后数

**学习目标**

* 学会数组排序算法
* 学会自己声明一个数组并排序

## [ArrayMethodTest.java](ArrayMethodTest.java)

**概要**

实现数组的遍历,带中括号输出

**学习目标**

独立完成代码，并能举一反三

## [ArrayMethodTest_1.java](ArrayMethodTest_1.java)

**概要**

实现数组翻转,数组中的元素,进行位置上的交换

**学习目标**

## [ArrayMethodTest_2.java](ArrayMethodTest_2.java)

**概要**

实现数组排序,选择和冒泡

**学习目标**

## [ArrayMethodTest_3.java](ArrayMethodTest_3.java)

**概要**

实现数组查找,折半和普通

**学习目标**

## [LoopTest.java](LoopTest.java)

**概要**

实现数组查找,折半和普通

**学习目标**

## [LoopTest_1.java](LoopTest_1.java)

**概要**

 计算出水仙花数

**学习目标**

## [LoopTest_3.java](LoopTest_3.java)

**概要**

 利用嵌套for循环,实现99乘法表示

**学习目标**
