//数组的排序
public class BubbleSort2 {
	public static void main(String[] args) {
		int[] arr = { 56, 23, 45, 2 };
		for (int i = 0; i < arr.length - 1; i++) {// 控制轮
			for (int j = 0; j < arr.length - 1 - i; j++) {// 控制次
				if (arr[j] > arr[j + 1]) {
					int t = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = t;
				}
				// 若前数大于后数则交换,保证前数小于后数-----升序
				// 若前数小于后数则交换,保证前数大于后数-----降序
			}
		}
		/*
		 * i=0 j=0 56和23比, 换 ,23,56,45,2 j=1 56和45比, 换 ,23,45,56,2 j=2 56和2比,
		 * 换,23,45,2,56----------56冒出来了 i=1 j=0 23和45比,不换 j=1
		 * 45和2比,换,23,2,45,56------------45冒出来了 i=2 j=0
		 * 23和2比,换,2,23,45,56------------23冒出来了
		 */
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
	}

}

/*
 * arr.length-1-i i=0(第一轮),比3次 i=1(第二轮),比2次 i=2(第三轮),比1次
 */
