/*
    计算出水仙花数
     三位数 100-999  个位数的立方+十位数的立方+百位数的立方 = 自己本身
	 153 = 1*1*1 + 5*5*5 + 3*3*3
*/
public class LoopTest_1{
	public static void main(String[] args){
		int bai = 0;
		int shi = 0;
		int ge = 0 ;
		
		for(int i = 100 ; i < 1000 ; i++){
			bai = i / 100;
			shi = i / 10 % 10;
			ge = i % 10;
			if(bai * bai * bai + shi * shi *shi + ge * ge *ge == i){
				System.out.println(i);
			}
		}
	}
}




