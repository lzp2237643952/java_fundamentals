/*
  数组的排序: 一般都是升序排列,元素,小到大的排列
  
  两种排序的方式
     选择排序: 数组的每个元素都进行比较
	 冒泡排序: 数组中相邻元素进行比较
	 规则: 比较大小,位置交换
*/
public class ArrayMethodTest_2{
	public static void main(String[] args){
		int[] arr  = {3,1,4,2,56,7,0};
		
		bubbleSort(arr);
		printArray(arr);
	}
	public static void bubbleSort(int[] arr){
		for(int i = 0 ; i < arr.length - 1; i++){
			for(int j = 0 ; j < arr.length-i-1; j++){
				if(arr[j] > arr[j+1]){
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
	}
	
	/*
	    实现数组的选择排序
	*/
	public static void selectSort(int[] arr){
		for(int i = 0 ; i < arr.length - 1; i++){
			for(int j = i+1 ; j < arr.length ; j++){
				if(arr[i] > arr[j]){
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp; 
				}
			}
		}
	}

	public static void printArray(int[] arr){
		System.out.print("[");
		for(int i = 0 ; i < arr.length ; i++){
			if( i == arr.length-1 ){
				System.out.print(arr[i]+"]");
			}else{
			    System.out.print(arr[i]+",");
			}
		}
		System.out.println();
	}
}


