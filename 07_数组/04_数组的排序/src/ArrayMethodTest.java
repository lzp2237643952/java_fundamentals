/*
    定义方法,实现数组的遍历
	遍历中,输出结果  [11,33,565,66,78,89]
	int[] arr = {3,4,45,7};
	结果包含字符串, [  ]  ,
*/
public class ArrayMethodTest{
	public static void main(String[] args){
		int[] arr = {11,44,55,33,66};
		printArray(arr);
		
		int[] arr2 = {22,88,99,33,66};
		printArray(arr2);
		
	}
	public static void printArray(int[] arr){
		System.out.print("[");
		for(int i = 0 ; i < arr.length ; i++){
			if( i == arr.length-1 ){
				System.out.print(arr[i]+"]");
			}else{
			    System.out.print(arr[i]+",");
			}
		}
		System.out.println();
	}
}



