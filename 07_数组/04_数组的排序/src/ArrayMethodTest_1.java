/*
   数组的逆序:
     数组中的元素,进行位置上的交换
	 逆序 不等于 反向遍历
	 反转 reverse
*/
public class ArrayMethodTest_1{
	public static void main(String[] args){
		int[] arr = {3,5,7,1,0,9,-2};
		reverse(arr);
		printArray(arr);
	}
	
	/*
	   翻转
	*/
	public static void reverse(int[] arr){
		for( int min = 0 , max = arr.length-1 ; min < max  ; min++,max--){
			int temp = arr[min];
			arr[min] =  arr[max];
			arr[max] = temp;
		}
	}
	
	/*
	   遍历数组
	*/
	public static void printArray(int[] arr){
		System.out.print("[");
		for(int i = 0 ; i < arr.length ; i++){
			if( i == arr.length-1 ){
				System.out.print(arr[i]+"]");
			}else{
			    System.out.print(arr[i]+",");
			}
		}
		System.out.println();
	}
}





