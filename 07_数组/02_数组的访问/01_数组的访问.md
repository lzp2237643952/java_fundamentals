# 001_数组的访问

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---
<!-- TOC -->

- [1. 获取数组的长度](#1-获取数组的长度)
    - [录播视频:数组的访问](#录播视频数组的访问)
- [2. 通过下标访问数组元素](#2-通过下标访问数组元素)
    - [录播视频:通过下标获取数组元素](#录播视频通过下标获取数组元素)
- [3. 遍历数组元素](#3-遍历数组元素)
    - [录播视频:数组的遍历演示01](#录播视频数组的遍历演示01)
    - [录播视频:数组的遍历演示02](#录播视频数组的遍历演示02)

<!-- /TOC -->

---

<a id="markdown-1-获取数组的长度" name="1-获取数组的长度"></a>
## 1. 获取数组的长度

通过调用数组的length属性可以获取到数组的长度

```java
int[] arr = {1, 2, 34, 6, 75, 4};
int len = arr.length;
System.out.println("数组的长度=" + len);  //输出数组的长度为6
```

<a id="markdown-录播视频数组的访问" name="录播视频数组的访问"></a>
### 录播视频:数组的访问

- [腾讯视频](https://v.qq.com/x/page/z06338tfa5s.html)
- [B站视频](https://www.bilibili.com/video/av23114182/?p=7)
- [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI4NDkyOA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺视频](http://www.iqiyi.com/w_19rxn8ocll.html#vfrm=8-8-0-1)

<a id="markdown-2-通过下标访问数组元素" name="2-通过下标访问数组元素"></a>
## 2. 通过下标访问数组元素

除了通过length属性还可以通过下标访问数组中的元素
注意：通过下标访问的时候**下标从 0 开始,最大到 length-1**

<a id="markdown-录播视频通过下标获取数组元素" name="录播视频通过下标获取数组元素"></a>
### 录播视频:通过下标获取数组元素

- [腾讯视频](https://v.qq.com/x/page/w0633g89moe.html)
- [B站视频](https://www.bilibili.com/video/av23114182/?p=8)
- [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI4NDcxMg==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺视频](http://www.iqiyi.com/w_19rxn9257p.html#vfrm=8-8-0-1)

<a id="markdown-3-遍历数组元素" name="3-遍历数组元素"></a>
## 3. 遍历数组元素

如果我们想要全部拿到数组中的元素，可以对数组进行遍历

遍历数组元素通常选用for循环语句

**循环变量作为访问数组元素的下标,可以访问数组中的每一个元素范围为:0 ~ length-1**

```java
int[] arr = new int[10];
for (int i = 0; i < arr.length; i++) {
    arr[i] = 100;
}
```

<a id="markdown-录播视频数组的遍历演示01" name="录播视频数组的遍历演示01"></a>
### 录播视频:数组的遍历演示01

- [腾讯视频](https://v.qq.com/x/page/n0633fmgthc.html)
- [B站视频](https://www.bilibili.com/video/av23114182/?p=9)
- [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI4NDczMg==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺视频](http://www.iqiyi.com/w_19rxn85ukh.html#vfrm=8-8-0-1)

<a id="markdown-录播视频数组的遍历演示02" name="录播视频数组的遍历演示02"></a>
### 录播视频:数组的遍历演示02

- [腾讯视频](https://v.qq.com/x/page/p06330pddmy.html)
- [B站视频](https://www.bilibili.com/video/av23114182/?p=10)
- [优酷视频](https://v.youku.com/v_show/id_XMzU5MDI5OTk5Mg==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺视频](http://www.iqiyi.com/w_19rxn921bh.html#vfrm=8-8-0-1)

- [ ] <font color = red>**补充内容** 欢迎后期补充</font>

在Java5之后出现了一个foreach语句,是for语句的特殊简化版本,用来进行数组的遍历,

比for循环效率更高, 任何的foreach语句都可以改写成for语句

```java
int arr []  = {1,2,34,57,78,6};
for(int i:arr){
    System.out.println(i);
}
//输出 i = 1, 2, 34, 57, 78, 6
```
