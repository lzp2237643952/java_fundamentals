public class ArrayAccess {
    public static void main(String[] args) {
        int[] array = new int[5];
        // 通过循环，初始化一个长度为5的整型数组
        for (int i = 0; i < array.length; i++) {
            array[i] = i + 1;
        }
        // 在通过循环，遍历每一个元素，并输出
        for (int j = 0; j < array.length; j++) {
            System.out.println(array[j]);
        }
    }
}
