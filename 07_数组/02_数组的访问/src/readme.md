# 07_02_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---
## [ArrayOutput1.java](ArrayOutput1.java)

**概要**

通过length访问数组的长度

范围:0 ~ length-1

**学习目标**

* 熟练使用length输出数组的长度

## [ArrayOutput.java](ArrayOutput.java)

**概要**

通过下标访问数组的长度,并用循环遍历输出

**学习目标**

* 熟练掌握循环遍历数组的长度
