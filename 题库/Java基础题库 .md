# Java基础题库

<!-- TOC -->
- [Java基础题库](#java)
	- [1. 运行环境](#1)
		- [在Java中，关于main()方法和类文件的说法错误的是()](#javamain)
		- [以下关于Java语言特点描述错误的是()](#java)
		- [在Java中，类A1为public公有类，那么保存它的源文件名可以是()](#javaa1public)
		- [在Java中，以下导入包的语句正确的是()](#java)
		- [在Java中，以下说法正确的是()](#java)
		- [阅读Test.java文件中的程序代码](#testjava)
		- [在java中，关于main()方法和类文件的说法错误的是()](#javamain)
		- [在JAVA中，下列说法正确的是()](#java)
		- [在用JAVA语言进行程序开发时，需要遵循的步骤()](#java)
		- [在JAVA中，关于程序调试说法错误的是()](#java)
		- [在JAVA中经常使用System.out对象的print()与println()方法实现控制台输出，则下面选项中描述正确的是()](#javasystemoutprintprintln)
		- [在java中，关于编程规范的说法，错误的是()](#java)
		- [关于JAVA中使用包的说法不正确的是()](#java)
		- [在JAVA中,关于类的描述错误的是()](#java)
		- [在java中，对以下程序分析错误的是()](#java)
	- [2. 变量](#2)
		- [在Java中，以下变量命名正确的是()](#java)
		- [下列选项中使用Java变量错误的是()](#java)
		- [在Java中，以下错误的变量名是()](#java)
		- [在Java中，以下变量错误的是()](#java)
		- [在Java中，以下合法的变量名是()](#java)
		- [在java中，关于变量的说法不正确的是()](#java)
		- [在java中，以下变量命名正确的是()](#java)
		- [在JAVA中，定义变量的前面加上()，那便是说，这个变更一旦被初始化，其值便不可更改](#java)
		- [在JAVA中,变量命名正确的是()](#java)
		- [在JAVA中,下列关于String说法正确的是()](#java-string)
		- [在JAVA中,下面说法正确的是()](#java)
		- [在java的以下的变量赋值语句中，无法通过编译的是()](#java)
	- [3. 基本数据类型](#3)
		- [下面Java代码的输出结果是()](#java)
		- [运行如下Java代码并输入10，则输出结果为()](#java10)
		- [在JAVA中，有下面代码，其中可以正确编译的是()](#java)
		- [在JAVA中,有下面代码,其中可以正确编译的是()](#java)
		- [在JAVA中,下列说法错误的是()](#java)
		- [在Java程序中，对于数据类型为boolean的变量x,可以采用的赋值方式是()](#javabooleanx)
		- [在Java中，以下()不属于Java基本数据类型](#javajava)
	- [4. 运算符和表达式](#4)
		- [在Java中，下面代码的运行结果是()](#java)
		- [在Java中，以下程序的输出结果是()](#java)
		- [在Java中，System.out.println("非常Happy!".toUpperCase());语句的输出结果是()](#javasystemoutprintlnhappytouppercase)
		- [Java中的语句 int num = 3+5*8%6-1;运行后，num变量的值是()](#java-int-num-3586-1-num)
		- [在Java中，以下程序的输出结果为()](#java)
		- [在java中，下面选项输出结果为true的是()](#javatrue)
		- [以下Java程序会输出()](#java)
		- [在Java中，以下赋值语句正确的是()](#java)
		- [在Java中，有如下代码段，当i和j分别为()时，输出结果是"你好！"](#javaij)
		- [在Java中，以下程序的运行结果是()](#java)
		- [在Java中，以下关于类与对象描述错误的是()](#java)
		- [在Java操作符中，以下选项优先级最高的是()](#java)
		- [在Java中，以下程序的运行结果是()](#java)
		- [在Java中，以下代码的运行结果是()](#java)
		- [在Java中，以下代码的运行结果是()](#java)
		- [在Java中，以下代码运行的结果是()](#java)
		- [在Java中，以下程序的输出结果是()](#java)
		- [在java中，以下代码的运行结果是()](#java)
		- [在java中，以下程序的执行结果是()](#java)
		- [在java中，以下代码的运行结果是()](#java)
		- [在java中，以下程序的输出结果是()](#java)
		- [在java中，以下程序的输出结果是()](#java)
		- [在java中，如下代码的输出结果是()](#java)
		- [在java 中，运行下面的代码输出结果是()](#java)
		- [在java中，下面选项输出结果为true的是()](#javatrue)
		- [在java中，System.out.println("非常happy！".toUpperCase());语句的输出结果是()。](#javasystemoutprintlnhappytouppercase)
		- [在java中的语句int num = 3+5*8%6-1;运行后num变量的值是()](#javaint-num-3586-1-num)
		- [在java语言中，下面语句的运行结果是()](#java)
		- [在java程序中，boolean变量赋值正确的是()](#javaboolean)
		- [在java程序中，如下代码输出的结果是()](#java)
		- [在java中,下列语句输出的结果是.()](#java)
		- [在java中,下列语句输出的结果是()](#java)
		- [在JAVA中，以下程序的输出结果为()](#java)
		- [在JAVA中,如下代码段的输出结果为()](#java)
		- [在JAVA中，如下代码的输出结果是()](#java)
		- [在下列java代码中，a的值是()](#javaa)
		- [在JAVA中，为boolean类型赋值正确的是()](#javaboolean)
		- [在JAVA中,如下代码的输出结果是()](#java)
		- [在java中，如下代码的输出结果是()](#java)
		- [在Java中，下列代码运行后的输出结果是()](#java)
		- [给定如下Java代码，编译运行后，将输出()](#java)
	- [5. 分支结构](#5)
		- [下面Java代码中，condition1和condition2取何值时，会输出"货到付款"四个字()](#javacondition1condition2)
		- [在Java中，关于main()方法和类文件的说法错误的是()](#javamain)
		- [在Java中，运行下面的代码,输出结果是()](#java)
		- [阅读下列Java代码片段，数据结果正确的是()](#java)
		- [执行下列Java代码片段，输出结果正确的是()](#java)
		- [在Java中，下列可以用作条件表达式的是()](#java)
		- [在Java中，如下代码的输出结果为()](#java)
		- [在Java中，为boolean类型变量赋值正确的是()](#javaboolean)
		- [如下的Java代码输出的结果是()](#java)
		- [如下的Java代码输出的结果是()](#java)
		- [下面Java代码中，condition1和condition2取何值时,会输出"货到付款"四个字()](#javacondition1condition2)
		- [如下的Java代码输出的结果是()](#java)
		- [下面Java代码的输出结果正确的是()](#java)
		- [在java中，以下程序的输出结果是()](#java)
		- [在JAVA中，运行如下代码，则输出结果为()](#java)
		- [在JAVA中,以下代码段中造成编译异常的原因是()](#java)
		- [在java语言中有如下代码，则输出结果为()](#java)
		- [在java中,运行如下代码,则输出结果为()](#java)
		- [在java中，运行下面的代码输出的结果是()](#java)
		- [在JAVA中,如下代码的输出结果是()](#java)
		- [在JAVA中,关于switch语句说法正确的是()](#java-switch)
		- [在JAVA中，运行下面代码后输入15，则输出结果是()](#java15)
		- [在java中，如下代码段的输出()](#java)
		- [在java中，关于switch选择结构说法错误的是()](#javaswitch)
		- [在java中，有如下代码段，当i 和j分别为()时，输出结果是"条件符合"](#javai-j)
		- [在JAVA中，使用switch语句时，如希望设置默认值，则需要使用()关键字](#javaswitch)
		- [分析如下Java程序段，程序编译运行结果是()](#java)
		- [在Java中，以下程序编译运行后的输出结果为()](#java)
		- [在Java语言中有如下代码，下列x的定义中，可以使该段代码输出100的是()](#javax100)
		- [在Java语言中，有如下代码：](#java)
		- [给定如下Java代码片段，编译运行时的结果是()](#java)
		- [分析下面的Java代码，当x=2时，运行结果是()](#javax2)
		- [给定如下Java代码片段，编译运行的结果是()](#java)
		- [给定某Java程序的main方法如下所示，该程序的运行结果是()](#javamain)
		- [在Java中，下列代码的运行结果是()](#java)
	- [6. 循环结构](#6)
		- [阅读下列Java代码，有()行出现编译错误。](#java)
		- [阅读下列Java代码片段,输出结果是( )。](#java)
		- [阅读下列Java代码片段，横线处填写( )输出结果为 "正确"。](#java)
		- [阅读下列Java代码，运行后正确的输出结果是( )。](#java)
		- [在Java中，以下程序的输出结果是( )](#java)
		- [在Java中，下面代码的运行结果是( )](#java)
		- [在Java中，以下程序的执行结果是( )](#java)
		- [在Java中，对以下程序分析错误的是( )。](#java)
		- [如下的Java代码输出的结果是( )](#java)
		- [如下的Java代码输出的结果是( )](#java)
		- [在Java中，以下程序的运行结果是()](#java)
		- [以下Java代码的输出结果为()](#java)
		- [阅读下列Java代码片段，横线处填写()输出结果为true。](#javatrue)
		- [阅读下列Java代码片段，如果从控制台上先输入"n"，再输入"Y"，则在控制台上输出共朗诵的次数是()](#javany)
		- [阅读下列Java代码，运行后正确的输出结果是()](#java)
		- [在Java中，如下代码的输出结果是()](#java)
		- [如下Java代码在控制台输出的结果是()](#java)
		- [在Java中，下列代码的输出结果为()](#java)
		- [在Java中运行下面代码输出的结果是().](#java)
		- [在Java中，以下程序的输出结果为()](#java)
		- [在java中，如下代码的输出结果是()](#java)
		- [在Java中，以下代码段的输出结果为()](#java)
		- [在Java中，下面代码输出结果为()](#java)
		- [下面java代码输出的结果是()](#java)
		- [下面java代码输出的结果是()](#java)
		- [下面java代码的输出结果是()](#java)
		- [在JAVA中,运行下面的代码,输出结果是()](#java)
		- [在java中，以下循环的执行次数是()](#java)
		- [在JAVA中,若要使下面程序的输出值为2,则应该从键盘给n输入的值是()](#java-2-n)
		- [在java中，如下代码的输出结果是()](#java)
		- [在JAVA中,运行如下代码,则输出结果为()](#java)
		- [在java中，运行如下代码，则输出结果为()](#java)
		- [在JAVA中，运行如下代码，则输出结果为()](#java)
		- [在JAVA中，运行如下代码，则输出结果为()](#java)
		- [在JAVA中，如下代码段的输出结果为()](#java)
		- [在java中，如下代码段的输出结果为()](#java)
		- [在JAVA中，如下代码的输出结果为()](#java)
		- [在JAVA中,如下代码段的输出结果为()](#java)
		- [在JAVA中，如下代码段的输出结果为()](#java)
		- [在java中，如下代码段的输出结果为()](#java)
		- [在java中，如下代码输出结果是()](#java)
		- [在java中,如下代码输出结果是()](#java)
		- [分析以下使用for循环的java代码，其最后的运行结果是()](#forjava)
		- [在Java中，给定代码片段如下所示，则编译运行后，输出结果是()](#java)
		- [在JAVA中,无论循环条件是什么，那个循环都将至少执行一次()](#java)
		- [分析下面的Java代码片段，编译运行后的输出结果是()](#java)
		- [编译并运行下面的Java代码，()会出现在输出结果中](#java)
		- [（单选题）在Java中，用来退出循环，将控制权转给程序的其他部分的关键字是什么()](#java)
		- [下面一段代码中break语句起到的作用是()](#break)
	- [7. 数组](#7)
		- [(单选题)定义一个数组](#)
		- [(单选题)下列数组的初始化正确的是()](#)
		- [(单选题)执行完以下代码](#)
		- [以下哪项说明是正确的()](#)
		- [(单选题)下面哪个是正确的()](#)
		- [(单选题)Given：](#given)
		- [(单选题)当编译并运行下面程序时会出现什么结果( )](#)
		- [(单选题)已知表达式](#)
		- [(单选题)对记录序列{314，298，508，123，486，145}按从小到大的顺序进行插入排](#314298508123486145)
		- [(多选题)下列说法错误的有()](#)
		- [(多选题)下列选项中创建数组能够编译通过的是()](#)
		- [在Java中，以下说法正确的是()。](#java)
		- [在Java中，以下声明数组错误的是()](#java)
		- [在Java中，以下对字符串数组定义错误的是()](#java)
		- [在java中,关于数组说法不正确的是()](#java)
		- [在JAVA中, 以下说法正确的是()](#java)
		- [在JAVA中，以下程序段能正确赋值的是()](#java)
		- [在java中，关于数组描述正确的是()](#java)
		- [在JAVA中,下列关于数组使用说法正确的是()](#java)
	- [8. 方法](#8)
		- [在Java中，以下关于类的方法说法不正确的是()](#java)
		- [以下关于Java中使用包的说法不正确的是()](#java)
		- [在使用Eclipse进行代码调试时，在调试视图中，按F5和F6键都是单步执行，它们的区别是()](#eclipsef5f6)
		- [在创建JAVA类时，使用()关键字声明包](#java)
		- [分析下面的Java程序段，编译运行后的输出结果是()](#java)
		- [在Java中，包有多种用途，但不包含()](#java)
		- [在Java中，包com中定义了类TestUtil，在com的子包util中定义了同名类TestUtil，给定如下Java代码，编译运行时，将发生()](#javacomtestutilcomutiltestutiljava)
		- [给定如下Java代码，编译运行后，输出的结果将是()](#java)
		- [给定如下Java程序的方法结构，则方法体实现语句正确的是()](#java)
		- [给定Java代码如下所示，则编译运行后，输出结果是()](#java)
		- [给定一个Java程序的代码如下所示，则编译运行后，输出结果是()](#java)
		- [在Java中，如果要在字符串类型对象s="java"中，得到字母 'v' 出现的位置，可使用以下()语句](#javasjava-v)
		- [给定某Java程序的main方法如下，该程序编译运行后的结果是()](#javamain)
		- [给定一个Java程序的代码如下所示，则编译运行后，输出结果是()](#java)
		- [有关Java中的类和对象，以下说法错误的是()](#java)
		- [给定如下Java代码片段，编译运行后，输出结果是()](#java)
		- [分析如下的Java代码，编译运行时将输出()](#java)
		- [分析如下的Java代码，编译运行的输出结果是()](#java)
		- [在Java中，源文件Test.java中包含如下代码段，则程序编译运行结果是()](#javatestjava)
		- [关于Java类中带参数的方法，下列选项中的说法错误的是()](#java)
		- [在java中，使用Arrays类对数组进行操作时，应在java源代码中编写的导入语句是()](#javaarraysjava)
		- [在java中,下面()语句可以在屏幕输出"hello,world!"](#java-hello-world)
		- [在JAVA中,下列说法中正确的是()](#java)
		- [在JAVA中,下列说法错误的是()](#java)
		- [在JAVA中,下列关于类与对象说法正确的是()](#java)
		- [在java中，下面说法正确的是()](#java)
		- [在JAVA中，能够去掉字符串前后空格的方法是()](#java)
		- [在JAVA中,关于类的方法,下列描述错误的是()](#java)
		- [在java中，关于类与对象的方法，正确的是()](#java)
		- [在JAVA中,运行下面的代码,输出结果是()](#java)
		- [在JAVA中，下面代码输出结果为()](#java)
		- [在JAVA中，如下代码段的输出结果为()](#java)
		- [在java中，运行下面代码后输入80，则输出结果是()](#java80)
		- [在Java中，关于输出语句的说法错误的是()](#java)
		- [在JAVA代码中，为求两数之和的方法添加javadoc注释正确的是()](#javajavadoc)
		- [下列java代码中，正确的是()](#java)
		- [在java中，关于main()方法说法正确的是()](#javamain)
		- [下列JAVA代码中，定义方法正确的是()](#java)
		- [在JAVA中，如下代码段输出结果为()](#java)
		- [下列JAVA代码，如果输出的结果产sohu,①②处应该填写()](#javasohu)
		- [在JAVA中，如下代码段的输出结果为()](#java)
		- [在JAVA中，关于trim()的用法说法正确的是()](#javatrim)
		- [在JAVA中，下面语句有()处错误](#java)
		- [在JAVA中,有HelloWorld.java的文件，其内容如下，则控制输出的内容是()](#java-helloworldjava)
		- [在java中，运行下面的代码，输出结果是()](#java)
		- [在JAVA中，如下代码段的输出结果为()](#java)
			- [在JAVA中，如下代码的输出结果是()](#java)
		- [在JAVA中，下面语句输出的结果是()](#java)
		- [在java中,运行如下代码段,则输出结果为()](#java)
		- [运行如下JAVA代码，输出结果为()](#java)
		- [在JAVA中，运行如下代码段，则输出结果为()](#java)
		- [在JAVA程序中,若出现](#java)
		- [在JAVA中,关于函数描述正确的是()](#java)
		- [在JAVA中,以下程序的输出结果为()](#java)
		- [在java语言中有如下代码，则输出结果为。()](#java)
		- [在java中main方法为入口方法，下列选项()是正确的写法](#javamain)
		- [关于java中方法的说法错误的是()](#java)
		- [在JAVA中，如下代码的输出结果为()](#java)
		- [在java中，如下代码横线处为()时，输出结果是"hello"](#javahello)
		- [在Java中,运行下列代码输出结果是()](#java)
		- [在Java中,如下代码段的输出结果为()](#java)
		- [在Java中，如下代码段的输出结果为()](#java)
		- [在Java中，已有](#java)
		- [在Java中，以下程序的运行结果是()](#java)
		- [在Java中,以下关于类与源文件描述错误的是()](#java)
		- [在Java中，以下关于类和对象描述正确的是()](#java)
		- [在Java中，以下代码的运行结果是()](#java)
		- [在Java中，以下程序的运行结果是()](#java)
		- [在Java中，如下代码段的输出结果为()](#java)
		- [在Java中，以下定义的方法可以编译通过的是()](#java)
		- [在java中，下面选项输出结果为true的是()](#javatrue)
		- [在Java中，关于程序调试说法正确的是( )](#java)
		- [在Java中，如下代码的输出结果为( )](#java)
		- [在Java中，如下代码的输出结果为( )](#java)
		- [如下的Java代码输出的结果是( )](#java)
		- [(单选题)阅读下列代码](#)
		- [(单选题)关于 Java 类中带参数的方法，下列选项中说法错误的是()](#java)
		- [(单选题)判断方法重载的依据，错误的是()](#)
	- [9. 项目案例](#9)

<!--/TOC -->

## 1. 运行环境

### 在Java中，关于main()方法和类文件的说法错误的是()

（选择一项）
- A) main()方法是Java程序的入口
- B) 在Java中必须用class关键字声明类
- C) Java源文件的扩展名为.java
- D) 在Java类中必须要有main()方法

【参考答案】：D

### 以下关于Java语言特点描述错误的是()

（选择一项）
- A) 是跨平台的编程语言
- B) 是面向过程的编程语言
- C) 具有运行时的多态特性
- D) 具有继承的特性

【参考答案】：B

### 在Java中，类A1为public公有类，那么保存它的源文件名可以是()

（选择一项）
- A) A1.class
- B) a1.class
- C) A1.java
- D) A1

【参考答案】：C

### 在Java中，以下导入包的语句正确的是()
（选择一项）
- A) import java.util.Random;
- B) import java.util.Random
- C) input java.util.Random;
- D) input java.util.Random

【参考答案】：A

### 在Java中，以下说法正确的是()
（选择一项）
- A) 运行程序之前必须调试程序
- B) 设置的断点在调试结束后会自动取消
- C) 类名首字母一般使用大写字母开头
- D) 遵守编码规范可以提高运行速度

【参考答案】：C

### 阅读Test.java文件中的程序代码

```java
public class Test{
	public static void main(String[]args){
		System.out.print("达内直播课");
		System.out.print("TLV");
	}
}
```

以上程序的输出结果为()

（选择一项）
- A) 达内直播课
        TLV

- B) 达内直播课TLV

- C) 达内直播课	TLV

- D) 编译错误

【参考答案】：B

### 在java中，关于main()方法和类文件的说法错误的是()
（选择一项）
- A) main()方法是java程序的入口
- B) 在java中必须用class关键字声明类
- C) java源文件的扩展名为.java
- D) 在java类中必须要有main()方法

【参考答案】：D

### 在JAVA中，下列说法正确的是()

（选择一项）
- A) 编写的源程序保存在扩展名为class文件中
- B) 源程序编译后积存在扩展名为java的文件中
- C) 编写的源程序必须先编译后才能运行
- D) 程序员可以读懂扩展名为class的文件

【参考答案】：C

### 在用JAVA语言进行程序开发时，需要遵循的步骤()

（选择一项）
- A) 创建项目并编写源程序
- B) 编译源程序
- C) 运行源程序
- D) 以上都包括

【参考答案】：D

### 在JAVA中，关于程序调试说法错误的是()
- A) 程序调试满足我们暂停程序，观察变量和逐条执行语句等功能
- B) 断点用来在调试的时候方便程序停在某一处，以便发现程序错误
- C) 使用Eclipse设计的步骤顺序为：启动调试，设置断点，单步执行，分析错误
- D) 设置的断点在调试结束后会自动取消

【参考答案】：CD

### 在JAVA中经常使用System.out对象的print()与println()方法实现控制台输出，则下面选项中描述正确的是()

（选择一项）
- A) print()可以实现控制台输出并实现换行
- B) println()在实现控制台输出前先进行换行
- C) print()实现控制台输出，但是不换行
- D) println()在控制台输出时，前后都进行换行

【参考答案】：C

### 在java中，关于编程规范的说法，错误的是()

（选择一项）
- A) 遵守编码规范可以增加代码的可读性
- B) 一行推荐写多条语句
- C) 类名一般使用大写字母开头
- D) 遵守编码规范可以使软件开发和维护更加方便

【参考答案】：B

### 关于JAVA中使用包的说法不正确的是()

（选择一项）
- A) 使用package关键字声明包
- B) 一个JAVA源文件可以有多个包声明语句
- C) JAVA包的名字通常是由小写字母组成，不能以圆点开头或结尾
- D) 如果要从键盘接收用户的输入，则导入包的语句如下：import java.util.*;

【参考答案】：B

### 在JAVA中,关于类的描述错误的是()

（选择二项）
- A) 类必须是公有的
- B) 类中可以定义属性和方法
- C) 定义类的关键字是Class
- D) 对象是类的实例

【参考答案】：AC

### 在java中，对以下程序分析错误的是()

```java
public class HelloWorld{
public static void main(String args[]){
System.out.println("HelloWorld");
	}
}
```

（选择一项）
- A) 该类文件的文件名称必须为HelloWorld.java
- B) public 修饰的类其文件名必须与类名一致
- C) 程序在输出HelloWorld后会自动换行
- D) java程序中一行只能写一条语句

【参考答案】：D

## 2. 变量

### 在Java中，以下变量命名正确的是()
（选择一项）
- A) 1_1
- B) 1-1
- C) _11
- D) 1__

【参考答案】：C

### 下列选项中使用Java变量错误的是()

（选择一项）
- A) int i = 5;
- B) double _d = 3.0;
- C) String $s = "考试过关";
- D) char c&2;

【参考答案】：D

### 在Java中，以下错误的变量名是()

（选择一项）
- A) constant
- B) flag
- C) a_b
- D) final

【参考答案】：D

### 在Java中，以下变量错误的是()
（选择一项）
- A) String str="";
- B) String str=new String();
- C) StringBuffer sb=new StringBuffer();
- D) Integer i=new Integer();

【参考答案】：D

### 在Java中，以下合法的变量名是()
（选择一项）
- A) _Sum
- B) var%
- C) 9var
- D) My name

【参考答案】：A

### 在java中，关于变量的说法不正确的是()
（选择一项）
- A) 变量必须先声明后使用
- B) 变量必须以字母、数字、下划线或"＄"符号开头
- C) 变量命名时不能使用java语言的关键字
- D) 在给变量赋值时使用"="运算符

【参考答案】：B

### 在java中，以下变量命名正确的是()
（选择二项）
- A) string
- B) void
- C) _&c
- D) _name

【参考答案】：AD

### 在JAVA中，定义变量的前面加上()，那便是说，这个变更一旦被初始化，其值便不可更改
（选择一项）
- A) final
- B) finally
- C) finalize
- D) const

【参考答案】：A

### 在JAVA中,变量命名正确的是()

（选择一项）
- A) ${student}
- B) 1_people
- C) _OK
- D) "name"

【参考答案】：C

### 在JAVA中,下列关于String说法正确的是()

（选择一项）
- A) String是Java的基本类型
- B) 调用String对象的size()方法可以获得字符串的长度
- C) 字符串的比较需要使用equals()方法
- D) String s='123';初始化一个字符串对象s,值为123

【参考答案】：C

### 在JAVA中,下面说法正确的是()

（选择一项）
- A) JAVA基本数据类型有int,char,String等
- B) 0_name是一个合法的变量名
- C) name与Name表示同一个变量
- D) 变量需要声明并赋值后,才能使用

【参考答案】：D

### 在java的以下的变量赋值语句中，无法通过编译的是()

（选择一项）
- A) char c1="男";
- B) char c2='女';
- C) int f1=128;
- D) double d1=1.2;

【参考答案】：A

## 3. 基本数据类型

### 下面Java代码的输出结果是()

```java
int num1,num2,num3;
num1 = (int)4.4;
num2 =(int)5.5;
num3 =(int)9.9;
System.out.println(num1);
System.out.println(num2);
System.out.println(num3);
```

（选择一项）
- A) 4
    5
    9
- B) 5
    6
    10
- C) 4
    5
    10
- D) 4
    6
    10

【参考答案】：A

### 运行如下Java代码并输入10，则输出结果为()

```java
import java.util.*;
public class Test{
	public static void main(String[]args){
		int a=20;
		Scanner input=new Scanner(System.in);
		double num=input.nextDouble();
		System.out.println(num+a);
	}
}
```

（选择一项）
- A) 10
- B) 10.0
- C) 30
- D) 30.0

【参考答案】：D

### 在JAVA中，有下面代码，其中可以正确编译的是()

（选择二项）
- A) float f = (int)2.5;
- B) int i =1.34;
- C) double i =1f;
- D) double b =(double)2;
    int c =b;

【参考答案】：AC

### 在JAVA中,有下面代码,其中可以正确编译的是()

（选择一项）
- A) double d=(int)2;
- B) int i =1.34;
- C) int i =(double)1;
- D) 以上都正确

【参考答案】：A

### 在JAVA中,下列说法错误的是()

（选择一项）
- A) boolean值可以表示真或者假
- B) boolean的值有两个:true、false
- C) boolean值必须与条件判断语句一起使用
- D) 1<2的boolean值是true

【参考答案】：C

### 在Java程序中，对于数据类型为boolean的变量x,可以采用的赋值方式是()

（选择一项）
- A) x=1;
- B) x==true;
- C) x=(3==3);
- D) x=(3=3);

【参考答案】：C

### 在Java中，以下()不属于Java基本数据类型

（选择一项）
- A) int
- B) double
- C) String
- D) boolean

【参考答案】：C

## 4. 运算符和表达式

### 在Java中，下面代码的运行结果是()

```java

System.out.print("我已经\n学习");
System.out.println("过了");
System.out.print("java\n的课程");

```

（选择一项）
- A) 我已经学习过了
    java的课程
- B) 我已经学习过了java的课程
- C) 我已经
    学习过了
    java
    的课程
- D) 我已经
    学习过了java
    的课程

【参考答案】：C

### 在Java中，以下程序的输出结果是()

```java
public class Test{
	public static void main(String[] args){
		//输出HelloWorld
		System.out.println("HelloWorld");
	}
}
```

（选择一项）
- A) //输出 HelloWorld
    HelloWorld
- B) HelloWorld
- C) 编译错误
- D) 运行错误

【参考答案】：B

### 在Java中，System.out.println("非常Happy!".toUpperCase());语句的输出结果是()

（选择一项）
- A) 非常HAPPY!
- B) 非常happy!
- C) 非常Happy!
- D) 转换时出现中文,会报错

【参考答案】：A

### Java中的语句 int num = 3+5*8%6-1;运行后，num变量的值是()

（选择一项）
- A) 8
- B) 6
- C) 12
- D) 3

【参考答案】：B

### 在Java中，以下程序的输出结果为()

```java
public class Test{
	public static void main(String[]args){
		boolean b1=false,b2=false;
		if((b1=2>3)&&(b2=5>0)){
			System.out.print("呵呵");
		}
		System.out.print("b1="+b1+";b2="+b2);
	}
}
```

（选择一项）
- A) b1=false;b2=true
- B) b1=true;b2=false
- C) b1=false;b2=false
- D) b1=true;b2=true

【参考答案】：C

### 在java中，下面选项输出结果为true的是()

（选择一项）
- A) System.out.println("CBD".equalsIgnoreCase("cbd"));
- B) String s1 = new String("CBD");
    String s2 = new String("CBD");
    System.out.println(s1==s2);
- C) System.out.println("CBD"=="cbd");
- D) System.out.println("CBD".equals("cbd"));

【参考答案】：A


### 以下Java程序会输出()

```java
double i=6;
int j=6;
boolean flag=i==j;
System.out.println(flag);
```

（选择一项）
- A) true
- B) false
- C) 假
- D) 真

【参考答案】：A

### 在Java中，以下赋值语句正确的是()
（选择二项）
- A) int num1=(int)"12";
- B) int num2=12.0;
- C) double num3=2d;
- D) double num4=5;

【参考答案】：CD

### 在Java中，有如下代码段，当i和j分别为()时，输出结果是"你好！"

```java
if((i>10&&j>50)||(j==20||i<5))
	System.out.println("Hello!");
else
	System.out.println("你好!");
```

（选择一项）
- A) i=-2 j=100
- B) i=20  j=20
- C) i=15   j=90
- D) i
    i=11   j=12

【参考答案】：D

### 在Java中，以下程序的运行结果是()

```java
public class Main{
	public static void main(String[]args){
		int sum=0;
		for(int i=1;i<=10;i++){
			sum+=i;
			if(i%2==0)break;
		}
	System.out.println(sum);
	}
}
```

（选择一项）
- A) 1
- B) 3
- C) 25
- D) 55

【参考答案】：B

### 在Java中，以下关于类与对象描述错误的是()

（选择二项）
- A) 类是用来表示客观世界中的一种事物
- B) 类是对象的一个实例
- C) 对象是类的一种抽象
- D) 类包含属性和方法

【参考答案】：BC

### 在Java操作符中，以下选项优先级最高的是()
（选择一项）
- A) *
- B) +
- C) ()
- D) ！

【参考答案】：C

### 在Java中，以下程序的运行结果是()

```java
public class Test{
	public static void main(String[]args){
		System.out.print("a"+"\n");
		System.out.print("b");
		System.out.println("c");
	}
}
```

（选择一项）
- A) a
    bc
- B) a   b
    c
- C) a
    b
    c

- D) a\nb
    c

【参考答案】：A

### 在Java中，以下代码的运行结果是()

```java
public class Test{
	public static void main(String[]args){
		String str="a,bc,def";
		System.out.println(str.substring(2,5));
	}
}
```

（选择一项）
- A) ,bc,
- B) bc,
- C) bc,de
- D) ,bc

【参考答案】：B

### 在Java中，以下代码的运行结果是()

```java
public class Test{
	public static void main(String[]args){
		char c=chang('B');
		System.out.println(c);
	}
	public static char chang(char c){
		if(c>='A'&&c<='Z'){
			c=(char)(c+32);
		}
		if(c>='a'&&c<='z'){
			c=(char)(c-32);
		}
		return c;
	}
}
```

（选择一项）
- A) B
- B) b
- C) C
- D) c

【参考答案】：A

### 在Java中，以下代码运行的结果是()

```java
public class Test{
	public static void main(String[]args){
		int s1=69;
		int s2=62;
		double sum=(double)((s1+s2)/2);
		System.out.println(sum);
	}
}
```

（选择一项）
- A) 65.5
- B) 66
- C) 65
- D) 65.0

【参考答案】：D

### 在Java中，以下程序的输出结果是()

```java
int num1=9;
double num2=1;
num1=num2;
System.out.println(num1);
```

（选择一项）
- A) 9
- B) 1
- C) false
- D) 编译错误

【参考答案】：D

### 在java中，以下代码的运行结果是()

```java
public static void main(String[] args){
	int age=22;
	double sum=5;
	age=age+sum;
	System.out.println(age);
}
```

（选择一项）
- A) 22
- B) 27.0
- C) 27
- D) 编译错误

【参考答案】：D

### 在java中，以下程序的执行结果是()

```java
public static void main(String[] args){
	String name="  abc de  ";
	System.out.println(name.trim()+"f");
}
```

（选择一项）
- A)   abc def
- B)      abc de   f
- C)   abcdef
- D)   abc de   f

【参考答案】：A

### 在java中，以下代码的运行结果是()

```java
public static void main(String[] args){
	boolean flag=true;
	while(flag){
		if(flag){
			System.out.println(flag);
			flag=false;
}else{
	System.out.println(flag);
			}
		}
	}
｝
```

（选择一项）
- A) true
- B) false
- C) true
    false
- D) 程序运行后出现死循环

【参考答案】：A

### 在java中，以下程序的输出结果是()

```java
public class Test{
	public static void main(String[] args){
	//输出HelloWorld
	System.out.println("HelloWorld");
	}
}
```

（选择一项）
- A) //输出HelloWorld
    HelloWorld
- B) HelloWorld
- C) 编译错误
- D) 运行错误

【参考答案】：B

### 在java中，以下程序的输出结果是()

```java
public static void main(String[] args){
	int age=22;
	String name="小婷";
	System.out.println("姓名="+name+"\n 年龄="+age);
}
```

（选择一项）
- A) 22 小婷
- B) 姓名=小婷年龄=22
- C) 姓名=小婷
    年龄=22
- D) 姓名=小婷\n年龄=22

【参考答案】：C

### 在java中，如下代码的输出结果是()

```java
public class Test{
	public static void main(String []args){
		int a =1,b =2;
		int c = a + b++;
		System.out.println(c++);
		System.out.println(c+a);
	}
}
```

- A) 3
    5
- B) 4
    5
- C) 3
    4
- D) 4
    4

【参考答案】：A

### 在java 中，运行下面的代码输出结果是()

```java
public class Test{
	public static void main(String args[]){
		int x=1;
		int y =2;
		x += y+y;
		if(x<5 && y<=4){
			System.out.println("1");
		}else if(x >=5 || y>4){
			System.out.println("2");
		}else
			System.out.println("error");
	}
}
```

（选择一项）
- A) 1
- B) 2
- C) error
- D) 运行时错误

【参考答案】：B

### 在java中，下面选项输出结果为true的是()
（选择一项）
- A) System.out.println("CBD".equalsIgnoreCase("cbd"));
- B) String s1=new String("CBD");
    String s2=new String("CBD");
    System.out.println(s1==s2);
- C) System.out.println("CBD"=="cbd"));
- D) System.out.println("CBD".equals("cbd"));

【参考答案】：A

### 在java中，System.out.println("非常happy！".toUpperCase());语句的输出结果是()。
（选择一项）
- A) 非常HAPPY!
- B) 非常happy!
- C) 非常Happy!
- D) 转换时出现中文,会报错

【参考答案】：A

### 在java中的语句int num = 3+5*8%6-1;运行后num变量的值是()
（选择一项）
- A) 8
- B) 6
- C) 12
- D) 3

【参考答案】：B

### 在java语言中，下面语句的运行结果是()

```java
StringBuffer strBuffer=new StringBuffer();
strBuffer.append("中央电视台");
strBuffer.append("+北京电视台");
System.out.println(strBuffer);
```

（选择一项）
- A) 中央电视台
- B) 中央电视台北京电视台
- C) +北京电视台
- D) 中央电视台+北京电视台

【参考答案】：D

### 在java程序中，boolean变量赋值正确的是()
（选择一项）
- A) boolean b = true&&false
- B) boolean b = 0|1
- C) boolean b = 2 = 2
- D) boolean b = 0

【参考答案】：A

### 在java程序中，如下代码输出的结果是()

```java
public class Test{
	public static void main(String[] args){
		int a = 1,b=2;
		int c = a+b++;
		System.out.println(c++);
		System.out.println(c+a);
	}
}
```

（选择一项）
- A) 3
    5
- B) 4
    5
- C) 3
    4
- D) 4
    4

【参考答案】：A

### 在java中,下列语句输出的结果是.()

```java
public class Test{
		public static void main(String[] args){
		System.out.print("hello");
		System.out.println("world");
	}
}
```

（选择一项）
- A) hello
    world
- B) hello    world
- C) helloworld
- D)    hello
    world

【参考答案】：C

### 在java中,下列语句输出的结果是()

```java
public class Test{
	public static void main(String[] args){
		int age = 21;
		String name = "Anne";
		System.out.println("name="name+",age="+age);
	}
}
```

（选择一项）
- A) name=Anne,age=21
- B) name=Anne,
    age=21
- C) 编译错误
- D) 运行错误

【参考答案】：C

### 在JAVA中，以下程序的输出结果为()

```java
public class Test{
	public static void main(String []args){
		boolean b1=false,b2=false;
		if((b1=2>3) && (b2=5>0)){
			System.out.print("(b1=2>3) && (b2=5>0)为真");
		}
		System.out.print("b1="+b1+";b2="+b2);
	}
}
```

（选择一项）
- A) b1=false;b2=true
- B) b1=true;b2=false
- C) b1=false;b2=false
- D) b1=true;b2=true

【参考答案】：C

### 在JAVA中,如下代码段的输出结果为()

```java
public class Test{
	public static void main(String []args){
		int x=3,y=4;
		x=(y<x++)?1:0;
		System.out.print(x);
	}
}
```

（选择一项）
- A) 0
- B) 1
- C) 3
- D) 4

【参考答案】：A

### 在JAVA中，如下代码的输出结果是()

```java
public static void main(String args[]){
	int a =3 , b = 4;
	int c = b++;
	System.out.println(c++);
	System.out.println(c+a);
}
```

（选择一项）
- A) 4
     8
- B) 5
     8
- C) 3
     8
- D) 3
     7

【参考答案】：A

### 在下列java代码中，a的值是()

```java
int a=4+2%(3*3)/2
```

（选择一项）
- A) 3
- B) 7
- C) 1
- D) 5

【参考答案】：D

### 在JAVA中，为boolean类型赋值正确的是()

（选择二项）
- A) boolean b=15=15;
- B) boolean b =true && false;
- C) boolean b =24/6;
- D) boolean b = 20>5;

【参考答案】：BD

### 在JAVA中,如下代码的输出结果是()

```java

public static void main(String args[]){
	int i=0;
	if(++i <1)
	System.out.println("i小于1");
}
```

（选择一项）
- A) i小于1
- B) 无输出
- C) 编译错误
- D) 运行时错误

【参考答案】：B

### 在java中，如下代码的输出结果是()

```java
public static void main(String []args){
     int i=0;
     System.out.println(i+=2);
     System.out.println(i+=2);
}
```

（选择一项）
- A) 2
    4
- B) 2
    2
- C) 0
    4
- D) 0
    2

【参考答案】：A

### 在Java中，下列代码运行后的输出结果是()

```java

int x=5*3;
int y=x+5/x+3;
System.out.println(y) ;
```

（选择一项）
- A) 18
- B) 21
- C) 19
- D) 4

【参考答案】：A

### 给定如下Java代码，编译运行后，将输出()

```java

public class Test{
	public static void main(String[] args){
		int x=0;
		int y=x++;
		System.out.println(++y);
	}
}
```

（选择一项）
- A) 0
- B) 1
- C) 2
- D) 3

【参考答案】：B

## 5. 分支结构

### 下面Java代码中，condition1和condition2取何值时，会输出"货到付款"四个字()

```java

boolean condition 1 = _____,condition2 = _____;
if(condition1)
    if(condition2)
         System.out.println("在线支付");
    else
     System.out.println("货到付款");

```

（选择一项）
- A) true  true
- B) true false
- C) false  true
- D) false  false

【参考答案】：B

### 在Java中，关于main()方法和类文件的说法错误的是()

（选择一项）
- A) main()方法是Java程序的入口
- B) 在Java中必须用class关键字声明类
- C) Java源文件的扩展名为.java
- D) 在Java类文件中必须要有main()方法

【参考答案】：D

### 在Java中，运行下面的代码,输出结果是()

```java

public class Test{
    public static void main(String[] args){
        int a[ ] = {1,2,3};
        int b[ ];
        b = a;
        for(int i = 0;i<b.length;i++){
             System.out.print(b[i]+",");
        }
    }
}

```

（选择一项）
- A) 1,
- B) 1
- C) 1,2,3,
- D) 编译错误


【参考答案】：C

### 阅读下列Java代码片段，数据结果正确的是()

```java

String str = "select  name,sex from user";
int num1 = str.indexOf("e");
int num2 = str.lastIndexOf("e");
System.out.println(num1+num2);

```

（选择一项）
- A) 22
- B) 23
- C) 24
- D) 26

【参考答案】：C

### 执行下列Java代码片段，输出结果正确的是()

```java

int num = 5;
switch(num){
    default:
	System.out.println("天蝎座");
    case 1:
	System.out.println("巨蟹座");
	break;
   case 2:
	System.out.println("射手座");
	break;
    case 3:
    case 4:
	System.out.println("双子座");
}

```

（选择一项）
- A) 天蝎座
- B) 天蝎座
    巨蟹座
- C) 编译错误
- D) 变异正确，什么也不输出

【参考答案】：B

### 在Java中，下列可以用作条件表达式的是()

（选择一项）
- A) 15+45>32
- B) 15+45||32
- C) x+y=32
- D) (45>15)&&32

【参考答案】：A

### 在Java中，如下代码的输出结果为()

```java
public static void main(String[] args){
    int num1 = " my Java ".trim().length();
    int num2 = " ".trim().length();
    int num3 = "Java".trim().length();
    System.out.println(num1+num2+num3);
```

（选择一项）
- A) 10
- B) 11
- C) 12
- D) 编译错误

【参考答案】：B

### 在Java中，为boolean类型变量赋值正确的是()
（选择一项）
    - A) boolean b = true&&false;
    - B) boolean b = 0|1;
    - C) boolean b = 2 = 2;
    - D) boolean b=0;

【参考答案】：A

### 如下的Java代码输出的结果是()

```java
public static void main(String[] args){
    for(int i = 1;i<5;i++){
        if(i>2){
            System.out.print(i);
            ++i;
            break;
        }
        System.out.print(i);
    }
}
```

（选择一项）
- A) 1234
- B) 123
- C) 14
- D) 1

【参考答案】：B

### 如下的Java代码输出的结果是()

```java
public static void main(String[] args){
    for(int i = 1;i<4;i++){
        for(int j = 1;j<i;j++){
             System.out.print(i);
        }
    }
}
```

（选择一项）
- A) 12233
- B) 233
- C) 23344
- D) 223344

【参考答案】：B

### 下面Java代码中，condition1和condition2取何值时,会输出"货到付款"四个字()

```java
boolean condition1=_____,condition2=______;
if(condition1)
if(condition2)
	System.out.println("在线支付");
else
	System.out.println("货到付款");
```

（选择一项）
- A) true  true
- B) true  false
- C) false  true
- D) false  false

【参考答案】：

### 如下的Java代码输出的结果是()

```java
public static void main(String[] args){
	int i = 4;
	do{
	    i++;
	    System.out.print(i);
	}while(i<7);
}
```

（选择一项）
- A) 567
- B) 456
- C) 67
- D) 4567

【参考答案】：A

### 下面Java代码的输出结果正确的是()

```java
int age = 21;
switch(age/10){
case 1:
	System.out.println("十岁不愁");
case 2:
	System.out.println("二十不悔");
case 3:
	System.out.println("三十而立");
}
```

（选择一项）
- A) 十岁不愁
- B) 二十不悔
- C) 十岁不愁
    二十不悔
    三十而立
    三十而立
- D) 二十不悔
    三十而立

【参考答案】：D

### 在java中，以下程序的输出结果是()

```java
public static void main(String[] args){
	switch(2){
		case2:
		System.out.println(2);
		break;
		case3:
		System.out.println(3);
		break;
		default:
		System.out.println("default");
	}
}
```

（选择一项）
- A) 2
    default
- B) 2
    3
    defult
- C) 2
    3
- D) 2

【参考答案】：D

### 在JAVA中，运行如下代码，则输出结果为()

```java
public static void main(String []args){
	float i =2;
	switch(i){
		case 1:
			System.out.println("i=1");
		case 2:
			System.out.println("i=2");
		case 3:
			System.out.println("i=3");
			break;
		default:
			System.out.println("i不符合判断条件");
	}
}
```

（选择一项）
- A) i=2
- B) i=2
    i=3
- C) i=2
    i=3
    i不符合判断条件
- D) 编译错误

【参考答案】：D

### 在JAVA中,以下代码段中造成编译异常的原因是()

```java
private void onLoading(){
	if(......){
		代码1
	}else{
		代码2
	}
		代码3
		return;
		代码4
}
```

（选择一项）
- A) 代码1
- B) 代码2
- C) 代码3
- D) 代码4

【参考答案】：D

### 在java语言中有如下代码，则输出结果为()

```java
public class Test{
	public static void main(String[] args){
		int i = 0;
		while(i>10){
			System.out.println("我是java程序员");
		}
	}
}
```

（选择一项）
- A) 打印10遍"我是java程序员"
- B) 什么也没输出
- C) 编译错误
- D) 死循环

【参考答案】：B

### 在java中,运行如下代码,则输出结果为()

```java
public class Test{
	public static void main(String[] args){
		for(int i=0;i<=5;i++){
			System.out.println(i+" ");
			if(i==3)
				continue;
		}
	}
}
```

（选择一项）
- A) 0 1 2
- B) 0 1 2 3
- C) 0 1 2 3 4
- D) 0 1 2 3 4 5

【参考答案】：D

### 在java中，运行下面的代码输出的结果是()

```java
public class Test{
	public static void main(String[] args){
		int x = 1;
		int y = 2;
		x += y+y;
		if(x < 5 && y <=4){
			System.out.println("1");
		}else if(x >= 5 || y>4){
			System.out.println("2");
		}else{
			System.out.println("error");
		}
	}
}
```

（选择一项）
- A) 1
- B) 2
- C) error
- D) 运行时错误

【参考答案】：B

### 在JAVA中,如下代码的输出结果是()

```java
public class Test{
	public static void main(String args[]){
		int score =78;
		if(score <60)System.out.print("没及格!");
		if(score <70)System.out.print("及格了!");
		if(score <80)System.out.print("良好！");
		if(score<90)System.out.print("优秀!");
	}
}
```

（选择一项）
- A) 良好！
- B) 良好！优秀！
- C) 没及格！
- D) 及格了！

【参考答案】：B

### 在JAVA中,关于switch语句说法正确的是()

（选择一项）
- A) 可以有多个case，每个case后面的常量值可以相同
- B) 每个case语句后面必须要有break,否则有编译错误
- C) default语句必须放在所有case语句块的后面
- D) switch语句中的default相当于多重if最后面的else

【参考答案】：D

### 在JAVA中，运行下面代码后输入15，则输出结果是()

```java

import java.util.Scanner;
public class Test{
	public static void main(String []args){
		Scanner input = new Scanner(System.out);
		int age = input.nextInt();
		if(age >18)
			System.out.println("恭喜你成年了!");
		else
			System.out.println("你还得使劲长！");
	}
}
```

（选择一项）
- A) 恭喜你成年了!
- B) 你还得使劲长！
- C) 什么都不输出
- D) 编译时出错

【参考答案】：D

### 在java中，如下代码段的输出()

```java

public static void main(String []args){
	int a=19,b=15,c=11,sum;
	if(!(a>19))
		sum =a+b;
	else if(b >18 || c <18)
		sum =b+c;
	else
		sum =a+b+c;
	System.out.println(sum);
}
```

（选择一项）
- A) 26
- B) 40
- C) 34
- D) 45

【参考答案】：C

### 在java中，关于switch选择结构说法错误的是()

（选择一项）
- A) switch选择结构中的default语句相当于多重if的最后一个else
- B) switch选择结构可以包含多个case语句
- C) switch选择结构的每个case语句必须要有break，否则会编译错误
- D) switch选择结构后面的小括号里的数据类型可以是int,char

【参考答案】：C

### 在java中，有如下代码段，当i 和j分别为()时，输出结果是"条件符合"

```java

if((i>30 && i<40) || (i==60 && j>60))
	System.out.println("条件符合");
else
	System.out.println("条件不符合");
```

（选择一项）
- A) i=35,j=40
- B) i =40,j=70
- C) i =60,j=60
- D) i=20,j=30

【参考答案】：A

### 在JAVA中，使用switch语句时，如希望设置默认值，则需要使用()关键字

（选择一项）
- A) case
- B) do
- C) default
- D) else

【参考答案】：C

### 分析如下Java程序段，程序编译运行结果是()

```java
public class A{
    public static void main(String[] args) {
        int num = 0;
        switch (num) {
        default:
            System.out.println("Default");
            num++;
        case 1:
            System.out.println("num = " +num);
            num += 2;
        case 2:
            System.out.println("num = " + ++num);
            break;
        case 3:
            System.out.println("num = " +num);
            break;
        }
    }
}
```

（选择一项）
- A) 输出：Default
    num = 1
    num = 3
- B) 输出：Default
- C)  输出：Default
    num = 1
    num = 4
- D) 程序编译出错

【参考答案】：C

### 在Java中，以下程序编译运行后的输出结果为()

```java
public static void main(String[] args) {
    int a = 5;
    int s = 0;
    switch (a) {
    case 5:
        s = s + 2;
    case 3:
        s = s + 5;
    case 8:
        s = s + 6;
    default:
        s = s + 10;
        break;
    }
    System.out.print(s);
}
```

（选择一项）
- A) 2
- B) 0
- C) 7
- D) 23

【参考答案】：D

### 在Java语言中有如下代码，下列x的定义中，可以使该段代码输出100的是()

```java
switch( x ) {
    case 100 :
        System.out.println("100");
        break ;
    case 110 :
        System.out.println("110");
        break ;
}
```

（选择一项）
- A) int x = 100;
- B) double x = 100;
- C) String x = "100";
- D) int x = 110;

【参考答案】：A

### 在Java语言中，有如下代码：

```java
switch(x) {
    case 100 :
        System.out.println("One hundred");
        break;
    case 200 ：
        System.out.println("Two hundred");
        break;
    case 300 :
        System.out.println( "Three hundred");
        break;
}
```

下列x的定义中，()可以使得上段代码编译通过。

（选择二项）
- A) double x = 100;
- B) char x = 100;
- C) String x = "100";
- D) int x = 100;

【参考答案】：BD

### 给定如下Java代码片段，编译运行时的结果是()

```java
int i = 2;
switch (i) {
	default:
    	System.out.println("default");
	case 0:
    	System.out.println("zero");
    	break;
	case 1:
    	System.out.println("one");
	case 2:
    	System.out.println("two");
}
```

（选择一项）
- A)  输出：default
- B) 输出：default
           zero
- C) 编译出错，default语句的位置不符合switch 结构的语法结构
- D)  输出：two

【参考答案】：D

### 分析下面的Java代码，当x=2时，运行结果是()

```java
switch (x) {
 case 1:
    System.out.println(1);
 case 2:
 case 3:
    System.out.println(3);
 case 4:
    System.out.println(4);
}
```

（选择一项）
- A) 没有输出任何结果
- B) 输出结果为3
- C) 输出结果是3和4
- D)  输出结果是1、3和4

【参考答案】：C

### 给定如下Java代码片段，编译运行的结果是()

```java
int i = 0, j = -1;
switch (i) {
    case 0, 1: j = 1;
    case 2: j = 2;
}
System.out.print("j=" + j);
```

（选择一项）
- A) 程序编译出错
- B) 输出：j=1
- C) 输出：j=2
- D) 输出：j=0

【参考答案】：A

### 给定某Java程序的main方法如下所示，该程序的运行结果是()

```java
public static void main(String[] args) {
    boolean boo = true;
    if (boo == false) {
        System.out.println("a");
    } else {
        System.out.println("b");
    }
}
```

（选择一项）
- A) 输出a
- B) 输出b
- C) 输出ab
- D) 运行时出错

【参考答案】：B

### 在Java中，下列代码的运行结果是()

```java
public static void main(String[] args) {
    int a=1,b=2,c=3;
    if(a<0)
        if(b<0)
            c=10;
        else
            c=20;
    System.out.println(c);
}
```

（选择一项）
- A) 输出：10
- B) 输出：20
- C) 输出：3
- D) 编译报错

【参考答案】：C

## 6. 循环结构

### 阅读下列Java代码，有()行出现编译错误。

```java

public class Test{
    public static void main(String[ ] args){
	Test2 test = new Test2();
	test.getId();
    }
}
class Test2{
    int id;
    String str;
    private void getId(){
	int num;
	System.out.println(id/num);
    }
}
```

（选择一项）
- A) 0
- B) 1
- C) 2
- D) 3

【参考答案】：B

### 阅读下列Java代码片段,输出结果是( )。

```java

for(int i = 0;i<5;i++){
    while(i%2 == 0){
	continue;
    }
    System.out.println(i);
}

```

（选择一项）
- A) 0
    2
    4
- B) 1
    3
- C) 编译错误
- D) 死循环,什么也不输出

【参考答案】：D

### 阅读下列Java代码片段，横线处填写( )输出结果为 "正确"。

```java

if(______)
    System.out.println("正确");
else
    System.out.println("错误");

```

（选择二项）
- A) 25>23&&15>5
- B) !(25>23||15>5)
- C) 25>23==true
- D) 20>5&&17>7&&5>13

【参考答案】：AC

### 阅读下列Java代码，运行后正确的输出结果是( )。

```java

public class Test {
	public String[] updateStr(String[] oldStr,int len){
		String[] newStr = new String[oldStr.length];
		for(int i = 0;i<oldStr.length;i++){
			if(oldStr[i].length()<len)
				newStr[i] = oldStr[i];
		}
		return newStr;
	}
	public static void main(String[] args) {
		Test test = new Test();
		String[] oldStr = {"select","from","top","update"};
		String[] newStr = test.updateStr(oldStr, 4);
		for(int i = 0;i<newStr.length;i++){
			System.out.println(newStr[i]+" ");
		}
	}
}

```

（选择一项）
- A) from top null null
- B) null from top null
- C) 编译错误
- D) 运行错误

【参考答案】：B

### 在Java中，以下程序的输出结果是( )

```java

public class Test{
	public static void main(String[] args){
		//输出HelloWorld
		System.out.println("HelloWorld");
	}
}

```

（选择一项）
- A) //输出HelloWorld
    HelloWorld
- B) HelloWorld
- C) 编译错误
- D) 运行错误

【参考答案】：B

### 在Java中，下面代码的运行结果是( )

```java

System.out.print("我已经\n学习");
System.out.println("过了");
System.out.print("java\n的课程");

```

（选择一项）
- A) 我已经学习过了
    java的课程
- B) 我已经学习过了java的课程
- C) 我已经
    学习过了
    java
    的课程
- D) 我已经
    学习过了 java
    的课程

【参考答案】：C

### 在Java中，以下程序的执行结果是( )

```java

public static void main(String[] args){
	String name="  abc de  ";
	System.out.println(name.trim()+"f");
}

```

（选择一项）
- A) abc def
- B) abc de   f
- C) abcdef
- D) abc de  f

【参考答案】：A

### 在Java中，对以下程序分析错误的是( )。

```java

public class HelloWorld{
	public static void main(String args[]){
		System.out.println("HelloWorld");
	}
}

```

（选择一项）
- A) 该类文件的文件名称必须为HelloWorld.java
- B) public 修饰的类其文件名必须与类名一致
- C) 程序在输出HelloWorld后回自动换行
- D) Java程序中一行只能写一条语句

【参考答案】：D

### 如下的Java代码输出的结果是( )

```java
public static void main(String[] args){
	int a = 2;
	while(a<2){
		System.out.println("执行while!");
	}
	do{
		System.out.println("执行do-while!");
	}while(a<2);
}

```

（选择一项）
- A) 执行while!
- B) 执行do-while!
- C) 执行while!
    执行do-while!
- D) 编译错误

【参考答案】：C

### 如下的Java代码输出的结果是( )

```java
public static void main(String[] args){
	for(int i = 0;i<5;i++){
		if(i>2){
			System.out.print(i);
			break;
		}
		continue;
		System.out.print(i);
	}
}

```

（选择一项）
- A) 34
- B) 4
- C) 3
- D) 编译错误

【参考答案】：D

### 在Java中，以下程序的运行结果是()

```java
public class Main{
	public static void main(String[]args){
		int i=0,num=0;
		do{
			--num;
			i=i-1;
		}while(i>0);
		System.out.println(num);
	}
}
```

（选择一项）
- A) 0
- B) 1
- C) -1
- D) 死循环

【参考答案】：C

### 以下Java代码的输出结果为()

```java
int num=1;
while(num<10){
	System.out.print(num);
	if(num>5)
		break;
	num+=2;
}
```

（选择一项）
- A) 1357
- B) 13579
- C) 135
- D) 2468

【参考答案】：A

### 阅读下列Java代码片段，横线处填写()输出结果为true。

```java
if(_________________)
	System.out.println(true);
else
	System.out.println(false);
```

（选择二项）
- A) !(25>23&&30>5)
- B) !(25>23||30>5)
- C) 25>23||30>5
- D) 20>5&&20>7||5>10

【参考答案】：CD

### 阅读下列Java代码片段，如果从控制台上先输入"n"，再输入"Y"，则在控制台上输出共朗诵的次数是()

```java
public static void main(String[]args){
	String answer=null;
	int num=0;
	Scanner input=new Scanner(System.in);
	do{
		System.out.println("高声朗诵诗歌《海燕》...");
		System.out.println("声音够大码？");
		answer=input.next();
		num++;
	}while(!answer.equalsIgnoreCase("y"));
	System.out.println("共朗诵的次数是"+num);
}
```

（选择一项）
- A) 1
- B) 2
- C) 3
- D) 编译错误

【参考答案】：B

### 阅读下列Java代码，运行后正确的输出结果是()

```Java
public class Test{
	public int coutStr(String[]arry,String str){
		int count=0;
		for(int i=0;i<arry.length;i++){
			if(arry[i].indexOf(str)!=-1)
				count++;
		}
		return count;
	}
	public static void main(String[]args){
		String[]arry={"happy","apple","money","ending"};
		String findStr="a";
		Test test=new Test();
		int count=test.coutStr(arry,findStr);
		System.out.println("此数组中有"+count+"个字符串包含字符"+findStr);
	}
}
```

（选择一项）
- A) 此数组中有1个字符串包含字符a
- B) 此数组中有2个字符串包含字符a
- C) 编译错误
- D) 正常编译,运行时报错

【参考答案】：B

### 在Java中，如下代码的输出结果是()

```java
public static void main(String[]args){
	char c[]={'1','2','3','4'};
	for(int i=0;i<c.length/2;i++){
		char d=c[i];
		c[i]=c[c.length-(i+1)];
		c[c.length-(i+1)]=d;
	}
	for(int i=0;i<c.length;i++){
		System.out.print(c[i]);
	}
}
```

（选择一项）
- A) 1234
- B) 3412
- C) 1324
- D) 4321

【参考答案】：D

### 如下Java代码在控制台输出的结果是()

```java
public static void main(String[]args){
	for(int i=1;i<=2;i++){
		i--;
		System.out.print(i);
		i++;
		System.out.print(i);
	}
}
```

（选择一项）
- A) 01
- B) 0112
- C) 无任何输出结果
- D) 编译错误

【参考答案】：B

### 在Java中，下列代码的输出结果为()

```java
int num=1;
while(num++<=5){
	System.out.print(num+" ");
}
```

（选择一项）
- A) 2 3 4 5 6
- B) 2 3 4 5
- C) 1 2 3 4 5
- D) 1 2 3 4 5 6

【参考答案】：A

### 在Java中运行下面代码输出的结果是().

```java
int[]num=new int[]{5,8,13,20};
for(int i=0;i<num.length;i++){
	if(num[i]%2==0)
		num[i]+=1;
	System.out.print(num[i]+" ");
}
```

（选择一项）
- A) 5 9 13 21
- B) 5 8 13 20
- C) 6 9 14 21
- D) 6 8 14 20

【参考答案】：A

### 在Java中，以下程序的输出结果为()

```java
int num=9;
if(num++>=9){
	System.out.println(++num);
}
else{
	System.out.println("num++的结果不满足大于等于9");
}
```

（选择一项）
- A) 9
- B) 10
- C) 11
- D) num++的结果不满足大于等于9

【参考答案】：C

### 在java中，如下代码的输出结果是()

```java
public class Test{
	public static void main(Sting args[]){
		int a=0;
		while(a<10){
			if(a>=0){
				System.out.print(a++);
			}
		}
	}
}
```

（选择一项）
- A) 1234567890
- B) 0123456789
- C) 死循环
- D) 0

【参考答案】：B

### 在Java中，以下代码段的输出结果为()

```java
public static void main(String[]args){
	int nums=new int[3];
	for(int i=1;i<nums.length;i++){
		nums[i]=i*2;
	}
	for(int i=0;i<nums.length;i++){
		System.out.print(nums[i]);
	}
}
```

（选择一项）
- A) 024
- B) 246
- C) null24
- D) 编译错误

【参考答案】：D

### 在Java中，下面代码输出结果为()

```java
int num=2;
do{
	System.out.println(num);
	num++;
}while(num%2==0);
```

（选择一项）
- A) 2
- B) 2
    2
- C) 2
    3
- D) 编译错误

【参考答案】：A

### 下面java代码输出的结果是()

```java
int sum = 0;
for(int i=20;i>0;i--){
	if(i%3!=0){
		continue;
	}
	sum += i;
}
System.out.println(sum);
```

（选择一项）
- A) 0
- B) 63
- C) 147
- D) 630

【参考答案】：B

### 下面java代码输出的结果是()

```java
int t;
int[] arr = {1,2,3,4,5,6};
for(int i=0;i<arr.length/2;i++){
	t = arr[i];
	arr[i] = arr[arr.length-i-1];
	arr[arr.length-i-1] = t;
}
for(int i=0;i<arr.length;i++){
	System.out.print(arr[i]);
}
```

（选择一项）
- A) 0 1 2 3 4 5
- B) 5 4 3 2 1 0
- C) 1 2 3 4 5 6
- D) 6 5 4 3 2 1

【参考答案】：D

### 下面java代码的输出结果是()

```java
int sum = 0;
for(int i=1;i<30;i++){
	if(i%3==0){
		sum += i;
	}else if(sum > 30){
		break;
	}
}
System.out.println(sum);
```

（选择一项）
- A) 120
- B) 48
- C) 30
- D) 45

【参考答案】：D

### 在JAVA中,运行下面的代码,输出结果是()

```java
public class Test{
	public static void main(String []args){
		int a[]={1,2,3};
		int b[];
		b=a;
		for(int i=0;i<b.length;i++){
			System.out.print(b[i]+",");
		}
	}
}
```

（选择一项）
- A) 1,
- B) 1
- C) 1,2,3,
- D) 编译错误

【参考答案】：C

### 在java中，以下循环的执行次数是()

```java
public class Test{
	public static void main(String args[]){
		for(int i=0,j=1;i<j+1;i+=2,j--){
			System.out.print(i);
		}
	}
}
```

（选择一项）
- A) 0
- B) 1
- C) 2
- D) 3

【参考答案】：B

### 在JAVA中,若要使下面程序的输出值为2,则应该从键盘给n输入的值是()

```java
public class Test{
	public static void main(String args[]){
		int s=0,a=1;
		Scanner input =new Scanner(System.in);
		int n=input.nextInt();
		do{
			s = s+1;
			a = a-2;
		}while(a!=n);
		System.out.print(s);
	}
}
```

（选择一项）
- A) -1
- B) -3
- C) -5
- D) 0

【参考答案】：B

### 在java中，如下代码的输出结果是()

```java
public class Test{
	public static void main(String []args){
		int i =1;
		while(i++ <=10)
			System.out.print(i+",");
	}
}
```

（选择一项）
- A) 1,2,3,4,5,6,7,8,9,10
- B) 2,3,4,5,6,7,8,9,10,11
- C) 11,
- D) 12,

【参考答案】：B


### 在JAVA中,运行如下代码,则输出结果为()

```java
public static void main(String []args){
	int i;
	for(i=1;i<=5;i++){
		if(i%2!=0){
			i++;
			continue;
		}
	}
	System.out.println(i);
}
```

（选择一项）
- A) 9
- B) 7
- C) 5
- D) 3

【参考答案】：B

### 在java中，运行如下代码，则输出结果为()

```java
public static void main(String []args){
	int i =1;
	switch(++i){
	case 1:
		System.out.println("i=1");
	case 2:
		System.out.println("i=2");
	case 3:
		System.out.println("i=3");
		break;
	default:
		System.out.println("i不符合判断条件");
	}
}
```

（选择一项）
- A) i=2
- B) i=1
    i=2
    i=3
    i不符合判断条件
- C) i=2
    i=3
- D) i=2
    i=3
    i不符合判断条件

【参考答案】：C

### 在JAVA中，运行如下代码，则输出结果为()

```java

public static void main(String []args){
    for(int i =0;i<=5;i++){
	System.out.println(i+"");
	if(i==3)
		break;
      }
}
```

（选择一项）
- A) 0 1 2
- B) 0 1 2 3
- C) 0 1 2 3 4
- D) 0 1 2 3 4 5

【参考答案】：B

### 在JAVA中，运行如下代码，则输出结果为()

```java
public static void main(String[]args){
	int i;
	for(i =1;i<=5;i++){
		if(i%2!=0){
			i++;
			continue;
		}
	}
	System.out.println(i);
}
```

（选择一项）
- A) 9
- B) 7
- C) 5
- D) 3

【参考答案】：B

### 在JAVA中，如下代码段的输出结果为()

```java
public class Test{
	public static void main(String []args){
		int i =4;
		switch(i--){
			default:
				System.out.println("i不符合判断条件");
			case 1:
				Systm.out.println("i=1");
			case 2:
				System.out.println("i=2");
				break;
			case 3:
				System.out.println("i=3");
		}
	}
}
```

（选择一项）
- A) i不符合判断条件
- B) i=3
- C) i不符合判断条件
    i=1
    i=2
- D) i不符合判断条件
    i=1
    i=2
    i=3

【参考答案】：C

### 在java中，如下代码段的输出结果为()

```java

public static void main(String []args){
	int []nums = new int [5];
	for(int i=0; i<4; i++){
		nums[i]=i;
	}
	System.out.println(nums[4]);
}
```

（选择一项）
- A) 3
- B) 0
- C) 编译时错误
- D) 运行时错误

【参考答案】：B

### 在JAVA中，如下代码的输出结果为()

```java

public static void main(String args[]){
	int i =3;
	do{
		for(int j =3;j<6;++j){
			System.out.println(j);
		}
		i--;
	}while(i >3);
}
```

（选择一项）
- A) 3
     4
     5
- B) 4
     5
- C) 3
- D) 什么都不输出

【参考答案】：A

### 在JAVA中,如下代码段的输出结果为()

```java
public static void main(String []args){
	int num =1;
	while(num <6){
		System.out.print(num);
		if(num /2==0)
			continue;
		else
			num++;
	}
}
```

（选择一项）
- A) 12345
- B) 135
- C) 24
- D) 死循环

【参考答案】：D

### 在JAVA中，如下代码段的输出结果为()

```java

public static void main(String args[]){
	int i=3;
	do{
		for(int j =3; j<4; ++j){
			System.out.println("j is "+j);
		}
		i++;
	}while(i>3 && i<5);
}
```

（选择一项）
- A) j is 3
    j is 4
    j is 3
    j is 4
- B) j is 3
    j is 3
- C) 什么都不输出
- D) j is 3

【参考答案】：B

### 在java中，如下代码段的输出结果为()

```java
public static void main(String args[]){
	int i =5;
	String []s={"a","b","c"};
	for(int i=0;i<10;i++)
		System.out.println(s[i]);
}
```

（选择一项）
- A) abc
- B) abcnullnullnullnullnullnullnull
- C) 编译时错误
- D) 运行时错误

【参考答案】：C

### 在java中，如下代码输出结果是()

```java
public static void main（String[] args）{
	int i;
	for(i=1;i<=10;i++){
		if(i%2!=0){
			i+=2;
			continue;
		}else{
			i++;
			break;
		}
	}System.out.println(i);
}
```

（选择一项）
- A) 9
- B) 4
- C) 5
- D) 1

【参考答案】：C

### 在java中,如下代码输出结果是()

```java
public static void main(String[]args){
	int i=1;
	do{
		i++;
	}while(i<1);
	while(i<1){
		i++;
	}
	System.out.println(i);
}
```

（选择一项）
- A) 1
- B) 2
- C) 3
- D) 4

【参考答案】：B

### 分析以下使用for循环的java代码，其最后的运行结果是()

```java
publci class HelloTEDU{
	publci static void main(String []args){
		int i=2;
		for(;i<5;i++){
			System.out.print("我是第"+(i-1)+"名");
		}
	}
}
```

（选择一项）
- A) 存在错误，缺少for循环的第一个表达式
- B) 我是第1名！我是第2名！
- C) 我是第1名！我是第2名！我是第3名！
- D) 我是第2名！我是第3名！我是第4名！

【参考答案】：C

### 在Java中，给定代码片段如下所示，则编译运行后，输出结果是()

```java
for(int i = 0;i < 10;i++){
   if(i==10-i){
	break;
   }
   if(i%3 != 0){
	continue;
   }
   System.out.println(i + " ");
}
```

（选择一项）
- A) 0
- B) 0   3
- C) 0   3   6
- D) 0   3   6   9

【参考答案】：B

### 在JAVA中,无论循环条件是什么，那个循环都将至少执行一次()

（选择一项）
- A) for
- B) do...while
- C) while
- D) while...do

【参考答案】：B

### 分析下面的Java代码片段，编译运行后的输出结果是()

```java
for (int i = 0; i < 6; i++) {
    int k = ++i;
    while(k <5) {
        System.out.print(i);
        break;
    }
}
```

（选择一项）
- A) 024
- B) 02
- C) 123
- D) 13

【参考答案】：D

### 编译并运行下面的Java代码，()会出现在输出结果中

```java
public class Test{
     public static void main(String args[])  {
         for(int i = 0; i < 3; i++)  {
           for(int j = 3; j >= 0; j--)  {
              if(i == j) 
                 continue;
           System.out.println("i="+ i + "  j="+j);
       }
      }
     }
}
```

（选择二项）
- A) i=0 j=3
- B) i=0 j=0
- C) i=2 j=2
- D) i=0 j=2
- E) i=1 j=1

【参考答案】：AD

### （单选题）在Java中，用来退出循环，将控制权转给程序的其他部分的关键字是什么()

- A) return
- B) continue
- C) break
- D) exit

【参考答案】：C

### 下面一段代码中break语句起到的作用是()

```java
int pointer = 0;
while (pointer <= 10) {
    switch (pointer % 3) {
    case 1:
        pointer += 1;
        break;
    case 2:
        pointer += 2;
        break;
    default:
        pointer += 3;
        break;
    }
}
```

（选择一项）
- A)  结束当次循环，使控制权直接转移到控制循环的条件表达式
- B) 从嵌套循环内部跳出最里面的循环
- C) 终止switch语句的语句序列，提高switch-case语句性能
- D) 退出循环

【参考答案】：C

## 7. 数组

### (单选题)定义一个数组

```java
 int[ ] scores = { 1, 2, 3, 4, 5, 6, 7 } ;
 ```

 数组中的 score[3] 指的是
- A)1
- B)2
- C)3
- D)4

### (单选题)下列数组的初始化正确的是()

- A)int[ ] score = new int[ ];
- B)int score[ ] = new int[ ] { 34, 90, 87, 54, 24 };
- C)int[ ] score = new int[ 4 ] { 90, 12, 34, 77 };
- D)int score = { 78, 23, 44, 78 }；

### (单选题)执行完以下代码

```java
 int [] x = new	int[25];
```

### 以下哪项说明是正确的()

- A)x[24]为 0
- B)x[24]未定义
- C)x[25]为 0
- D)x[0]为空

### (单选题)下面哪个是正确的()

- A)String temp [] = new String {"a""b""c"};
- B)String temp [] = {"a""b""c"};
- C)String temp = {"a", "b", "c"};
- D)String temp [] = {"a", "b", "c"};

### (单选题)Given：

```java
public class Test {
private static float[] f = new float[2];
public static void main(String args[]) {
System.out.println("f[0] = " + f[0]);
		}
}
```

What is the result()
- A)f[0] = 0
- B)f[0] = 0.0
- C)Compilation fails.
- D)An exception is thrown at runtime.

### (单选题)当编译并运行下面程序时会出现什么结果( )

```java
public class MyAr{
public static void main(String argv[]){
int[] i = new int[5];
System.out.println(i[5]);
	}
}
```

- A)编译错误
- B)运行错误
- C)输出0
- D)输出“null”

【参考答案】：B

### (单选题)已知表达式

```java
 int m [ ] = {0, 1, 2, 3, 4, 5, 6};
```

下面哪个表达式的值与数组下标量总数相等()
- A)m.length()
- B)m.length
- C)m.length()+1
- D)m.length+1

【参考答案】：B

### (单选题)对记录序列{314，298，508，123，486，145}按从小到大的顺序进行插入排

序，经过两趟排序后的结果为()
- A){314，298，508，123，145，486}
- B){298，314，508，123，486，145}
- C){298，123，314，508，486，145}
- D){123、298，314，508，486，145}

【参考答案】：B

### (多选题)下列说法错误的有()

- A)数组是一种对象
- B)数组属于一种原生类
- C)int number=[]={31,23,33,43,35,63}
- D)数组的大小可以任意改变

【参考答案】：BCD

### (多选题)下列选项中创建数组能够编译通过的是()

- A)int[] ia = new int [15];
- B)float fa = new float [20];
- C)char[] ca = "Some String";
- D)Object oa = new float[20];

【参考答案】：AD

### 在Java中，以下说法正确的是()。

（选择一项）
- A) 定义了数组的长度后,我们可以随时去修改这个长度
- B) 对象可作方法参数,对象数组不能作方法参数
- C) 数组是相同类型的数据元素按顺序组成的一种新结构数据类型
- D) 以上说法都不正确

【参考答案】：C

### 在Java中，以下声明数组错误的是()

（选择一项）
- A) int[]num;
- B) int num[];
- C) int num[][];
- D) int[][3]num;

【参考答案】：D

### 在Java中，以下对字符串数组定义错误的是()
（选择一项）
- A) String str1[]={"abc","d","ef","g"};
- B) String []str2={"abc","d","ef","g"};
- C) String [4]str3={"abc","d","ef","g"};
- D) String str4[]=new String[4];

【参考答案】：C

### 在java中,关于数组说法不正确的是()
（选择一项）
- A) 数组中可以存放不同数据类型的数据
- B) int i[] = {1,2}，声明数组i并初始化值1和2
- C) 可以使用数组对象的length属性获取数组的长度
- D) 数组中的索引从0开始

【参考答案】：A

### 在JAVA中, 以下说法正确的是()

（选择一项）
- A) 定义了数组的长度后,我们可以随时去修改这个长度
- B) 对象可作方法的参数,对象数组不能作方法参数
- C) 数组是相同类型的数据元素按顺序组成的一种新结构数据类型
- D) 以上说法都不正确

【参考答案】：C

### 在JAVA中，以下程序段能正确赋值的是()
（选择一项）
- A) int a[]={1,2,3,4};
- B) int b[4]={1,2,3,4};
- C) int c[];c={1,2,3,4};
- D) int d[];d[]={1,2,3,4};

【参考答案】：A

### 在java中，关于数组描述正确的是()

（选择二项）
- A) 数组中的索引下标从1开始
- B) 存储在数组当中的数据都属于同一数据类型
- C) 通过数组名.length()能获得数组的长度
- D) 数组的最大索引下标是数组的长度减1

【参考答案】：BD

```java
public static void main(String [] args){
       char[] cs={'a','b','c','d','e','f'};
       System.out.println(cs[2]);
       System.out.println(cs[6]);
}
```

（选择一项）
- A) bf
- B) c
- C) 编译时错误
- D) 运行时错误

【参考答案】：D

### 在JAVA中,下列关于数组使用说法正确的是()

（选择一项）
- A) 在JAVA中,数组是一个常量,用于将相同数据类型的数据存放在一起
- B) 数组元素的下标从1开始
- C) 数组中的元素是无序排列的
- D) 向数组中添加元素时,元素数目不能超过数组的长度

【参考答案】：D

## 8. 方法

### 在Java中，以下关于类的方法说法不正确的是()

（选择二项）
- A) 在程序中使用方法的名称可以执行方法中包含的语句，这个过程称为方法调用
- B) 在类的一个方法中不可以直接使用方法名引用该类中的其他方法
- C) 调用别的类定义的方法是，必须首先创建类的对象，然后使用点运算符引用
- D) 定义类的方法必须包括以下3个部分：方法名称、方法返回值的类型、方法的主体

【参考答案】：BC

### 以下关于Java中使用包的说法不正确的是()
（选择一项）
- A) package是关键字，包的声明必须是Java源文件中的第一条非注释性语句
- B) 一个Java源文件只能有一个包声明语句
- C) Java包的名字通常是由小写字母组成，不能以圆点开头或结尾
- D) 要使用不在同一个包中的类，不需要将包显式的包括在Java程序中

【参考答案】：D

### 在使用Eclipse进行代码调试时，在调试视图中，按F5和F6键都是单步执行，它们的区别是()
（选择一项）
- A) F5是"单步跳过"，仅执行本行代码；F6是"单步跳过"，会进入本行代码内部执行
- B) F5是"单步跳过"，仅执行本行代码；F6是"单步跳出"，会从代码内部跳出
- C) F5是"单步跳入"，会进入本行代码内部执行；F6是"单步跳过"，仅执行本行代码
- D) F5和F6都是单步执行，没有本质区别

【参考答案】：C

### 在创建JAVA类时，使用()关键字声明包

（选择一项）
- A) package
- B) import
- C) class
- D) new

【参考答案】：A

### 分析下面的Java程序段，编译运行后的输出结果是()

```java
public class Test {
    public void changeString(StringBuffer sb) {
        sb.append("stringbuffer2");
    }
    public static void main(String[] args) {
        Test a = new Test();
        StringBuffer sb = new StringBuffer("stringbuffer1");
        a.changeString(sb);
        System.out.println("sb = " + sb);
    }
}
```

（选择一项）
- A) sb = stringbuffer2stringbuffer1
- B) sb = stringbuffer1
- C) sb = stringbuffer2
- D) sb = stringbuffer1stringbuffer2

【参考答案】：D

### 在Java中，包有多种用途，但不包含()

（选择一项）
- A) 将类组合成较小的单元，便于使用
- B) 有助于避免命名冲突
- C) 有助于提高运行效率
- D) 允许在更广的范围内保护类、数据和方法

【参考答案】：C

### 在Java中，包com中定义了类TestUtil，在com的子包util中定义了同名类TestUtil，给定如下Java代码，编译运行时，将发生()

```java
package net;
import com.util.TestUtil;
import com.TestUtil;
public class Test {
  public static void main(String[] args)  {
    TestUtil testutil = new TestUtil();
  }
}
```

（选择一项）
- A) 创建了一个com.TestUtil对象
- B) 创建了一个com.util.TestUtil对象
- C) 创建了一个com.TestUtil对象和一个com.util.TestUtil对象
- D) 编译无法通过

【参考答案】：D

### 给定如下Java代码，编译运行后，输出的结果将是()

```java
public class Test {
    public static void main(String args[]) {
        String s1 = new String("Test");
        String s2 = new String("Test");
        if (s1 == s2)
            System.out.println("Same");
        if (s1.equals(s2))
            System.out.println("Equals");
    }
}
```

（选择一项）
- A) Same
- B) Equals
- C) Same Equals
- D) 什么都不输出

【参考答案】：B

### 给定如下Java程序的方法结构，则方法体实现语句正确的是()

```java
public String change(int i){
    //方法体
}
```

（选择一项）
- A) return 100;
- B) return 'a';
- C) return i+"";
- D) return i;

【参考答案】：C

### 给定Java代码如下所示，则编译运行后，输出结果是()

```java
public class Test{
  static int i;
  public int aMethod() {
      i++;
      return i;
  }
  public static void main(String args[]) {
    Test test = new Test();
    test.aMethod();
    System.out.println(test.aMethod());
  }
}
```

（选择一项）
- A) 0
- B) 1
- C) 2
- D) 3

【参考答案】：C

### 给定一个Java程序的代码如下所示，则编译运行后，输出结果是()

```java
public class Test {
    int count = 9;
    public void count1() {
        int count = 10;
        System.out.println("count1=" + count);
    }
    public void count2() {
        System.out.println("count2=" + count);
    }
    public static void main(String args[]) {
        Test t = new Test();
        t.count1();
        t.count2();
    }
}
```

（选择一项）
- A) count1=9
     count2=9
- B) count1=10
     count2=9
- C) count1=10
     count2=10
- D) count1=9
     count2=10

【参考答案】：B

### 在Java中，如果要在字符串类型对象s="java"中，得到字母 'v' 出现的位置，可使用以下()语句

（选择一项）
- A) s.matches('v');
- B) s.charAt('v');
- C) s.indexOf('v');
- D) s.substring('v');

【参考答案】：C

### 给定某Java程序的main方法如下，该程序编译运行后的结果是()

```java
public static void main(String[] args) {
    String str=null;
    str.concat("abc");
    str.concat("def");
    System.out.println(str);
}
```

（选择一项）
- A) 输出：null
- B) 输出：abcdef
- C) 编译错误
- D) 运行时出现异常

【参考答案】：D

### 给定一个Java程序的代码如下所示，则编译运行后，输出结果是()

```java
public class Test {
    int count = 9;
    public void count1() {
        count = 10;
        System.out.println("count1=" + count);
    }
    public void count2() {
        System.out.println("count2=" + count);
    }
    public static void main(String args[]) {
        Test t = new Test();
        t.count1();
        t.count2();
    }
}
```

（选择一项）
- A) count1=9
     count2=9
- B) count1=10
     count2=9
- C) count1=10
     count2=10
- D) count1=9
     count2=10

【参考答案】：C

### 有关Java中的类和对象，以下说法错误的是()

（选择一项）
- A) 同一个类的所有对象都拥有相同的特征和行为
- B)  类和对象一样，只是说法不同
- C) 对象是具有属性和行为的实体
- D) 类规定了对象拥有的特征和行为

【参考答案】：B

### 给定如下Java代码片段，编译运行后，输出结果是()

```java
String s="ABCDE";
System.out.print(s.substring(3).concat("FGH"));
```

（选择一项）
- A)  CDEFGH
- B)  DEFGH
- C) FGH
- D) ABCDE

【参考答案】：B

### 分析如下的Java代码，编译运行时将输出()

```java
String s = new String("TEDUTest");
int i = 1;
int j = 4;
System.out.println(s.substring(i,j));
```

（选择一项）
- A) TEDU
- B) TED
- C) EDU
- D) EDUT

【参考答案】：C

### 分析如下的Java代码，编译运行的输出结果是()

``` java
public class Test {
    public static void main(String[] args) {
        String s;
        System.out.println("s=" + s);
    }
}

```

（选择一项）
- A) 编译通过，并且输出：s=
- B) 编译通过，并且输出：s=null
- C) 编译通过，无任何输出
- D)  编译报错，s未初始化

【参考答案】：D

### 在Java中，源文件Test.java中包含如下代码段，则程序编译运行结果是()

```java

public class Test{
	public static void main(String[]args){
		system.out.print("Hello!");
	}
}
```

（选择一项）
- A) 输出：Hello！
- B) 编译出错，提示"无法解析system"
- C) 运行正常，但没有输出任何内容
- D) 运行时出现异常

【参考答案】：B

### 关于Java类中带参数的方法，下列选项中的说法错误的是()

（选择一项）
- A) 使用带带参数的方法 分为两步：1、定义带参数的方法；2、调用带参数的方法
- B) 带参数方法的返回值不能为void
- C) 带参数方法的参数个数能为一个或多个
- D) 带参数方法的参数可以是任意的基本类型数据或引用类型数据

【参考答案】：B

### 在java中，使用Arrays类对数组进行操作时，应在java源代码中编写的导入语句是()

（选择一项）
- A) import java.util.*;
- B) import java.lang.*;
- C) package java.lang.*;
- D) package java.util.*;

【参考答案】：A

### 在java中,下面()语句可以在屏幕输出"hello,world!"

（选择一项）
- A) System.out.println("hello,world!");
- B) system.out.println("hello.world!");
- C) system.Out.println("hello,world!");
- D) System.Out.println("hello,world!");

【参考答案】：A

### 在JAVA中,下列说法中正确的是()

（选择一项）
- A) "hello ".indexOf("e")返回的值为"1"
- B) "hello ".substring(2)返回的字符串是"io"
- C) "hello ".substring(2,3)返回的字符串是"el"
- D) "hello ".trim()返回的字符串是" hello"

【参考答案】：A

### 在JAVA中,下列说法错误的是()

（选择一项）
- A) 访问修饰符有protected,public,private与static
- B) _Test是一个合法的方法名称
- C) 如果方法不返回值需要声明为void类型
- D) 方法的参数可以是一个类,也可以是一个数据类型

【参考答案】：A

### 在JAVA中,下列关于类与对象说法正确的是()

（选择一项）
- A) JAVA是一种结构化的语言
- B) 对象是用来描述客观事物的一个实体
- C) 对象静态特征是对象所表现的行为或对象所具有的功能
- D) 对象的动态特征是可以用某些数据来描述的特征

【参考答案】：B

### 在java中，下面说法正确的是()

（选择一项）
- A) 对象是类的类型
- B) 类定义了对象将会拥有的特征和行为
- C) public Car定义了一个类，类名为Car
- D) Car car=Car();该语句创建了一个Car对象，对象名为car

【参考答案】：B

### 在JAVA中，能够去掉字符串前后空格的方法是()

（选择一项）
- A) trim()
- B) replace()
- C) delete()
- D) substring()

【参考答案】：A

### 在JAVA中,关于类的方法,下列描述错误的是()

（选择一项）
- A) 类的每一个方法都定义了一个功能
- B) 类的方法对应对象的属性,描述对象的静态特征
- C) 方法名必须以字母,下划线"_"或"$"开头
- D) 方法体应该由一对大括号括起来,它包含一段程序代码

【参考答案】：B

### 在java中，关于类与对象的方法，正确的是()

（选择一项）
- A) 代码：String s= new String(); 中的String 是类,s是对象
- B) 对象包括类，类是对象的具体表现
- C) 对象拥有自己的行为和属性，和对应的类没有关系
- D) 学生，可以看作是张三类或者李四类的对象

【参考答案】：A

### 在JAVA中,运行下面的代码,输出结果是()

```java
public class Sample{
	int i =20;
	int j=10;
	void test(){
		int i =3;
		System.out.println("result is "+(i+j));
	}
	public static void main(String args[]){
		Sample t = new Sample();
		t.test();
	}
}
```

（选择一项）
- A) result is30
- B) result is 13
- C) result is 33
- D) 编译错误

【参考答案】：B

### 在JAVA中，下面代码输出结果为()

```java
public static void main(String []args){
	String s1=new String("hr@bdqn.com");
	String s2=new String();
	s2=s1.substring(0,s1.indexOf("@"));
	System.out.println("s2="+s2);
}
```

（选择一项）
- A) hr
- B) s2=hr@
- C) s2=hr
- D) hr@

【参考答案】：C

### 在JAVA中，如下代码段的输出结果为()

```java
public class Test{
	public void method1(int i){
		System.out.println(i);
	}
	public void method2(){
		int i=2;
		System.out.println(i);
	}
	public void method3(){
		System.out.println(i);
	}
	public static void main(String args[]){
		Test test =new Test();
		test.method1(5);
		test.method2();
		test.method3();
	}
}
```

（选择一项）
- A) 5
     2
     2
- B) 5
     2
     5
- C) 不能正常运行，编译错误
- D) 以上都不正确

【参考答案】：C

### 在java中，运行下面代码后输入80，则输出结果是()

```java

import java.util.*;
public class Hello{
	public static void main(String []args){
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println(num+10);
	}
}
```

（选择一项）
- A) 80
- B) 90
- C) 70
- D) 50

【参考答案】：B

### 在Java中，关于输出语句的说法错误的是()

（选择一项）
- A) System.out.println();这条语句只能用于输出字符串内容，不能输出数值
- B) System.out.println("");与System.out.print("\n")的作用是一样的
- C) System.out.println();用途是打印信息后自动换行
- D) 转义字符"\n"表示换行

【参考答案】：A

### 在JAVA代码中，为求两数之和的方法添加javadoc注释正确的是()

（选择二项）
- A) //**计算两数之和
    *@return sum
    */
- B) /**计算两数之和
    *@return sum
    */
- C) /**计算两数之和
    *@param a
    *@param b
    */
- D) /*计算两数之和
    *@author chris
    */

【参考答案】：BC

### 下列java代码中，正确的是()

（选择一项）
- A) StringBuffer sb1 = new StringBuffer();
    sb1="重在实践";
- B) String s="重在实践";
    StringBuffer sb1 = new StringBuffer(s);
- C) StringBuffer sb1 = "重在实践";
- D) StringBuffer sb1 = new StringBuffer("重在实践");
    String s =sb1;

【参考答案】：B

### 在java中，关于main()方法说法正确的是()

（选择二项）
- A) java程序的main()方法是程序运行的入口
- B) main()方法中可以将void改成String
- C) java程序的main()方法中如果只有一条语句，也要用{}号括起来
- D) 一个程序可以有多个main方法

【参考答案】：AC

### 下列JAVA代码中，定义方法正确的是()
（选择一项）
- A) public void add(){
    	return 1+2;
    }
- B) public int add(){
    	return 3+2.5;
    }
- C) public double add(){
    	return 2,3;
    }
- D)
    } public int add(){
    	System.out.print("很努力的计算中……");
    	return 5+2;

【参考答案】：D

### 在JAVA中，如下代码段输出结果为()

```java

public static void main(String []args){
	String s1="mama";
	String s2=",I miss you!";
	s1=s1.toUpperCase();
	s2.toLowerCase();
	System.out.println(s1+s2);
}
```

（选择一项）
- A) mama,I miss you!
- B) mama,I MISS YOU!
- C) MAMA,i miss you!
- D) MAMA,I miss you!

【参考答案】：D

### 下列JAVA代码，如果输出的结果产sohu,①②处应该填写()

```java

public static void main(String []args){
	String str="yalida@sohu.com";
	String type=str.substring(str.indexOf( ① )+1,str.indexOf( ② ));
	System.out.println(type);
}
```

（选择一项）
- A) ①处填写'@' ②处填写'.'
- B) ①处填写'@' ②处填写'c'
- C) ①处填写'a' ②处填写'u'
- D) ①处填写'@' ②处填写'u'

【参考答案】：A

### 在JAVA中，如下代码段的输出结果为()

```java
class Book{
	float price;
	public void setPrice(Book book,int addPrice){
		System.out.println(book.price+addPrice);
	}
}
public class Test{
	public static void main(String [] args){
		Book book=new Book();
		book.setPrice(book,5);
	}
}
```

（选择一项）
- A) 5
- B) 0
- C) 5.0
- D) 编译错误

【参考答案】：C

### 在JAVA中，关于trim()的用法说法正确的是()

（选择一项）
- A) 仅可以去掉字符串前面的空格
- B) 仅可以去掉字符中后面的空格
- C) 仅可以去掉字符串中间的空格
- D) 可以去掉字符串前后的空格

【参考答案】：D

### 在JAVA中，下面语句有()处错误

```java

public static void main(String args[]){
	int age =21;
	String name ='李伟';
	System.out.println("姓名="name+",年龄=" +age);
}
```

（选择一项）
- A) 1
- B) 2
- C) 3
- D) 0

【参考答案】：B

### 在JAVA中,有HelloWorld.java的文件，其内容如下，则控制输出的内容是()

```java

public class helloWorld{
	public static void main(String args[]){
		System.out.print("今天学习了；");
		System.out.println("HelloWorld");
	}
}
```

（选择一项）
- A) 今天学习了;HelloWorld
- B) 今天学习了;
     HelloWorld
- C) 今天学习了；	HelloWorld
- D) 编译错误

【参考答案】：D

### 在java中，运行下面的代码，输出结果是()

```java

public static void main(String []args){
	String s1= new String("pb_java_OOP_T5");
	String s2 = s1.substring(s1.lastIndexOf("_"));
	System.out.println("s2="+s2);
}
```

（选择一项）
- A) s2=_java_OOP_T5
- B) s2=_OOP_T5
- C) s2=_T5
- D) 编译出错

【参考答案】：C

### 在JAVA中，如下代码段的输出结果为()

```java
public class Test{
	public float add(){
		return 25.5+13;
	}
	public double division(){
		return 25/2;
	}
	public static void main(String []args){
		Test test = new Test();
		System.out.println(test.add());
		System.out.println(test.division());
	}
}
```

（选择一项）
- A) 38.5
    12.0
- B) 38.5
    12
- C) 38.5
    1.0
- D) 编译时出错

【参考答案】：D

#### 在JAVA中，如下代码的输出结果是()

```java
public static void main(String[]args){
	int le = "my Java".trim().length();
	System.out.println(le);
}
```

（选择一项）
- A) 8
- B) 7
- C) 6
- D) 编辑错误

【参考答案】：B

### 在JAVA中，下面语句输出的结果是()

```java
public static void main(String []args){
	int age =21;
	String name ="阳光";
	System.out.println("姓名=name"+"，年龄="+age);
}
```

（选择一项）
- A) 姓名=阳光,年龄=21
- B) 姓名=name,年龄=age
- C) 姓名=name,年龄=21
- D) 编译错误

【参考答案】：C

### 在java中,运行如下代码段,则输出结果为()

```java

public class Test{
	public static void swap(int num1,int num2){
		int temp=num1;
		num1=num2;
		num2=temp;
	}
	public static void main(String []args){
		int n1=20 ;
		int n2=30;
		swap(n1,n2);
		System.out.println(n1+","+n2);
	}
}
```

（选择一项）
- A) 20,30
- B) 30,20
- C) 程序逻辑错误
- D) 程序编译错误

【参考答案】：A

### 运行如下JAVA代码，输出结果为()

```java

public class Demo{
	public void method1(int i,String s){
		i = i *3;
		System.out.println(s +"说的数字扩大3倍后为:" + i);
	}
	public static void main(String []args){
		Demo demo = new Demo();
		int i =9;
		String s = "tom";
		demo.method1(i,s);
		System.out.println(i);
	}
}
```

（选择一项）
- A) tom说的数字扩大3倍后为： 27
    9
- B) tom说的数字扩大3倍后为：27
    27
- C) tom说的数字扩大3倍后为：9
    9
- D) tom说的数字扩大3倍后为：9
    27

【参考答案】：A

### 在JAVA中，运行如下代码段，则输出结果为()

```java
public class Test{
	public static void swap(int num1,int num2){
		int temp = num1;
		num1 = num2;
		num2 = temp;
	}
	public static void main(String []args){
		int n1=20;
		int n2 =30;
		swap(n1,n2);
		System.out.println(n1+","+n2);
	}
}
```

(选择一项）
- A) 20，30
- B) 30，20
- C) 程序逻辑错误
- D) 程序编译错误

【参考答案】：A

### 在JAVA程序中,若出现

```java
fun(3,fun(fun(1,2),5))
//返回值均为int
```

这样一段代码，则可以判断一定存在含有()个形参的fun()方法
（选择一项）
- A) 1
- B) 2
- C) 3
- D) 4

【参考答案】：B

### 在JAVA中,关于函数描述正确的是()

（选择一项）
- A) 每个函数都可被其它函数调用
- B) 在一个函数内部可以定义另一个函数
- C) 在一个函数内部可以调用另一个函数
- D) 以上说法都不正确

【参考答案】：C

### 在JAVA中,以下程序的输出结果为()

```java
public class Test{
	public static void chang(String str){
		str ="你好";
	}
	public static void main(String []args){
		String str="hello";
		chang(str);
		System.out.print(str);
	}
}
```

（选择一项）
- A) 你好
- B) hello
- C) Hello
- D) 编译时出错

【参考答案】：B

### 在java语言中有如下代码，则输出结果为。()

```java
public class Test{
	public static String str = "hello";
	public static void main(String[] args){
		Test test = new Test();
		test.change();
		System.out.println(str);
	}
	public void change(){
		str = "Hello,world";
	}
}
```

（选择一项）
- A) hello
- B) Hello,world
- C) 无任何输出
- D) 编译错误

【参考答案】：B

### 在java中main方法为入口方法，下列选项()是正确的写法

（选择一项）
- A) public static void main(String args[]){}
- B) private void static main(String args[]){}
- C) public static void main(int args){}
- D) public static String main(String args){}

【参考答案】：A

### 关于java中方法的说法错误的是()

（选择一项）
- A) 对象执行的操作可以通过编写类的方法来实现
- B) 方法可以带参数,也可以不带参数
- C) 方法调用的形式必须是:类名.方法名()
- D) 方法可以有返回值，也可以没有返回值

【参考答案】：C

### 在JAVA中，如下代码的输出结果为()

```java
public class Test{
	public static String str="hello";
	public static void main(String[]args){
		Test test = new Test();
		test.change();
		System.out.print(str);
	}
	public void change(){
		str="Hello,world!";
	}
}
```

（选择一项）
- A) hello
- B) Hello,world!
- C) 无任何内容
- D) 编译错误

【参考答案】：B

### 在java中，如下代码横线处为()时，输出结果是"hello"

```java
public class Test{
	public static String str="hello";
	public static void main(String []args){
		System.out.println(_________);
	}
}
```

（选择二项）
- A) Test.str
- B) new Test().str
- C) Test().str
- D) 任何代码都无法满足

【参考答案】：AB

### 在Java中,运行下列代码输出结果是()

```java
public class Test{
	public void changeString(String s){
		s=s.concat("world");
	}
	public static void main(String[]args){
		Test test=new Test();
		String s="hello";
		test.changeString(s);
		System.out.println(s);
	}
}
```

（选择一项）
- A) hello
- B) helloworld
- C) world
- D) 编译错误

【参考答案】：A

### 在Java中,如下代码段的输出结果为()

```java
String s="hello";
s.substring(2,4);
System.out.println(s.length());
```

（选择一项）
- A) 5
- B) 2
- C) 4
- D) 3

【参考答案】：A

### 在Java中，如下代码段的输出结果为()

```java
public class Test{
	public void findLetter(String s,String find){
		System.out.println(s.indexOf(find));
	}
	public static void main(String[]args){
		String s="Hello World!";
		Test test=new Test();
		test.findLetter(s,"o");
	}
}
```

（选择一项）
- A) 5
- B) 4
- C) -1
- D) 4
    7

【参考答案】：B

### 在Java中，已有

```java
StringBuffer str=new StringBuffer()
```

使str的内容为"Hello"的语句是()
（选择一项）
- A) str.append("hello")
- B) str="hello"
- C) str=str.concat("hello")
- D) str.concat("hello")

【参考答案】：A

### 在Java中，以下程序的运行结果是()

```java
public class Test{
	private int num;
	public static void main(String[]args){
		Test t=new Test();
		t.num=12;
		System.out.println(t.num);
	}
}
```

（选择一项）
- A) 编译错误
- B) 运行错误
- C) 0
- D) 12

【参考答案】：D

### 在Java中,以下关于类与源文件描述错误的是()

（选择一项）
- A) 一个源文件中可以有多个类
- B) 一个源文件中只能有一个public修饰的类
- C) public修饰的类名与源文件名必须相同
- D) 根本不存在private修饰的类，这样的类毫无意义

【参考答案】：D

### 在Java中，以下关于类和对象描述正确的是()

（选择一项）
- A) 一个类只能有一个对象
- B) 对象是类的具体实例
- C) 对象是现实世界中客观事物的抽象
- D) 使用class关键字定义一个对象

【参考答案】：B

### 在Java中，以下代码的运行结果是()

```java
public class Test{
	public static void main(String args[]){
		String str="aBcD";
		str.toLowerCase();
		System.out.println(str.equals("abcd"));
		System.out.println(str.equals("ABCD"));
	}
}
```

（选择一项）
- A) true
    false
- B) false
    true
- C) true
    true
- D) false
    false

【参考答案】：D

### 在Java中，以下程序的运行结果是()

```java
public class Main{
	public static void main(String[]args){
		String str="test";
		chang(str);
		System.out.println(str);
	}
	public static void chang(String str){
		str+="<<";
		str+=">>";
	}
}
```

（选择一项）
- A) <<test>>
- B) test<<>>
- C) test
- D) str

【参考答案】：C

### 在Java中，如下代码段的输出结果为()

```java
String s="my";
s=s.concat("dog");
System.out.println(s.length());
```

（选择一项）
- A) 5
- B) 2
- C) 10
- D) 3

【参考答案】：A

### 在Java中，以下定义的方法可以编译通过的是()

（选择一项）

```java
- A) String check(){
            System.out.print("正在执行方法");
            return 1+1;
        }
```

```java
- B) public int check(){
            double number = 3+3.5;
            return number;
        }
```

```java
- C) public int check(){
            return 2+'a';
            sSystem.out.print("正在执行方法");
        }
```

```java
- D) public double check(){
            return 5+2;
            System.out.print("正在执行方法");
        }

```

【参考答案】：C

### 在java中，下面选项输出结果为true的是()
（选择一项）

```java
- A) System.out.println("CBD".equalsIgnoreCase("cbd"));

```

```java
- B) String s1 = new String("CBD");
    String s2 = new String("CBD");
    System.out.println(s1==s2);

```

```java
- C) System.out.println("CBD" =="cbd");

```

```java
- D) System.out.println("CBD".equals("cbd"));

```

【参考答案】：A

### 在Java中，关于程序调试说法正确的是( )

（选择一项）
- A) 每个程序都必须进行调试工作
- B) 可以在程序中设置断点,在调试的时候方便程序停在某一处,一遍发现程序错误
- C) 使用Eclipse调试的步骤顺序为:启动调试\设置断点\单步执行\分析错误
- D) 设置的断点在调试结束后会自动修改错误

【参考答案】：B

### 在Java中，如下代码的输出结果为( )

```java
public static void main(String[] args){
	int math = 90,english = 56;
	int sum =(math+english)/2;
	if(sum>85){
		System.out.println("老师说：奖励一个MP4!");
	}else if(english<60){
		System.out.println("老师说：英语太差！");
	}else{
		System.out.println("老师什么也没说！");
	}
}

```

（选择一项）
- A) 老师说：英语太差！
- B) 老师说：奖励一个MP4！
- C) 老师什么也没说！
- D) 老师说：奖励一个MP4！
     老师说：英语太差！

【参考答案】：A

### 在Java中，如下代码的输出结果为( )

```java
int num =1;
for(int i=0;i<3;i++){
	switch(num){
		case 0:
			System.out.print(i);
		case 1:
			System.out.print(i);
			break;
		default:
			break;
	}
}

```

（选择一项）
- A) 012
- B) 01
- C) 12
- D) 无信息输出

【参考答案】：A

### 如下的Java代码输出的结果是( )

```java
public static void main(String[] args){
	for(int i = 0;i<3;i++){
		for(int j = 1;j<i;j++){
			System.out.print(i);
			System.out.print(j);
		}
	}
}

```

（选择一项）
- A) 021
- B) 012
- C) 21
- D) 321

【参考答案】：C

### (单选题)阅读下列代码

```java
public class Test{
String s="One World One Dream";
public static void main(String args[]){
System.out.println(s);
    }
}
```

其运行结果是()

- A)args
- B)One World One Dream
- C)s
- D)编译错误

【参考答案】：D

### (单选题)关于 Java 类中带参数的方法，下列选项中说法错误的是()

- A)使用带参数的方法分为两步：定义带参数的方法，调用带参数的方法
- B)带参数方法的返回值不能为 void
- C)带参数方法的参数个数可以为一个或多个
- D)带参数方法的参数可以是任意的基本类型数据或引用类型数据

【参考答案】：B

### (单选题)判断方法重载的依据，错误的是()

A)必须在同一个类中
B)方法名不同
C)方法参数的个数、顺序或类型不同
D)与方法的修饰符或返回值没有关系

【参考答案】：B

## 9. 项目案例
