# 03_04_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [DataDemo.java](DataDemo.java)

**概要**

基本数据类型转换的相关代码

**学习目标**

* 自动类型的转换和强制转换的注意事项