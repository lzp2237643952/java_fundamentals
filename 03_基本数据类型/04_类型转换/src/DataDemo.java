//基本数据类型的转换
public class DataDemo {
	public static void main(String[] args) {
		double h = 25.9876;
		int i = (int) h;
		System.out.println(i);// 强转有可能会丢失精度--25

		double j = 80 / 100;
		System.out.println(j); // 0.0
		double k = 80.0 / 100;
		System.out.println(k); // 0.8

		byte b1 = 5;
		byte b2 = 6;
		byte b3 = (byte) (b1 + b2);

		System.out.println(2 + 2); // 4
		System.out.println('2' + '2'); // '2'的码50,加上'2'的码50---100
	}
}
