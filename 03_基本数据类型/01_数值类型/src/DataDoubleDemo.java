//double类型
public class DataDoubleDemo {
	public static void main(String[] args) {
		// double：浮点型，8个字节
		double pi = 3.14; // 3.14为浮点直接量，为double类型
		float f = 3.14F; // 3.14为float直接量

		double a = 3.0, b = 2.9;
		System.out.println(a - b);// 0.10000000000000009,舍入误差
		double c = 6.0, d = 4.9;
		System.out.println(c - d);// 1.0999999999999996,舍入误差
	}

}
