//int类型
public class DataTypeDemo {
	public static void main(String[] args) {
		int a = 5;// 5为整数直接量，默认为int类型
		// int b=10000000000; //100亿为整数直接量，默认为int类型，但超出了int范围

		System.out.println(5 / 2); // 2
		System.out.println(2 / 5); // 0
		System.out.println(5.0 / 2); // 2.5

		int b = 2147483647;
		b = b + 1;
		System.out.println(b); // 发生了溢出(-2147483648)
								// b+2(-2147483647) b+3(-2147483646)
	}
}
