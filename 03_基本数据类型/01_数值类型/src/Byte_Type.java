public class Byte_Type {
    public static void main(String[] args) {

        // byte类型占据的内存大小
        System.out.println("byte类型占据的二进制位数：" + Byte.SIZE);

        // byte类型对应的包装类型
        System.out.println("包装类：java.lang.Byte");

        // byte类型能够存储的最小值
        System.out.println("byte类型能够存储的最大值：Byte.MIN_VALUE = " + Byte.MIN_VALUE);

        // byte类型能够存储的最大值
        System.out.println("byte类型能够存储的最大值：Byte.MAX_VALUE = " + Byte.MAX_VALUE);

        // 给byte类型赋值（正常范围内）
        byte byte_min = -128;
        byte byte_max = 127;

        // 输出赋值后的byte变量
        System.out.println("byte_min = " + byte_min);
        System.out.println("byte_max = " + byte_max);

        // 给byte类型赋值（超出范围）
        // byte byte_min_err = -200; //取消注释后会出现编译时错误
        // byte byte_max_err = 200; //编译时错误

    }
}