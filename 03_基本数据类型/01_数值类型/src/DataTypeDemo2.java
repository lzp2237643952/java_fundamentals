//long类型
public class DataTypeDemo2 {
	public static void main(String[] args) {
		// long：长整型，8个字节，很大
		// long a=10000000000; //编译错误，100为int类型，但超出了int范围
		long b = 10000000000L; // 100亿L为长整型直接量

		long c = 1000000000 * 2 * 10L;
		System.out.println(c); // 200亿

		long d = 1000000000 * 3 * 10L;
		System.out.println(d); // 溢出了

		long e = 1000000000L * 3 * 10;
		System.out.println(e);// 300亿

		long f = System.currentTimeMillis();
		System.out.println(f); // 1970年1月1日到当前系统时间的毫秒数
	}

}
