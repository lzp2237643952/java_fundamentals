# 03_01_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [Byte_Byte.java](Byte_Type.java)

**概要**

基本数据类型 byte 的相关代码

**学习目标**

* byte类型的存储范围
* 包装类：java.lang.Byte

## [DataTypeDemo.java](DataTypeDemo.java)

**概要**

基本数据类型 int 的相关代码

**学习目标**

* int类型的存储范围
* int类型的溢出

## [DataTypeDemo1.java](DataTypeDemo1.java)

**概要**

基本数据单位之间的转换规则

**学习目标**

* 学会单位之间的转换规则

## [DataTypeDemo2.java](DataTypeDemo2.java)

**概要**

基本数据类型 long 的相关代码

**学习目标**

* long类型的存储范围
* System.currentTimeMillis()方法计算1970年1月1日到当前系统时间的毫秒数

## [DataDoubleDemo.java](DataDoubleDemo.java)

**概要**

基本数据类型 double 的相关代码

**学习目标**

* double类型和float类型的书写区别
* 计算时的舍入误差