# 1. 03_int类型

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC
---

<!-- TOC -->

- [1. 录播视频int类型](#1-录播视频int类型)
- [2. int：整型](#2-int整型)

<!-- /TOC -->

---

<a id="markdown-1-录播视频int类型" name="1-录播视频int类型"></a>
## 1. 录播视频int类型

- [腾讯视频](https://v.qq.com/x/page/g0615o2igmh.html)
- [B站](https://www.bilibili.com/video/av23108274/?p=2)
- [优酷](https://v.youku.com/v_show/id_XMzQ3NTU2NjYxMg==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19rwrnnx0p.html#vfrm=8-8-0-1)

<a id="markdown-2-int整型" name="2-int整型"></a>
## 2. int：整型

4个字节，-21个多亿到21个多亿（-2^31到2^31-1）

1、整数直接量默认为int型，但不能超范围，超范围则编译错误

2、两个整数相除，结果还是整数，小数位无条件舍弃

3、整数运算时超范围则发生溢出，溢出是需要避免的