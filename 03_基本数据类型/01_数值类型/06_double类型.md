# 1. 06_double类型

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC
---

<!-- TOC -->

- [1.1. 录播视频double类型](#11-录播视频double类型)
- [1.2. double：双精度浮点类型](#12-double双精度浮点类型)

<!-- /TOC -->

---

<a id="markdown-11-录播视频double类型" name="11-录播视频double类型"></a>
## 1.1. 录播视频double类型

- [腾讯视频](https://v.qq.com/x/page/q0646ii5ipe.html)
- [B站](https://www.bilibili.com/video/av23108274/?p=4)
- [优酷](https://v.youku.com/v_show/id_XMzU5MDM1MzU4NA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19rxlyrzvp.html#vfrm=8-8-0-1)

<a id="markdown-12-double双精度浮点类型" name="12-double双精度浮点类型"></a>
## 1.2. double：双精度浮点类型

8个字节，很大很大很大 10^-308 ~ 10^308

1、浮点数直接量默认为double型，表示float需在数字后加F或f

2、浮点型数据参与运算时，有可能会出现舍入误差，精确运算场合不能使用