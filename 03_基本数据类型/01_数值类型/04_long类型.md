# 1. 04_long类型

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC
---

<!-- TOC -->

- [1. 录播视频long类型](#1-录播视频long类型)
- [2. long：长整型](#2-long长整型)

<!-- /TOC -->

---

<a id="markdown-1-录播视频long类型" name="1-录播视频long类型"></a>
## 1. 录播视频long类型

- [腾讯视频](https://v.qq.com/x/page/q06158xw5vl.html)
- [B站](https://www.bilibili.com/video/av23108274/?p=3)
- [优酷](https://v.youku.com/v_show/id_XMzQ3NTU2ODE2NA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19rwrnql0h.html#vfrm=8-8-0-1)

<a id="markdown-2-long长整型" name="2-long长整型"></a>
## 2. long：长整型

8个字节，很大很大很大 -2^63 到 2^63-1

1、长整型直接量需在数字后加L或l

2、运算时若有可能溢出，建议在第1个数字后加L或l

3、`System.currentTimeMillis()` 用于获取自 1970.1.1零时到此时此刻的毫秒数