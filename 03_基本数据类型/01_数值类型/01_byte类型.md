# 01_byte类型

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. 录播视频](#1-录播视频)
    - [1.1. 8种基本数据类型](#11-8种基本数据类型)
- [2. byte:整型，1个字节，-128~127](#2-byte整型1个字节-128127)

<!-- /TOC -->

---

<a id="markdown-1-录播视频" name="1-录播视频"></a>
## 1. 录播视频

<a id="markdown-11-8种基本数据类型" name="11-8种基本数据类型"></a>
### 1.1. 8种基本数据类型

- [腾讯视频](https://v.qq.com/x/page/t061582oco1.html)
- [B站](https://www.bilibili.com/video/av23108274/?p=1)
- [优酷](https://v.youku.com/v_show/id_XMzQ3NTU2NTk4MA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19rwrnkoo5.html#vfrm=8-8-0-1)

<a id="markdown-2-byte整型1个字节-128127" name="2-byte整型1个字节-128127"></a>
## 2. byte:整型，1个字节，-128~127

1、byte是做为最小的数字来处理的，范围为-128~127。

2、在Java中，byte类型的数据是8位带符号的二进制数。

3、为什么byte的范围是-128~127呢？

- [ ] 待完善。共产主义接班人来回答