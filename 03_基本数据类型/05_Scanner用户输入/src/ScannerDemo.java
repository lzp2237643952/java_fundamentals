import java.util.Scanner;

public class ScannerDemo {

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);

        System.out.println("使用 next() 方式接收用户输入：");

        if (scanner.hasNext()) {
            String input = scanner.next();
            System.out.println("输入的数据为：" + input);
        }
        scanner.close();
    }
}