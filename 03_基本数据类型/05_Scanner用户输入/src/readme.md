# 05_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [ScannerDemo.java](ScannerDemo.java)

**概要**

Scanner 类常用语法

**学习目标**

- Scanner类的创建
- 使用Scanner类接收用户输入