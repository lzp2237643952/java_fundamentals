# 01_Scanner用户输入

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. Scanner 类基本语法](#1-scanner-类基本语法)
    - [1.1. next() 方法](#11-next-方法)
    - [1.2. nextLine() 方法](#12-nextline-方法)
    - [1.3. nextInt() 方法](#13-nextint-方法)
    - [1.4. Scanner的用法](#14-scanner的用法)

<!-- /TOC -->

---

<a id="markdown-1-scanner-类基本语法" name="1-scanner-类基本语法"></a>
## 1. Scanner 类基本语法

我们可以通过 Scanner 类来获取用户的输入。

Scanner 对象的基本语法如下：

```java
Scanner s = new Scanner(System.in);
```

<a id="markdown-11-next-方法" name="11-next-方法"></a>
### 1.1. next() 方法

使用 next() 方法需要注意以下几点：

- 一定要读取到有效字符后才可以结束输入
- 对输入有效字符之前遇到的空白，next() 方法会自动将其去掉
- 只有输入有效字符后才将其后面输入的空白作为分隔符或者结束符
- next() 不能得到带有空格的字符串

示例代码如下：

```java
public static void main(String[] args) {
    // 创建Scanner对象实例
    Scanner scanner = new Scanner(System.in);
    // 给用户输入提示，在控制台显式
    System.out.println("使用 next() 方式接收用户输入：")
    // 如果获取输入不为空
    if (scanner.hasNext()) {
        // 将用户输入存储变量 input
        String input = scanner.next();
        // 在控制台输出
        System.out.println("输入的数据为：" + input);
    }
    scanner.close();  // 关闭Scanner对象实例
}
```

<a id="markdown-12-nextline-方法" name="12-nextline-方法"></a>
### 1.2. nextLine() 方法

- [ ] 有待补充

<a id="markdown-13-nextint-方法" name="13-nextint-方法"></a>
### 1.3. nextInt() 方法

- [ ] 有待补充

<a id="markdown-14-scanner的用法" name="14-scanner的用法"></a>
### 1.4. Scanner的用法

1、在package下:

```java
import java.util.Scanner;
```

2、在main中:

```java
Scanner scan = new Scanner(System.in);
```

3、在第2步之下:

```java
System.out.println("请输入年龄:");
int age = scan.nextInt();
System.out.println("请输入价格:");
double price = scan.nextDouble();
```