//char类型演示
public class DataCharDemo {
	/*
	 * 0------------------------0 '0'----------------------48
	 * 表现的形式是字符char,实质上是码int
	 * 
	 * 'a'---97 'A'----65 '0'--48 字符char 码int(0到65535之间)
	 * 
	 */
	public static void main(String[] args) {
		// char:字符型,2个字节
		char c1 = '女'; // 字符女
		char c2 = 'f'; // 字符f
		char c3 = '6'; // 字符6
		char c4 = ' '; // 字格符
		// char c5=女; //编译错误,字符直接量必须放在单引号中
		// char c6=''; //编译错误,必须有字符--不允许空字符
		// char c7='女性'; //编译错误,只能有一个字符
		System.out.println(c1);

		char c8 = 65; // 0到65535
		System.out.println(c8); // A

		char c9 = '\\';
		System.out.println(c9);// \
	}

}
