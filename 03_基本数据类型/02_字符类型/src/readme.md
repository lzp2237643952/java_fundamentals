# 03_02_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [DataCharDemo.java](DataCharDemo.java)

**概要**

基本数据类型 char 的相关代码

**学习目标**

* char 类型的书写格式
* 转义字符