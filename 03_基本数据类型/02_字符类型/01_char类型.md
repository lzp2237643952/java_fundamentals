# 1. 01_char类型

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC
---

<!-- TOC -->

- [1.1. 录播视频](#11-录播视频)
    - [1.1.1. char类型的概念01](#111-char类型的概念01)
    - [1.1.2. char类型的概念02](#112-char类型的概念02)
    - [1.1.3. char类型转义符](#113-char类型转义符)
- [1.2. char类型,2个字节](#12-char类型2个字节)
- [1.3. 对char型变量赋值](#13-对char型变量赋值)
- [1.4. 使用转义字符](#14-使用转义字符)

<!-- /TOC -->

---

<a id="markdown-11-录播视频" name="11-录播视频"></a>
## 1.1. 录播视频

<a id="markdown-111-char类型的概念01" name="111-char类型的概念01"></a>
### 1.1.1. char类型的概念01

- [腾讯视频](https://v.qq.com/x/page/s0646vwf2qt.html)
- [B站](https://www.bilibili.com/video/av23108274/?p=7)
- [优酷](https://v.youku.com/v_show/id_XMzU5MDM2NTI0MA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19rxly4fbd.html#vfrm=8-8-0-1)

<a id="markdown-112-char类型的概念02" name="112-char类型的概念02"></a>
### 1.1.2. char类型的概念02

- [腾讯视频](https://v.qq.com/x/page/x0646duuiuc.html)
- [B站](https://www.bilibili.com/video/av23108274/?p=8)
- [优酷](https://v.youku.com/v_show/id_XMzU5MDM2NjAyNA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19rxly3w5h.html#vfrm=8-8-0-1)

<a id="markdown-113-char类型转义符" name="113-char类型转义符"></a>
### 1.1.3. char类型转义符

- [腾讯视频](https://v.qq.com/x/page/h0646xh2qll.html)
- [B站](https://www.bilibili.com/video/av23108274/?p=9)
- [优酷](https://v.youku.com/v_show/id_XMzU5MDM3NTA4OA==.html?spm=a2hzp.8253869.0.0)
- [爱奇艺](http://www.iqiyi.com/w_19rxly4gfd.html#vfrm=8-8-0-1)

<a id="markdown-12-char类型2个字节" name="12-char类型2个字节"></a>
## 1.2. char类型,2个字节

1、用Unicode字符集编码，一个字符(char)对应一个码(int)

*Tips：表现的形式是字符char，本质上是码 int（0 到 65535 之间）
ASCII码（'a'--97  'A'--65  '0'--48）*

<a id="markdown-13-对char型变量赋值" name="13-对char型变量赋值"></a>
## 1.3. 对char型变量赋值

```java
char a = 'A';
```

字符直接量必须放在单引号中，只能有一个.

<a id="markdown-14-使用转义字符" name="14-使用转义字符"></a>
## 1.4. 使用转义字符

特殊符号需要通过`\`来转义。

1. `\b`：退格
2. `\n`：换行
3. `\t`：制表符，相当于tab键
4. `\r`：回车
5. `\\`：表示反斜杠
6. `\'`：表示单引号
7. `\"`：表示双引号

*Tips：转义字符的意义就是避免出现二义性，二义性是所有编程语言都不允许的。*