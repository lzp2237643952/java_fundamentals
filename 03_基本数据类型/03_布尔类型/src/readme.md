# 03_03_代码说明

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

## [DataBooleanDemo.java](DataBooleanDemo.java)

**概要**

基本数据类型 boolean 的相关代码

**学习目标**

* boolean的赋值