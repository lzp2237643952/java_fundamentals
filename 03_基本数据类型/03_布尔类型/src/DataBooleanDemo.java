//boolean类型
public class DataBooleanDemo {
	public static void main(String[] args) {

		// boolean :布尔型,1个字节
		boolean b1 = true; // true为布尔直接量
		boolean b2 = false;
		// boolean b3=5; //编译错误,只能赋值为true和false

	}

}
